#ifndef SCRIPTENGINE_H
#define SCRIPTENGINE_H

#include <QObject>
#include <QJSEngine>
#include <QFileInfo>
#include <QThread>
#include "database.h"

namespace Script
{

class File;

class ScriptEngine : public QObject
{
    Q_OBJECT
    QJSEngine *_jsEngine;
    Database *_database;
    QString _scriptPath;
    QString _outputDir;
    QStringList _paths;
public:
    explicit ScriptEngine(QObject *parent = nullptr);
    void setParams(const QString &scriptPath, const QStringList &paths, const QString &outputDir);
    void reportError(const QString &message);
    const QString& outputDir() const;
    void interrupt();
    void logError(const QString &message);
    Q_INVOKABLE void log(const QString &message);
    Q_INVOKABLE void mark(File *file);
    Q_INVOKABLE void unmark(File *file);
    Q_INVOKABLE bool isMarked(const File *file) const;
public slots:
    void run();
signals:
    void newMessage(const QString &message, bool error);
    void finished();
};

class ScriptEngineThread : public QObject
{
    Q_OBJECT
    QThread *_thread;
    ScriptEngine *_engine;
public:
    ScriptEngineThread(QObject *parent = nullptr);
    ~ScriptEngineThread();
    void setParams(const QString &scriptPath, const QStringList &paths, const QString &outputDir);
    void start();
    void interrupt();
signals:
    void newMessage(const QString &message, bool error);
    void finished();
};

class File : public QObject
{
    Q_OBJECT
    ScriptEngine *_engine;
    QString _path;
    QFileInfo _info;
    bool _fitsKeywordsLoaded = false;
    QMap<QString, QString> _fitsKeywords;
    void loadFitsKeywords();
    bool mkpath(const QString &path) const;
public:
    explicit File(const QString &path, ScriptEngine *engine);
    Q_INVOKABLE QString fileName() const;
    Q_INVOKABLE QString absoluteFilePath() const;
    Q_INVOKABLE QString absolutePath() const;
    Q_INVOKABLE QString baseName() const;
    Q_INVOKABLE QString completeBaseName() const;
    Q_INVOKABLE QString suffix() const;
    Q_INVOKABLE qint64 size() const;
    Q_INVOKABLE QStringList fitsKeywords();
    Q_INVOKABLE QString fitsValue(const QString &key);
    Q_INVOKABLE bool isMarked() const;
    Q_INVOKABLE bool copy(const QString &newpath) const;
    Q_INVOKABLE bool move(const QString &newpath) const;
    Q_INVOKABLE bool convertTo(const QString &format);
};

}

#endif // SCRIPTENGINE_H
