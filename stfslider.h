#ifndef STFSLIDER_H
#define STFSLIDER_H

#include <QObject>
#include <QWidget>

class STFSlider : public QWidget
{
    Q_OBJECT
    float m_blackPoint;
    float m_midPoint;
    float m_whitePoint;
    int m_grabbed;
    bool m_fineTune;
    float m_fineTuneX;
    QColor m_color;
    float m_threshold;
public:
    explicit STFSlider(const QColor &color = Qt::white, QWidget *parent = nullptr);
    float blackPoint() const;
    float midPoint() const;
    float whitePoint() const;
    void setMTFParams(float low, float mid, float high);
signals:
    void paramChanged(float blackPoint, float midPoint, float whitePoint);
protected:
    void paintEvent(QPaintEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *) override;
};

#endif // STFSLIDER_H
