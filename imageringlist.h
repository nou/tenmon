#ifndef IMAGERINGLIST_H
#define IMAGERINGLIST_H

#include <QObject>
#include <QFileSystemWatcher>
#include <QList>
#include <QPixmap>
#include <QDir>
#include <memory>
#include "imageinfo.h"
#include "rawimage.h"

class ImageRingList;
class QThreadPool;

class Image : public QObject
{
    Q_OBJECT
    bool m_loading;
    bool m_released;
    bool m_current;
    int m_number;
    std::shared_ptr<RawImage> m_rawImage;
    std::shared_ptr<RawImage> m_thumbnail;
    QString m_name;
    ImageInfoData m_info;
    ImageRingList *m_ringList;
public:
    explicit Image(const QString name, int number, ImageRingList *ringList);
    void load();
    void loadThumbnail(QThreadPool *pool);
    void release();
    QString name() const;
    std::shared_ptr<RawImage> rawImage();
    const RawImage* thumbnail() const;
    ImageInfoData info() const;
    bool isCurrent() const;
    int number() const;
    void clearThumbnail();
signals:
    void pixmapLoaded(Image *ptr);
    void thumbnailLoaded(Image *ptr);
protected slots:
    void imageLoaded(std::shared_ptr<RawImage> rawImage, ImageInfoData info);
    void thumbnailLoadFinish(std::shared_ptr<RawImage> rawImage);
};

typedef std::shared_ptr<Image> ImagePtr;

class Database;

class ImageRingList : public QAbstractItemModel
{
    Q_OBJECT
    int m_width;
    QList<ImagePtr> m_images;
    QList<ImagePtr>::iterator m_firstImage;
    QList<ImagePtr>::iterator m_currImage;
    QList<ImagePtr>::iterator m_lastImage;
    QFileSystemWatcher m_fileSystemWatcher;
    bool m_liveMode;
    QDir::SortFlag m_sort = QDir::Name;
    bool m_reversed = false;
    AnalyzeLevel m_analyzeLevel;
    QThreadPool *m_thumbPool;
    Database *m_database;
    QStringList m_nameFilter;
    QTimer *m_slideShowTimer;
public:
    explicit ImageRingList(Database *database, const QStringList &nameFilter, QObject *parent = 0);
    ~ImageRingList() override;
    bool setDir(const QString path, const QString &currentFile = QString());
    void setFile(const QString &file);
    ImagePtr currentImage();
    void setLiveMode(bool live);
    void setCalculateStats(bool stats);
    void setFindPeaks(bool findPeaks);
    void setFindStars(bool findStars);
    AnalyzeLevel analyzeLevel() const;
    void loadFile(int row);
    void loadThumbnails();
    void stopLoading();
    int imageCount() const;
    QStringList imageNames() const;
    void updateMark();
    void clearThumbnails();

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
public slots:
    void setPreload(int width);
    void setSort(QDir::SortFlag sort);
    void reverseSort();
    void toggleSlideshow(bool start);
    void increment();
    void decrement();
protected:
    void setFiles(const QStringList files, const QString &currentFile = QString());
    QList<ImagePtr>::iterator increment(QList<ImagePtr>::iterator iter);
    QList<ImagePtr>::iterator decrement(QList<ImagePtr>::iterator iter);
signals:
    void pixmapLoaded(Image *image);
    void thumbnailLoaded(Image *image);
    void infoLoaded(ImageInfoData info);
    void currentImageChanged(int index);
protected slots:
    void imageLoaded(Image *image);
    void dirChanged(QString dir);
};

#endif // IMAGERINGLIST_H
