uniform sampler2D qt_Texture0;
in vec2 qt_Vertex;
in vec2 qt_MultiTexCoord0;
out vec2 qt_TexCoord0;
out vec2 center;

void main(void)
{
    vec2 texSize = vec2(textureSize(qt_Texture0, 0));
    gl_Position = vec4(qt_Vertex, 0.0, 1.0);
    qt_TexCoord0 = qt_MultiTexCoord0;
    qt_TexCoord0.y = 1.0 - qt_TexCoord0.y;
    center = qt_TexCoord0 * texSize.xy;
}
