uniform sampler2D qt_Texture0;
uniform ivec2 firstRed;
in vec2 qt_TexCoord0;
in vec2 center;
layout(location = 0) out vec4 color;

#define f(x, y) texelFetch(qt_Texture0, icenter + ivec2(x, y), 0).r

void main(void)
{
    ivec2 texSize = textureSize(qt_Texture0, 0);
    ivec2 icenter = ivec2(center);
    ivec2 alternate = (icenter + firstRed) % 2;

    // cross, checker, theta, phi
    const vec4 kA = vec4(-1.0, -1.5,  0.5, -1.0) / 8.0;
    const vec4 kB = vec4( 2.0,  0.0,  0.0,  4.0) / 8.0;
    const vec4 kC = vec4( 4.0,  6.0,  5.0,  5.0) / 8.0;
    const vec4 kD = vec4( 0.0,  2.0, -1.0, -1.0) / 8.0;
    const vec4 kE = vec4(-1.0, -1.5, -1.0,  0.5) / 8.0;
    const vec4 kF = vec4( 2.0,  0.0,  4.0,  0.0) / 8.0;

    float A = f(0,2) + f(0,-2);
    float B = f(0,1) + f(0,-1);
    float C = f(0,0);
    float D = f(1,1) + f(-1,1) + f(1,-1) + f(-1,-1);
    float E = f(2,0) + f(-2,0);
    float F = f(1,0) + f(-1,0);

    vec4 P = kA*A + kB*B + kC*C + kD*D + kE*E + kF*F;

    color.rgb = alternate.y == 0 ?
                (alternate.x == 0 ? vec3(C, P.xy) : // even row even col
                                    vec3(P.z, C, P.w)) : // even row odd col
                (alternate.x == 0 ? vec3(P.w, C, P.z) : // odd row even col
                                    vec3(P.yx, C)); // odd row odd col

    color.a = 1.0;
}

