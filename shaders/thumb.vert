in vec2 qt_Vertex;
in vec2 qt_MultiTexCoord0;
in ivec3 imageSize_num;
out vec3 qt_TexCoord0;
uniform ivec3 viewport_row;
uniform mat4 mvp;
uniform vec2 offset;
uniform ivec3 thumb_size;

void main(void)
{
    vec2 pos = qt_Vertex * 0.5;
    pos.y *= -1.0;
    pos = pos * vec2(imageSize_num.xy) + float(thumb_size.x);
    ivec2 off = ivec2(imageSize_num.z % viewport_row.z, imageSize_num.z / viewport_row.z) * thumb_size.yz;

    gl_Position = mvp * vec4(pos - offset + vec2(off), 0.0, 1.0);
    qt_TexCoord0 = vec3(qt_MultiTexCoord0, float(imageSize_num.z) + 0.1);
}
