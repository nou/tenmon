#version 330

uniform sampler2D qt_Texture0;
in vec2 qt_Vertex;
in vec2 qt_MultiTexCoord0;
out vec2 qt_TexCoord0;
uniform vec2 viewport;
uniform vec2 offset;
uniform float zoom;

void main(void)
{
    vec2 texSize = vec2(textureSize(qt_Texture0, 0));
    vec2 scale = viewport / texSize;
    vec2 offsetRel = offset / texSize;
    gl_Position = vec4(qt_Vertex, 0.0, 1.0);
    qt_TexCoord0 = (qt_MultiTexCoord0 * scale + offsetRel) * zoom;
}
