#version 330

uniform sampler2DArray qt_Texture0;
uniform vec3 mtf_param[3];
uniform bool invert;
in vec3 qt_TexCoord0;
layout(location = 0) out vec4 color;

vec4 MTF(vec4 x, vec4 low, vec4 mid, vec4 high)
{
    x = (x - low) / (high - low);
    x = clamp(x, vec4(0.0), vec4(1.0));
    return ((mid - 1) * x) / ((2 * mid - 1) * x - mid);
}

void main(void)
{
    color = texture(qt_Texture0, qt_TexCoord0);
    color = MTF(color, vec4(mtf_param[0], 0.0), vec4(mtf_param[1], 0.5), vec4(mtf_param[2], 1.0));
    if(invert)color = vec4(1.0) - color;
    color.a = 1.0;
}
