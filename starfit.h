#ifndef STARFIT_H
#define STARFIT_H

#include "rawimage.h"
#include <gsl/gsl_multifit_nlinear.h>

double gauss_model(double a, double x0, double y0, double sx, double sy, double x, double y);

struct Star
{
    double m_am;
    double m_x,m_y;
    double m_sx,m_sy;
    double m_theta;
    Star();
    bool valid() const;
    double hwhmX() const;
    double hwhmY() const;
    double hw20X() const;
    double hw20Y() const;
    double fwhmX() const;
    double fwhmY() const;
    bool operator<(const Star &d) const;
};

class StarFit
{
    int m_size;
    gsl_multifit_nlinear_fdf m_fdf;
    gsl_multifit_nlinear_fdf m_fdf_an;
    gsl_multifit_nlinear_parameters m_fdf_params;
    gsl_vector *m_vector;
public:
    StarFit(int size);
    ~StarFit();
    Star fitStar(const std::vector<double> &data, bool angle);
};

#endif // STARFIT_H
