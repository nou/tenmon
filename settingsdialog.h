#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSpinBox>
#include <QCheckBox>
#include <QComboBox>

class SettingsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent = nullptr);
    static void loadSettings();
    static bool loadThumbsizes();
signals:
    void preloadChanged(int witdth);
private:
    void saveSettings();

    QSpinBox *m_preloadImages;
    QSpinBox *m_thumSize;
    QDoubleSpinBox *m_slideShowTime;
    QDoubleSpinBox *m_saturation;
    QCheckBox *m_useNativeDialog;
    QCheckBox *m_qualityThumbnail;
    QComboBox *m_filtering;
    QCheckBox *m_bestFit;
};

#endif // SETTINGSDIALOG_H
