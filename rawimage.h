#ifndef RAWIMAGE_H
#define RAWIMAGE_H

#include <vector>
#include <algorithm>
#include <memory>
#include <stdint.h>
#include <math.h>
#include <memory.h>
#include <QImage>

extern int THUMB_SIZE;
extern int THUMB_SIZE_BORDER;
extern int THUMB_SIZE_BORDER_Y;

class Peak
{
    uint32_t m_v;
    uint32_t m_x,m_y;
public:
    Peak() : m_v(0), m_x(0), m_y(0) {}
    Peak(uint32_t v, uint32_t x, uint32_t y) : m_v(v), m_x(x), m_y(y) {}
    uint32_t v() const { return m_v; }
    uint32_t x() const { return m_x; }
    uint32_t y() const { return m_y; }
    double distance(const Peak &d) const
    {
        uint32_t dx = m_x-d.m_x;
        uint32_t dy = m_y-d.m_y;
        return dx*dx + dy*dy;
    }
    bool operator <(const Peak &d) const
    {
        return m_v > d.m_v;
    }
};

class RawImage
{
    using PixelType = uint8_t;
public:
    enum DataType
    {
        UINT8,
        UINT16,
        UINT32,
        FLOAT32,
        FLOAT64,
    };
    struct Stats
    {
        bool m_stats = false;
        double m_mean[4] = {0.0};
        double m_stdDev[4] = {0.0};
        double m_median[4] = {0.0};
        double m_min[4] = {0.0};
        double m_max[4] = {0.0};
        double m_mad[4] = {0.0};
        uint32_t m_saturated[4] = {0};
        std::vector<uint32_t> m_histogram;
    };
protected:
    std::unique_ptr<PixelType[]> m_pixels;
    std::unique_ptr<PixelType[]> m_original;
    uint32_t m_width = 0;
    uint32_t m_height = 0;
    uint32_t m_channels = 0;
    uint32_t m_ch = 0;
    DataType m_type = UINT8;
    DataType m_origType = UINT8;
    float m_thumbAspect = 0.0;
    Stats m_stats;
    void allocate(uint32_t w, uint32_t h, uint32_t ch, DataType type);
public:
    RawImage();
    RawImage(uint32_t w, uint32_t h, uint32_t ch, DataType type);
    RawImage(const RawImage &d);
    RawImage(RawImage &&d);
    RawImage(const QImage &img);
    const RawImage::Stats& imageStats();
    void calcStats();
    void rect(int &x, int &y, int w, int h, std::vector<double> &r) const;
    int findPeaks(double background, double distance, std::vector<Peak> &peaks) const;
    uint32_t width() const;
    uint32_t height() const;
    uint32_t channels() const;
    uint32_t size() const;
    DataType type() const;
    uint32_t norm() const;
    void* data();
    const void* data() const;
    void* data(uint32_t row, uint32_t col = 0);
    const void* data(uint32_t row, uint32_t col = 0) const;
    const void *origData() const;
    const void *origData(uint32_t row, uint32_t col = 0) const;
    void convertToThumbnail();
    void convertToGLFormat();
    float thumbAspect() const;
    bool pixel(int x, int y, double &r, double &g, double &b) const;
    void resize(uint32_t w, uint32_t h);
    std::pair<float, float> unitScale() const;

    static std::shared_ptr<RawImage> fromPlanar(const RawImage &img);
    static std::shared_ptr<RawImage> fromPlanar(const void *pixels, uint32_t w, uint32_t h, uint32_t ch, DataType type);
    static size_t typeSize(DataType type);
    std::vector<RawImage> split() const;
    bool valid() const;
};

//Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr);
//Q_DECLARE_METATYPE(std::shared_ptr<RawImage>);

#endif // RAWIMAGE_H
