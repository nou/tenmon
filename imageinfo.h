#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QTreeWidget>
#include <wcslib/wcs.h>
#include <cmath>
#include <memory>

namespace LibXISF { struct FITSKeyword; struct Property; }

struct FITSRecord
{
    QByteArray key;
    QVariant value;
    QByteArray comment;
    bool editable() const;
    FITSRecord(){}
    FITSRecord(const QByteArray &key, const QVariant &value, const QByteArray &comment);
    FITSRecord(const LibXISF::FITSKeyword &record);
    FITSRecord(const LibXISF::Property &property);
    QByteArray valueToByteArray() const;
};

class SkyPoint
{
    double ra = NAN;
    double dec = NAN;
public:
    SkyPoint();
    SkyPoint(double ra, double dec);
    void set(double ra, double dec);
    double RA() const { return ra; }
    double DEC() const { return dec; }
    QString toString() const;
};

class WCSData
{
    int nwcs = 0;
    struct wcsprm *wcs = nullptr;
    int width;
    int height;
    void freeWCS();
public:
    WCSData(int width, int height, char *header, int nrec);
    WCSData(int width, int height, const QVector<FITSRecord> &header);
    WCSData(const WCSData &) = delete;
    ~WCSData();
    bool pixelToWorld(const QPointF &pixel, SkyPoint &point) const;
    bool worldToPixel(const SkyPoint &point, QPointF &pixel) const;
    void calculateBounds(double &minRa, double &maxRa, double &minDec, double &maxDec, double &crVal1, double &crVal2) const;
    bool valid() const { return wcs; };
};

struct ImageInfoData
{
    QVector<FITSRecord> fitsHeader;
    QVector<QPair<QString, QString>> info;
    std::shared_ptr<WCSData> wcs;
};

Q_DECLARE_METATYPE(ImageInfoData);

typedef enum
{
    None,
    Statistics,
    Peaks,
    Stars,
}AnalyzeLevel;

class ImageInfo : public QTreeWidget
{
    Q_OBJECT
public:
    explicit ImageInfo(QWidget *parent);
    ~ImageInfo() override;
public slots:
    void setInfo(const ImageInfoData &info);
};

#endif // IMAGEINFO_H
