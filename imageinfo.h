#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QTreeWidget>
#include "imageinfodata.h"

class ImageInfo : public QTreeWidget
{
    Q_OBJECT
public:
    explicit ImageInfo(QWidget *parent);
    ~ImageInfo() override;
public slots:
    void setInfo(const ImageInfoData &info);
};

#endif // IMAGEINFO_H
