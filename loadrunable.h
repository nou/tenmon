#ifndef LOADRUNABLE_H
#define LOADRUNABLE_H

#include <QRunnable>
#include <QString>
#include <QSemaphore>
#include <QSize>
#include "imageinfodata.h"

class Image;

class LoadRunable : public QRunnable
{
    QString m_file;
    Image *m_receiver;
    AnalyzeLevel m_analyzeLevel;
    bool m_thumbnail;
public:
    LoadRunable(const QString &file, Image *receiver, AnalyzeLevel level, bool thumbnail = false);
    void run() override;
};

class ConvertRunable : public QRunnable
{
public:
    struct ConvertParams
    {
        int compressionLevel = -1;
        QString compressionType;
        int binning = 0;
        bool average = true;
        QSize resize;
        Qt::AspectRatioMode aspect = Qt::KeepAspectRatio;
        bool autostretch = false;
        ConvertParams(){}
        ConvertParams(const QVariantMap &map);
    };
    ConvertRunable(const QString &in, const QString &out, const QString &format, const ConvertParams &params = ConvertParams(), QSemaphore *semaphore = nullptr);
    void run() override;
private:
    QString m_infile;
    QString m_outfile;
    QString m_format;
    ConvertParams m_params;
    QSemaphore *m_semaphore;
};

#endif // LOADRUNABLE_H
