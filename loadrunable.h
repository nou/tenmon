#ifndef LOADRUNABLE_H
#define LOADRUNABLE_H

#include <QRunnable>
#include <QString>
#include "imageinfo.h"

bool readFITSHeader(const QString &path, ImageInfoData &info);
bool readXISFHeader(const QString &path, ImageInfoData &info);

class Image;

class LoadRunable : public QRunnable
{
    QString m_file;
    Image *m_receiver;
    AnalyzeLevel m_analyzeLevel;
    bool m_thumbnail;
public:
    LoadRunable(const QString &file, Image *receiver, AnalyzeLevel level, bool thumbnail = false);
    void run() override;
};

class ConvertRunable : public QRunnable
{
    QString m_infile;
    QString m_outfile;
    QString m_format;
public:
    ConvertRunable(const QString &in, const QString &out, const QString &format);
    void run() override;
};

#endif // LOADRUNABLE_H
