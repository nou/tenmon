<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
    <id>space.nouspiro.tenmon</id>
    <launchable type="desktop-id">space.nouspiro.tenmon.desktop</launchable>
    <name>Tenmon</name>
    <summary>FITS/XISF image viewer, converter, index and search</summary>
    <developer id="nouspiro.space">
    	<name>Dušan Poizl</name>
    </developer>
    <developer_name>Dušan Poizl</developer_name>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0</project_license>
    <description>
        <p>It is intended primarily for viewing astro photos and images. It supports the following formats:</p>
        <ul>
            <li>FITS 8, 16, 32 bit integer and 32, 64 bit float</li>
            <li>XISF 8, 16, 32 bit integer and 32, 64 bit float</li>
            <li>RAW CR2/CR3, DNG, NEF</li>
            <li>JPEG, PNG, BMP, GIF, PBM, PGM, PPM and SVG images</li>
        </ul>
        <p>Features of application:</p>
        <ul>
            <li>Using same stretch function as PixInsight</li>
            <li>OpenGL accelerated drawing</li>
            <li>Index and search FITS XISF header data</li>
            <li>Quick mark images and then copy/move marked files</li>
            <li>Convert FITS &lt;-&gt; XISF</li>
            <li>Convert FITS/XISF -&gt; JPEG/PNG</li>
            <li>Image statistics mean, media, min, max</li>
            <li>Support for WCS</li>
            <li>Thumbnails</li>
            <li>Convert CFA images to colour - debayer</li>
            <li>Color space aware</li>
            <li>Histogram</li>
            <li>Scripting</li>
        </ul>
    </description>
    <categories>
        <category>Graphics</category>
        <category>Viewer</category>
        <category>Science</category>
        <category>Astronomy</category>
    </categories>
    <keywords>
        <keyword>astronomy</keyword>
    </keywords>
    <url type="homepage">https://nouspiro.space/?page_id=206</url>
    <url type="bugtracker">https://github.com/flathub/space.nouspiro.tenmon/issues</url>
    <screenshots>
        <screenshot type="default">
            <caption>Main window with image</caption>
            <image>https://nouspiro.space/wp-content/uploads/2022/04/tenmon-1024x579.png</image>
        </screenshot>
        <screenshot type="default">
            <caption>Thumnail view</caption>
            <image>https://nouspiro.space/wp-content/uploads/2022/12/tenmon2-1024x645.png</image>
        </screenshot>
    </screenshots>
    <content_rating type="oars-1.1"/>
    <releases>
    <release version="20250302" date="2025-03-02">
      <description>
        <ul>
          <li>Add resize and binning to script</li>
          <li>Auto stretch to script</li>
          <li>Fix opening UNC paths starting</li>
          <li>Add more color maps for false color</li>
          <li>Open image with best fit</li>
        </ul>
      </description>
    </release>
    <release version="20250126" date="2025-01-26">
      <description>
        <ul>
          <li>Support for really big images +50000px</li>
          <li>Fix handling of MAX_PATH on Windows</li>
          <li>Add setting solver profile in scripts</li>
        </ul>
      </description>
    </release>
    <release version="20241116" date="2024-11-16">
      <description>
        <ul>
          <li>Extending support of data formats</li>
        </ul>
      </description>
    </release>
    <release version="20241002" date="2024-10-02">
      <description>
        <ul>
          <li>Plate solving</li>
          <li>Open marked files as directory</li>
          <li>Linux ARM port</li>
          <li>Improved ICC color profile handling</li>
          <li>Add *.fz and *.fts as FITS extension</li>
        </ul>
      </description>
    </release>
    <release version="20240816" date="2024-08-16">
      <description>
        Fix saving image
      </description>
    </release>
    <release version="20240616" date="2024-06-16">
      <description>
        <ul>
          <li>Batch processing with JavaScript</li>
          <li>Opening directory recursively</li>
        </ul>
      </description>
    </release>
      <release version="20240201" date="2024-02-01">
            <description>
                  <ul>
                      <li>Smooth thumbnails</li>
                      <li>Respect ROWORDER</li>
                      <li>Bugfixes</li>
                  </ul>
            </description>
      </release>
    	<release version="20240108" date="2024-01-08">
            <description>
                  <ul>
                      <li>Update to Qt6</li>
                      <li>Add support for CR3 RAW files</li>
                      <li>Slideshow</li>
                      <li>Improved rapid image view</li>
                  </ul>
            </description>
        </release>
    	<release version="20231116" date="2023-11-16">
            <description>
                  <ul>
                      <li>Histogram</li>
                      <li>False colors</li>
                      <li>Strech each RGB channel individually</li>
                      <li>Better white balancing</li>
                  </ul>
            </description>
        </release>
        <release version="20230419" date="2023-04-19">
            <description>
                  <ul>
                      <li>Add file sorting</li>
                      <li>Improved zoom and scrolling.</li>
                      <li>Shift modify if zoom on mouse position or center</li>
                      <li>Fix issue with XISF from NINA</li>
                      <li>Fix crash in flatpak version</li>
                  </ul>
            </description>
        </release>
        <release version="20230212" date="2023-02-12">
            <description>
                  <ul>
                      <li>Replace PCL with LibXISF</li>
                      <li>Fix issue with OpenGL</li>
                      <li>Fix loading SATURATION settings</li>
                      <li>Fix issue with whitebalance coefficients</li>
                  </ul>
            </description>
        </release>
        <release version="20221228" date="2022-12-28">
            <description>
                  <ul>
                      <li>Add gray world white balance</li>
                      <li>Export database to CSV</li>
                      <li>Fine tune of STF when Shift key is pressed</li>
                      <li>Add saturation statistic</li>
                      <li>Various fixes</li>
                  </ul>
            </description>
        </release>
        <release version="20221215" date="2022-12-15">
            <description>
                  <ul>
                      <li>Add debayer support</li>
                      <li>Move to trash action</li>
                      <li>Change copy and move shortcuts to F5 and F6</li>
                      <li>Change mark and unmark key shortcuts to F7 and F8</li>
                      <li>Fix not refreshing file tree</li>
                      <li>Add option to not use native file dialog</li>
                  </ul>
            </description>
        </release>
    	<release version="20221209" date="2022-12-09"/>
        <release version="20221126" date="2022-11-26"/>
        <release version="20221121" date="2022-11-11"/>
        <release version="20221023" date="2022-10-23"/>
    </releases>
</component>
