#ifndef IMAGESCROLLAREA_H
#define IMAGESCROLLAREA_H

#include <QScrollArea>
#include <QLabel>

class ImageScrollArea : public QScrollArea
{
    Q_OBJECT
    QPoint m_lastPos;
    QLabel *m_label;
    QPixmap m_pixmap;
    float m_scale;
public:
    explicit ImageScrollArea(QWidget *parent = 0);
    void setImage(const QPixmap &img);
    void setScale(float scale);
public slots:
    void zoomIn();
    void zoomOut();
    void bestFit();
    void oneToOne();
protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
};

#endif // IMAGESCROLLAREA_H
