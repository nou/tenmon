#ifndef IMAGESCROLLAREA_H
#define IMAGESCROLLAREA_H

#include "imagewidget.h"
#include <QScrollBar>

class ImageScrollArea : public QWidget
{
    Q_OBJECT
    QScrollBar *m_verticalScrollBar;
    QScrollBar *m_horizontalScrollBar;
    ImageWidget *m_imageWidget;
public:
    explicit ImageScrollArea(Database *database, QWidget *parent = nullptr);
    ~ImageScrollArea();

    void allocateThumbnails(const QStringList &paths);
    void showThumbnail(bool enable);
    void setBayerMask(int mask);
    void setColormap(int colormap);
protected:
    void updateScrollbars(int valueH, int stepH, int maxH, int valueV, int stepV, int maxV);
public slots:
    void zoomIn();
    void zoomOut();
    void bestFit();
    void oneToOne();
    void imageLoaded(Image *image);
    void thumbnailLoaded(const Image *image);
    void setMTFParams(const MTFParam &params);
    void invert(bool enable);
    void superPixel(bool enable);
    void falseColor(bool enable);
    QImage renderToImage();
protected slots:
    void scrollEvent();
signals:
    void fileDropped(const QString &path);
    void status(const QString &value, const QString &pixelCoords, const QString &celestialCoords);
    void scrollBarsUpdate(int valueH, int stepH, int maxH, int valueV, int stepV, int maxV);
};

#endif // IMAGESCROLLAREA_H
