<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk_SK" sourcelanguage="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="12"/>
        <source>About Tenmon</source>
        <translation>O Tenmon</translation>
    </message>
</context>
<context>
    <name>BatchProcessing</name>
    <message>
        <location filename="../batchprocessing.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="208"/>
        <source>Batch Processing</source>
        <translation>Hromadné spracovanie</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="22"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="209"/>
        <source>Input files and directories</source>
        <translation>Vstupné súbory a adresáry</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="44"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="210"/>
        <source>Add files</source>
        <translation>Pridať súbory</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="51"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="211"/>
        <source>Add directories</source>
        <translation>Pridať adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="58"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="212"/>
        <source>Remove</source>
        <translation>Odstrániť</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="213"/>
        <source>Remove all</source>
        <translation>Odstrániť všetko</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="78"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="214"/>
        <source>Output directory</source>
        <translation>Výstupný adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="92"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="215"/>
        <source>Browse</source>
        <translation>Vybrať adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="103"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="216"/>
        <source>Scripts</source>
        <translation>Skripty</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="217"/>
        <source>Open scripts</source>
        <translation>Otvoriť scripty</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="142"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="218"/>
        <source>Log</source>
        <translation>Záznam</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="182"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="219"/>
        <source>Start script</source>
        <translation>Spustiť script</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="192"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="220"/>
        <source>Stop script</source>
        <translation>Zastaviť script</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="199"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="221"/>
        <source>Close</source>
        <translation>Zatvoriť</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="123"/>
        <source>Interrupt running script?</source>
        <translation>Prerušiť bežiaci script?</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="143"/>
        <source>Select files</source>
        <translation>Vybrať súbory</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="154"/>
        <source>Select directory</source>
        <translation>Vybrať adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="175"/>
        <source>Select output directory</source>
        <translation>Vybrať výstupný adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Invalid output directory</source>
        <translation>Neplatný výstupný adresár</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Output directory path doesn&apos;t exist or is not writable</source>
        <translation>Vystupný adresár neexistuje alebo sa doňho nedá zapisovať</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="257"/>
        <source>Enter text</source>
        <translation>Zadajte text</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="264"/>
        <source>Enter integer number</source>
        <translation>Zadajte celé číslo</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="271"/>
        <source>Enter float number</source>
        <translation>Zadajte reálne číslo</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="278"/>
        <source>Select item</source>
        <translation>Vyberte položku</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <location filename="../databaseview.cpp" line="255"/>
        <source>Select columns</source>
        <translation>Vyber stĺpce</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="294"/>
        <source>Text to search, you can % as wildcard</source>
        <translation>Text na vyhľadanie, môžete použit % ako zástupný znak</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="310"/>
        <source>Filter</source>
        <translatorcomment>Meno súboru</translatorcomment>
        <translation>Filter</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <location filename="../databaseview.cpp" line="215"/>
        <source>Mark</source>
        <translation>Označiť</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="216"/>
        <source>Unmark</source>
        <translation>Odznačiť</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <location filename="../databaseview.cpp" line="194"/>
        <source>File name</source>
        <translation>Meno súboru</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="26"/>
        <source>Sort by filename</source>
        <translation>Zoradiť podľa názvu súboru</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="27"/>
        <source>Sort by time</source>
        <translation>Zoradiť podľa času</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="28"/>
        <source>Sort by size</source>
        <translation>Zoradiť podľa veľkosti</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="29"/>
        <source>Sort by type</source>
        <translation>Zoradiť podľa typu</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="30"/>
        <source>Reverse</source>
        <translation>Otočit poradie</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="90"/>
        <location filename="../filesystemwidget.cpp" line="94"/>
        <source>Open</source>
        <translation>Otvoriť</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="96"/>
        <source>Copy marked files</source>
        <translation>Skopírovať označené súbory</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="97"/>
        <source>Move marked files</source>
        <translation>Presunúť označené súbory</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="98"/>
        <source>Index directory</source>
        <translation>Indexovať adresár</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="95"/>
        <source>Set as root</source>
        <translation>Nastav koreňový adresár</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="102"/>
        <source>Reset root</source>
        <translation>Resetuj koreňový adresár</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="103"/>
        <source>Go up</source>
        <translation>O úroveň vyššie</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="104"/>
        <source>Show hidden files</source>
        <translation>Zobraz skryté súbory</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../about.cpp" line="33"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <location filename="../histogram.cpp" line="73"/>
        <source>Logarithmic scale</source>
        <translation>Logaritmická stupnica</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Property</source>
        <translation>Vlastnosť</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="25"/>
        <source>FITS Header</source>
        <translation>FITS hlavička</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="34"/>
        <source>Image info</source>
        <translation>Informácie o obrázku</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <location filename="../imageringlist.cpp" line="406"/>
        <source>Name</source>
        <translation>Meno</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation type="vanished">OpenGL chyba</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation type="vanished">Nepodarilo sa incializovať OpenGL 3.3 context. Ubezpečte sa že sú nainštalované ovládače pre GPU.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation type="vanished">L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation type="vanished">X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation type="vanished">R:%1 G:%2 B:%3</translation>
    </message>
    <message>
        <source>Failed to load image</source>
        <translation type="vanished">Zlyhalo načítanie obrázka</translation>
    </message>
</context>
<context>
    <name>ImageWidgetGL</name>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>OpenGL error</source>
        <translation>OpenGL chyba</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Nepodarilo sa incializovať OpenGL 3.3 context. Ubezpečte sa že sú nainštalované ovládače pre GPU.</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="109"/>
        <source>Failed to load image</source>
        <translation>Zlyhalo načítanie obrázka</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="71"/>
        <source>Image info</source>
        <translation>Informácie o obrázku</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open DB</source>
        <translation>Nie je možné otvoriť DB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Nie je možné otvoriť SQLITE databázu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <source>Filesystem</source>
        <translation>Zoznam súborov</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Tenmon</source>
        <translation>Tenmon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="157"/>
        <source>File</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Open</source>
        <translation>Otvoriť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="162"/>
        <source>Copy marked files</source>
        <translation>Skopírovať označené súbory</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <location filename="../mainwindow.cpp" line="614"/>
        <location filename="../mainwindow.cpp" line="776"/>
        <source>Save as</source>
        <translation>Uložiť ako</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Open directory recursively</source>
        <translation>Otvoriť adresár rekurzívne</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>Batch processing</source>
        <translation>Hromadné spracovanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="177"/>
        <source>Exit</source>
        <translation>Ukončiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="185"/>
        <source>View</source>
        <translation>Zobrazenie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="186"/>
        <source>Zoom In</source>
        <translation>Priblížiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="187"/>
        <source>Zoom Out</source>
        <translation>Oddialiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="188"/>
        <source>Best Fit</source>
        <translation>Najlepšia veľkosť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="212"/>
        <source>Colormap</source>
        <translation>Farebná mapa</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="219"/>
        <source>User %1</source>
        <translation>Použivaľská %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="245"/>
        <source>Select</source>
        <translation>Výber</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="246"/>
        <source>Mark</source>
        <translation>Označiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="247"/>
        <source>Unmark</source>
        <translation>Odznačiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="249"/>
        <source>Mark and next</source>
        <translation>Označiť a ďaľší</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="250"/>
        <source>Unmark and next</source>
        <translation>Odznačiť a ďaľší</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Show marked list</source>
        <translation>Ukázať zoznam označených</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="253"/>
        <source>Open marked</source>
        <translation>Otvoriť označené</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Overwrite file?</source>
        <translation>Prepísať súbor?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Destination file %1 already exists. Overwrite?</source>
        <translation>Cieľový súbor %1 už existuje. Prepísať?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>Missing marked files</source>
        <translation>Chybajúce označené súbory</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>%1 marked files were missing. They were skipped.</source>
        <translation>%1 označených súborov chýbalo. Boly preskočené.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <location filename="../mainwindow.cpp" line="801"/>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Update check</source>
        <translation>Kontrola aktualizácie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <source>You have newest version</source>
        <translation>Máte najnovšiu verzsiu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="801"/>
        <source>New version %1 is available. Do you want to download it now?</source>
        <translation>Nová verzia %1 je k dispozícii. Chcete ju stiahnuť teraz?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Failed to check version</source>
        <translation>Kontrola novej verzie zlyhala</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analýza</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation type="vanished">Štatistiky obrázka</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Docks</source>
        <translation>Dokovacie panely</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="427"/>
        <source>Select destination</source>
        <translation>Vybrať cieľ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Copying</source>
        <translation>Kopírovanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="163"/>
        <source>Move marked files</source>
        <translation>Presunúť označené súbory</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="589"/>
        <source>Index directory</source>
        <translation>Indexovať adresár</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Live mode</source>
        <translation>Živý mód</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>Thumbnails</source>
        <translation>Náhľady</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation type="vanished">Ukázať označené</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation type="vanished">Vyhľadávač vrcholov</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="294"/>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="296"/>
        <source>About Tenmon</source>
        <translation>O Tenmon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="297"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Moving</source>
        <translation>Presúvanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <source>Indexing FITS files</source>
        <translation>Indexovanie FITS/XISF súborov</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>Reindex files</source>
        <translation>Reindexuj súbory</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="124"/>
        <source>FITS/XISF files database</source>
        <translation>Databáza FITS/XISF súborov</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>File tree</source>
        <translation>Strom súborov</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation type="vanished">Vyhľadávač hviezd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Edit</source>
        <translation>Upraviť</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="182"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Images (</source>
        <translation>Obrázky (</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="51"/>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>Obrázok FITS (*.fits *.fit);;Obrázok XISF (*.xisf);;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Check for update</source>
        <translation>Skontroluj novú verziu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to copy</source>
        <translation>Zlyhalo kopírovanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to move</source>
        <translation>Zlyhalo presúvanie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to move from %1 to %2</source>
        <translation>Zlyhalo presúvanie z %1 do %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to copy from %1 to %2</source>
        <translation>Zlyhalo kopírovanie z %1 do %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>;;All files (*)</source>
        <translation>;;Všetky súbory (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Move files to trash?</source>
        <translation>Presunúť súbory do koša?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Presunúť %1 súborov do koša?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash</source>
        <translation>Zlyhalo presunutie súbora do koša</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash %1</source>
        <translation>Zlyhalo presunutie súbora do koša %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="164"/>
        <source>Move marked files to trash</source>
        <translation>Presunúť označené súbory do koša</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Moving marked files to trash</source>
        <translation>Presúvanie do koša</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="168"/>
        <source>Export database to CSV</source>
        <translation>Exportovať databázu do CSV súboru</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>CSV file (*.csv)</source>
        <translation>Súbory CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source>Histogram</source>
        <translation>Histogram</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>Bayer mask</source>
        <translation>Bayerova maska</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="193"/>
        <source>RGGB</source>
        <translation>RGGB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="194"/>
        <source>GRBG</source>
        <translation>GRBG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="195"/>
        <source>GBRG</source>
        <translation>GBRG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="196"/>
        <source>BGGR</source>
        <translation>BGGR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Slideshow</source>
        <translation>Prezentácia</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <location filename="../markedfiles.cpp" line="11"/>
        <source>Marked files</source>
        <translation>Označené súbory</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="22"/>
        <source>Filename</source>
        <translation>Meno súboru</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="30"/>
        <source>Clear selected</source>
        <translation>Zrušiť vybrané</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="31"/>
        <source>Clear all</source>
        <translation>Zrušiť všetky</translation>
    </message>
</context>
<context>
    <name>PlateSolving</name>
    <message>
        <location filename="../platesolving.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="345"/>
        <source>Plate Solving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="23"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="346"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="35"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="347"/>
        <source>Start point</source>
        <translation>Štartovný bod</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="42"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="348"/>
        <source>Degree width</source>
        <translation>Širka v stupňoch</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="47"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="349"/>
        <source>Arcmin width</source>
        <translation>Šírka v oblúkových minútach</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="52"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="350"/>
        <source>Arcsec per pixel</source>
        <translation>Oblúková sekunda na pixel</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="57"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="351"/>
        <source>35 mm equivalent focal length</source>
        <translation>Ekvivalent ohniskovej dĺžky pre 35 mm</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="353"/>
        <source>Use position</source>
        <translation>Použiť polohu</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="88"/>
        <location filename="../platesolving.ui" line="208"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="354"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="364"/>
        <source>DEC</source>
        <translation>DEC</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="355"/>
        <source> h</source>
        <translation> h</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="111"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="356"/>
        <source>High</source>
        <translation>Horná</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="357"/>
        <source>Use scale</source>
        <translation>Použiť škálu</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="358"/>
        <source> deg</source>
        <translation> stupeň</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="164"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="359"/>
        <source>Low </source>
        <translation>Spodná </translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="171"/>
        <location filename="../platesolving.ui" line="194"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="360"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="363"/>
        <source>RA</source>
        <translation>RA</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="178"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="361"/>
        <source>Unit</source>
        <translation>Jednotka</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="188"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="362"/>
        <source>Solution</source>
        <translation>Riešenie</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="222"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="365"/>
        <source>Field width</source>
        <translation>Šírka poľa</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="236"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="366"/>
        <source>Field height</source>
        <translation>Výška poľa</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="250"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="367"/>
        <source>Orientation</source>
        <translation>Orientácia</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="264"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="368"/>
        <source>Pixel scale</source>
        <translation>Škála pixelu</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="278"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="369"/>
        <source>Stars</source>
        <translation>Počet hviezd</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="292"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="370"/>
        <source>HFR</source>
        <translation>HFR</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="318"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="371"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="325"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="372"/>
        <source>Extract</source>
        <translation>Extrahovať</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="332"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="373"/>
        <source>Solve</source>
        <translation>Vyriešiť</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="339"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="374"/>
        <source>Extract with HFR</source>
        <translation>Extrahovať s HFR</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="346"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="375"/>
        <source>Abort</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="353"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="376"/>
        <source>Update FITS header</source>
        <translation>Aktualizovať FITS hlavičku</translation>
    </message>
    <message>
        <location filename="../platesolving.cpp" line="152"/>
        <source>Header update failed</source>
        <translation>Header update failed</translation>
    </message>
</context>
<context>
    <name>PlateSolvingSettings</name>
    <message>
        <location filename="../platesolvingsettings.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="232"/>
        <source>Dialog</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="32"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="233"/>
        <source>Add</source>
        <translation>Pridať</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="39"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="234"/>
        <source>Remove</source>
        <translation>Odstrániť</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving need index files in order to solve an image. You can download them here by checking. This is possible only to default location.&lt;/p&gt;&lt;p&gt;It is required to download index files that cover 100%-50% field of view and recomended 100%-10%. So for images with 70&apos; field of view it is required to download index files in 30&apos;-85&apos; and recomended 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving potrebuje indexové súbory aby mohlo nájsť rišenie pre obrázok. Žašktrnutím políčka sa stiahnú indexové súbory pre danú škálu. Stiahne ich do východieho adresáru.&lt;/p&gt;&lt;p&gt;Pre správnú funknčnosť je nutné stiahnuť súbory v 100%-50% rozsahu zorného poľa a je odporúčané stiahnuť 100%-10%. Takže pre obrázky s 70&apos; zorným poľom je nutné stiahnuť indexové súbory v rozsahu 30&apos;-85&apos; a odporúča sa 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="48"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving need index files in order to solve an image. You can download them here by clicking on check box. It will download them into default location. Or you can reuse &lt;span style=&quot; font-weight:700;&quot;&gt;any&lt;/span&gt; index files (not only listed bellow) from astrometry.net by adding path pointing to them. &lt;a href=&quot;https://astrometrynet.readthedocs.io/en/latest/readme.html#getting-index-files&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;More details about index files.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;It is required to download index files that cover 100%-50% field of view and recomended 100%-10%. So for images with 70&apos; field of view it is required to download index files in 30&apos;-85&apos; and recomended 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving potrebuje indexové súbory aby mohlo nájsť rišenie pre obrázok. Môžete si stiahnuť indexové súbory kliknutím na zaškrtávacie políčko. Stiahne ich do východzieho adresáru. Taktiež môžete použiť hocijaké &lt;span style=&quot; font-weight:700;&quot;&gt;existujúce&lt;/span&gt; indexové súbory (nielen uvedené dole) z astrometry.net pridaním cesty k nim. &lt;a href=&quot;https://astrometrynet.readthedocs.io/en/latest/readme.html#getting-index-files&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Viacej informácii o indexových súboroch.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Pre správnú funknčnosť je nutné stiahnuť súbory v 100%-50% rozsahu zorného poľa a je odporúčané stiahnuť 100%-10%. Takže pre obrázky s 70&apos; zorným poľom je nutné stiahnuť indexové súbory v rozsahu 30&apos;-85&apos; a odporúča sa 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="61"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="236"/>
        <source>Index files</source>
        <translation>Indexové súbory</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="67"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="237"/>
        <source>240&apos; - 340&apos;index-4114.fits (1.4 MiB)</source>
        <translation>240&apos; - 340&apos;index-4114.fits (1.4 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="74"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="238"/>
        <source>680&apos; - 1000&apos; index-4117.fits (242 kiB)</source>
        <translation>680&apos; - 1000&apos; index-4117.fits (242 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="81"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="239"/>
        <source>480&apos; - 680&apos; index-4116.fits (400 kiB)</source>
        <translation>480&apos; - 680&apos; index-4116.fits (400 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="88"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="240"/>
        <source>1000&apos; - 1400&apos; index-4118.fits (183 kiB)</source>
        <translation>1000&apos; - 1400&apos; index-4118.fits (183 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="241"/>
        <source>340&apos; - 480&apos; index-4115.fits (723 kiB)</source>
        <translation>340&apos; - 480&apos; index-4115.fits (723 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="102"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="242"/>
        <source>1400&apos; - 2000&apos; index-4119.fits (141 kiB)</source>
        <translation>1400&apos; - 2000&apos; index-4119.fits (141 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="109"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="243"/>
        <source>120&apos; - 170&apos; index-4112.fits (5.1MiB)</source>
        <translation>120&apos; - 170&apos; index-4112.fits (5.1MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="116"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="244"/>
        <source>170&apos; - 240&apos; index-4113.fits (2.7MiB)</source>
        <translation>170&apos; - 240&apos; index-4113.fits (2.7MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="245"/>
        <source>85&apos; - 120&apos; index-4111.fits (9.8 MiB)</source>
        <translation>85&apos; - 120&apos; index-4111.fits (9.8 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="130"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="246"/>
        <source>60&apos; - 85&apos; index-4110.fits (24 MiB)</source>
        <translation>60&apos; - 85&apos; index-4110.fits (24 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="247"/>
        <source>42&apos; - 60&apos; index-4109.fits (48 MiB)</source>
        <translation>42&apos; - 60&apos; index-4109.fits (48 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="144"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="248"/>
        <source>30&apos; - 42&apos; index-4108.fits (91 MiB)</source>
        <translation>30&apos; - 42&apos; index-4108.fits (91 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="249"/>
        <source>22&apos; - 30&apos; index-4107.fits (158 MiB)</source>
        <translation>22&apos; - 30&apos; index-4107.fits (158 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="158"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="250"/>
        <source>16&apos; - 22&apos; index-5206-*.fits (294 MiB)</source>
        <translation>16&apos; - 22&apos; index-5206-*.fits (294 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="165"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="251"/>
        <source>11&apos; - 16&apos; index-5205-*.fits (587 MiB)</source>
        <translation>11&apos; - 16&apos; index-5205-*.fits (587 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="172"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="252"/>
        <source>8&apos; - 11&apos; index-5204-*.fits (1.2 GiB)</source>
        <translation>8&apos; - 11&apos; index-5204-*.fits (1.2 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="179"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="253"/>
        <source>4.0&apos; - 5.6&apos; index-5203-*.fits (2.3 GiB)</source>
        <translation>4.0&apos; - 5.6&apos; index-5203-*.fits (2.3 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="186"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="254"/>
        <source>5.6&apos; - 8.0&apos; index-5202-*.fits (4.6 GiB)</source>
        <translation>5.6&apos; - 8.0&apos; index-5202-*.fits (4.6 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="193"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="255"/>
        <source>2.0&apos; - 2.8&apos; index-5201-*.fits (8.9 GiB)</source>
        <translation>2.0&apos; - 2.8&apos; index-5201-*.fits (8.9 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="205"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="256"/>
        <source>0 files</source>
        <translation>0 súborov</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="219"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="257"/>
        <source>Stop download</source>
        <translation>Zastaviť sťahovanie</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="40"/>
        <source>Index files directory</source>
        <translation>Adresár s indexovými súbormi</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="133"/>
        <source>%1 files</source>
        <translation>%1 súborov</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../loadimage.cpp" line="371"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="372"/>
        <source>Shutter speed</source>
        <translation>Rýchlosť uzávierky</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="96"/>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <source>Unsupported sample format</source>
        <translation>Nepodporovaný formát</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="156"/>
        <location filename="../loadimage.cpp" line="224"/>
        <location filename="../loadimage.cpp" line="369"/>
        <location filename="../loadimage.cpp" line="404"/>
        <source>Width</source>
        <translation>Šírka</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="157"/>
        <location filename="../loadimage.cpp" line="225"/>
        <location filename="../loadimage.cpp" line="370"/>
        <location filename="../loadimage.cpp" line="405"/>
        <source>Height</source>
        <translation>Výška</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="32"/>
        <source>Filename</source>
        <translation>Meno súboru</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Failed to load image</source>
        <translation>Zlyhalo načítanie obrázka</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="52"/>
        <location filename="../loadrunable.cpp" line="62"/>
        <source>Mean</source>
        <translation>Priemer</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="53"/>
        <location filename="../loadrunable.cpp" line="63"/>
        <source>Standart deviation</source>
        <translation>Štandardná odchýlka</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="54"/>
        <location filename="../loadrunable.cpp" line="64"/>
        <source>Median</source>
        <translation>Medián</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="55"/>
        <location filename="../loadrunable.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="56"/>
        <location filename="../loadrunable.cpp" line="66"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="57"/>
        <location filename="../loadrunable.cpp" line="67"/>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="58"/>
        <location filename="../loadrunable.cpp" line="68"/>
        <source>Saturated</source>
        <translation>Saturované</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <location filename="../stfslider.cpp" line="41"/>
        <source>Press Shift for fine tuning</source>
        <translation>Stlačte Shift pre jemné ladenie</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <location filename="../databaseview.cpp" line="52"/>
        <source>Select columns</source>
        <translation>Výber stĺpcov</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.cpp" line="39"/>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="46"/>
        <source>How many images are preloaded before and after current image.</source>
        <translation>Koľko obrázkov sa prednačíta pred a za aktuálnym obrázkom.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="52"/>
        <source>Thumbnail size in pixels</source>
        <translation>Veľkosť náhľadu v pixeloch</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="83"/>
        <source>Image preload count</source>
        <translation>Počet prednačítaných obrázkov</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="84"/>
        <source>Thumbnails size</source>
        <translation>Veľkosť náhľadu</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="68"/>
        <source>Don&apos;t use native file dialog</source>
        <translation>Nepoužívať natívny súborový dialóg</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="59"/>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Nastavuje prahovú hodnotu ktorá sa považuje za saturovanú.
Pre RAW súbory možno treba nastaviť 22%</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="85"/>
        <source>Saturation</source>
        <translation>Saturované</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Nearest</source>
        <translation>Žiadna</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Linear</source>
        <translation>Lineárna</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Cubic</source>
        <translation>Kubická</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="75"/>
        <source>Smooth thumbnails</source>
        <translation>Hladké náhľady</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="77"/>
        <source>Use box filter when downsampling thumbnails instead of nearest. Slightly slower.</source>
        <translation>Pri zemnšovaní obrázkov na náhľady sa pixely spriemerujú namiesto najblžšej hodnoty. Trocha pomalšie.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="79"/>
        <source>Best Fit on image load</source>
        <translation>Najlepšia veľkosť pri otvorení obrázka</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="80"/>
        <source>Set Best Fit zoom level when opening new image.</source>
        <translation>Nastav najlepšiu veľkosť pri otvorení nového obrázku.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="86"/>
        <source>Slideshow interval</source>
        <translation>Interval medzi obrázkami</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="87"/>
        <source>Image interpolation</source>
        <translation>Interpolácia obrázku</translation>
    </message>
</context>
<context>
    <name>Solver</name>
    <message>
        <location filename="../solver.cpp" line="77"/>
        <source>Unsupported image data type</source>
        <translation>Nepodporovaný dátový typ obrázka</translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="157"/>
        <source>Solving is not finished</source>
        <translation>Riešenie nie je dokončené</translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="190"/>
        <source>Failed to update file header</source>
        <translation>Zlyhalo aktualizovanie hlavičky súboru</translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <location filename="../stretchtoolbar.cpp" line="20"/>
        <source>Stretch toolbar</source>
        <translation>Panel úrovní</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="74"/>
        <source>Auto Stretch F12</source>
        <translation>Automatické natiahnutie F12</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="78"/>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Resetuj funkciu prevodu na obrazovku F11</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="82"/>
        <source>Invert colors</source>
        <translation>Invertuj farby</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="94"/>
        <source>Apply auto stretch on load</source>
        <translation>Aplikuj automatické natiahnutie pri načítaní</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="90"/>
        <source>Debayer CFA</source>
        <translation>Preveď CFA na farbu</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="86"/>
        <source>False colors</source>
        <translation>Falošné farby</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="69"/>
        <source>Linked channels</source>
        <translation>Prepojené kanály</translation>
    </message>
</context>
</TS>
