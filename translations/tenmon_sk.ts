<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sk_SK" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <source>About Tenmon</source>
        <translation>O Tenmon</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <source>Select columns</source>
        <translation>Vyber stĺpce</translation>
    </message>
    <message>
        <source>Text to search, you can % as wildcard</source>
        <translation>Text na vyhľadanie, môžete použit % ako zástupný znak</translation>
    </message>
    <message>
        <source>Filter</source>
        <translatorcomment>Meno súboru</translatorcomment>
        <translation>Filter</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <source>Mark</source>
        <translation>Označiť</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Odznačiť</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <source>File name</source>
        <translation>Meno súboru</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <source>Sort by filename</source>
        <translation>Zoradiť podľa názvu súboru</translation>
    </message>
    <message>
        <source>Sort by time</source>
        <translation>Zoradiť podľa času</translation>
    </message>
    <message>
        <source>Sort by size</source>
        <translation>Zoradiť podľa veľkosti</translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation>Zoradiť podľa typu</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation>Otočit poradie</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <source>Open</source>
        <translation>Otvoriť</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Skopírovať označené súbory</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Presunúť označené súbory</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Indexovať adresár</translation>
    </message>
    <message>
        <source>Set as root</source>
        <translation>Nastav koreňový adresár</translation>
    </message>
    <message>
        <source>Reset root</source>
        <translation>Resetuj koreňový adresár</translation>
    </message>
    <message>
        <source>Go up</source>
        <translation>O úroveň vyššie</translation>
    </message>
    <message>
        <source>Show hidden files</source>
        <translation>Zobraz skryté súbory</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <source>Logarithmic scale</source>
        <translation>Logaritmická stupnica</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <source>Property</source>
        <translation>Vlastnosť</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <source>FITS Header</source>
        <translation>FITS hlavička</translation>
    </message>
    <message>
        <source>Image info</source>
        <translation>Informácie o obrázku</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <source>Name</source>
        <translation>Meno</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation>OpenGL chyba</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Nepodarilo sa incializovať OpenGL 3.3 context. Ubezpečte sa že sú nainštalované ovládače pre GPU.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Image info</source>
        <translation>Informácie o obrázku</translation>
    </message>
    <message>
        <source>Can&apos;t open DB</source>
        <translation>Nie je možné otvoriť DB</translation>
    </message>
    <message>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Nie je možné otvoriť SQLITE databázu</translation>
    </message>
    <message>
        <source>Filesystem</source>
        <translation>Zoznam súborov</translation>
    </message>
    <message>
        <source>Tenmon</source>
        <translation>Tenmon</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Súbor</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otvoriť</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Skopírovať označené súbory</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Uložiť ako</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Ukončiť</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Zobrazenie</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Priblížiť</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Oddialiť</translation>
    </message>
    <message>
        <source>Best Fit</source>
        <translation>Najlepšia veľkosť</translation>
    </message>
    <message>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Výber</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Označiť</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Odznačiť</translation>
    </message>
    <message>
        <source>Mark and next</source>
        <translation>Označiť a ďaľší</translation>
    </message>
    <message>
        <source>Unmark and next</source>
        <translation>Odznačiť a ďaľší</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation>Analýza</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation>Štatistiky obrázka</translation>
    </message>
    <message>
        <source>Docks</source>
        <translation>Dokovacie panely</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message>
        <source>Select destination</source>
        <translation>Vybrať cieľ</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation>Kopírovanie</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Presunúť označené súbory</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Indexovať adresár</translation>
    </message>
    <message>
        <source>Live mode</source>
        <translation>Živý mód</translation>
    </message>
    <message>
        <source>Thumbnails</source>
        <translation>Náhľady</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation>Ukázať označené</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation>Vyhľadávač vrcholov</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>About Tenmon</source>
        <translation>O Tenmon</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation>Presúvanie</translation>
    </message>
    <message>
        <source>Indexing FITS files</source>
        <translation>Indexovanie FITS/XISF súborov</translation>
    </message>
    <message>
        <source>Reindex files</source>
        <translation>Reindexuj súbory</translation>
    </message>
    <message>
        <source>FITS/XISF files database</source>
        <translation>Databáza FITS/XISF súborov</translation>
    </message>
    <message>
        <source>File tree</source>
        <translation>Strom súborov</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation>Vyhľadávač hviezd</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Upraviť</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <source>Images (</source>
        <translation>Obrázky (</translation>
    </message>
    <message>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>Obrázok FITS (*.fits *.fit);;Obrázok XISF (*.xisf);;</translation>
    </message>
    <message>
        <source>Failed to copy</source>
        <translation>Zlyhalo kopírovanie</translation>
    </message>
    <message>
        <source>Failed to move</source>
        <translation>Zlyhalo presúvanie</translation>
    </message>
    <message>
        <source>Failed to move from %1 to %2</source>
        <translation>Zlyhalo presúvanie z %1 do %2</translation>
    </message>
    <message>
        <source>Failed to copy from %1 to %2</source>
        <translation>Zlyhalo kopírovanie z %1 do %2</translation>
    </message>
    <message>
        <source>;;All files (*)</source>
        <translation>;;Všetky súbory (*)</translation>
    </message>
    <message>
        <source>Move files to trash?</source>
        <translation>Presunúť súbory do koša?</translation>
    </message>
    <message>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Presunúť %1 súborov do koša?</translation>
    </message>
    <message>
        <source>Failed to move file to trash</source>
        <translation>Zlyhalo presunutie súbora do koša</translation>
    </message>
    <message>
        <source>Failed to move file to trash %1</source>
        <translation>Zlyhalo presunutie súbora do koša %1</translation>
    </message>
    <message>
        <source>Move marked files to trash</source>
        <translation>Presunúť označené súbory do koša</translation>
    </message>
    <message>
        <source>Moving marked files to trash</source>
        <translation>Presúvanie do koša</translation>
    </message>
    <message>
        <source>Export database to CSV</source>
        <translation>Exportovať databázu do CSV súboru</translation>
    </message>
    <message>
        <source>CSV file (*.csv)</source>
        <translation>Súbory CSV (*.csv)</translation>
    </message>
    <message>
        <source>Histogram</source>
        <translation>Histogram</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <source>Marked files</source>
        <translation>Označené súbory</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Meno súboru</translation>
    </message>
    <message>
        <source>Clear selected</source>
        <translation>Zrušiť vybrané</translation>
    </message>
    <message>
        <source>Clear all</source>
        <translation>Zrušiť všetky</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <source>Shutter speed</source>
        <translation>Rýchlosť uzávierky</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <source>Unsupported sample format</source>
        <translation>Nepodporovaný formát</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Šírka</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Výška</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Meno súboru</translation>
    </message>
    <message>
        <source>Mean</source>
        <translation>Priemer</translation>
    </message>
    <message>
        <source>Standart deviation</source>
        <translation>Štandardná odchýlka</translation>
    </message>
    <message>
        <source>Median</source>
        <translation>Medián</translation>
    </message>
    <message>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <source>Saturated</source>
        <translation>Saturované</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <source>Press Shift for fine tuning</source>
        <translation>Stlačte Shift pre jemné ladenie</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <source>Select columns</source>
        <translation>Výber stĺpcov</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Settings</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <source>How many images are preloaded before and after current image.</source>
        <translation>Koľko obrázkov sa prednačíta pred a za aktuálnym obrázkom.</translation>
    </message>
    <message>
        <source>Thumbnail size in pixels</source>
        <translation>Veľkosť náhľadu v pixeloch</translation>
    </message>
    <message>
        <source>Image preload count</source>
        <translation>Počet prednačítaných obrázkov</translation>
    </message>
    <message>
        <source>Thumbnails size</source>
        <translation>Veľkosť náhľadu</translation>
    </message>
    <message>
        <source>Don&apos;t use native file dialog</source>
        <translation>Nepoužívať natívny súborový dialóg</translation>
    </message>
    <message>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Nastavuje prahovú hodnotu ktorá sa považuje za saturovanú.
Pre RAW súbory možno treba nastaviť 22%</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Saturované</translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <source>Stretch toolbar</source>
        <translation>Panel úrovní</translation>
    </message>
    <message>
        <source>Auto Stretch F12</source>
        <translation>Automatické natiahnutie F12</translation>
    </message>
    <message>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Resetuj funkciu prevodu na obrazovku F11</translation>
    </message>
    <message>
        <source>Invert colors</source>
        <translation>Invertuj farby</translation>
    </message>
    <message>
        <source>Apply auto stretch on load</source>
        <translation>Aplikuj automatické natiahnutie pri načítaní</translation>
    </message>
    <message>
        <source>Debayer CFA</source>
        <translation>Preveď CFA na farbu</translation>
    </message>
    <message>
        <source>False colors</source>
        <translation>Falošné farby</translation>
    </message>
    <message>
        <source>Linked channels</source>
        <translation>Prepojené kanály</translation>
    </message>
</context>
</TS>
