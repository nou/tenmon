<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <source>About Tenmon</source>
        <translation>A propos de Tenmon</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <source>Select columns</source>
        <translation>Choix des colonnes</translation>
    </message>
    <message>
        <source>Text to search, you can % as wildcard</source>
        <translation>Texte à chercher, utilisez % comme caractère générique</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <source>Mark</source>
        <translation>Marquer</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Décocher</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <source>File name</source>
        <translation>Nom de fichier</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <source>Sort by filename</source>
        <translation>Trier par nom de fichier</translation>
    </message>
    <message>
        <source>Sort by time</source>
        <translation>Trier par heure</translation>
    </message>
    <message>
        <source>Sort by size</source>
        <translation>Trier par taille</translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation>Trier par type</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation>Ordre inverse</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Copier les fichiers marqués</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Déplacer les fichiers marqués</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Indexer le répertoire</translation>
    </message>
    <message>
        <source>Set as root</source>
        <translation>Définir comme répertoire racine</translation>
    </message>
    <message>
        <source>Reset root</source>
        <translation>Réinitialiser la racine</translation>
    </message>
    <message>
        <source>Go up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <source>Show hidden files</source>
        <translation>Afficher les fichiers cachés</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <source>Logarithmic scale</source>
        <translation>Échelle logarithmique</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <source>Property</source>
        <translation>Propriété</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <source>FITS Header</source>
        <translation>En-tête FITS</translation>
    </message>
    <message>
        <source>Image info</source>
        <translation>Informations sur l&apos;image</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation>Erreur OpenGL</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Impossible d&apos;initialiser le contexte OpenGL 3.3. Assurez-vous que le pilote GPU approprié est installé.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Image info</source>
        <translation>Information sur l&apos;image</translation>
    </message>
    <message>
        <source>Can&apos;t open DB</source>
        <translation>Ne peut ouvrir la base de donnée</translation>
    </message>
    <message>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Ne peut ouvrir la base de donnée SQLITE</translation>
    </message>
    <message>
        <source>Filesystem</source>
        <translation>Système de fichier</translation>
    </message>
    <message>
        <source>Tenmon</source>
        <translation>Tenmon</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Copier les fichiers marqués</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <source>Live mode</source>
        <translation>Mode temps réel</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Voir</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <source>Best Fit</source>
        <translation>Meilleur ajustement</translation>
    </message>
    <message>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Marquer</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Décocher</translation>
    </message>
    <message>
        <source>Mark and next</source>
        <translation>Marquer et suivant</translation>
    </message>
    <message>
        <source>Unmark and next</source>
        <translation>Décocher et suivant</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation>Statistiques de l&apos;image</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation>Détecteur de pic</translation>
    </message>
    <message>
        <source>Docks</source>
        <translation>Fenêtres encrables</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Ouvrir le ficher</translation>
    </message>
    <message>
        <source>Select destination</source>
        <translation>Choisir la destination</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation>Copier</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Déplacer les fichiers marqués</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Indexer le répertoire</translation>
    </message>
    <message>
        <source>Thumbnails</source>
        <translation>Vignettes</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation>Afficher marqué</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>About Tenmon</source>
        <translation>A propos de Tenmon</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>A propos de Qt</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <source>Indexing FITS files</source>
        <translation>Indexation des fichiers FITS</translation>
    </message>
    <message>
        <source>Reindex files</source>
        <translation>Ré-indexer les fichiers</translation>
    </message>
    <message>
        <source>FITS/XISF files database</source>
        <translation>Base de donnée FITS/XISF</translation>
    </message>
    <message>
        <source>File tree</source>
        <translation>Arborescence de fichiers</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation>Détecteur d&apos;étoiles</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>Images (</source>
        <translation>Images (</translation>
    </message>
    <message>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>FITS image (*.fits *.fit);;XISF image (*.xisf);;</translation>
    </message>
    <message>
        <source>Failed to copy</source>
        <translation>Échec de la copie</translation>
    </message>
    <message>
        <source>Failed to move</source>
        <translation>Échec du déplacement</translation>
    </message>
    <message>
        <source>Failed to move from %1 to %2</source>
        <translation>Échec du déplacement de %1 vers %2</translation>
    </message>
    <message>
        <source>Failed to copy from %1 to %2</source>
        <translation>Échec de la copie de %1 vers %2</translation>
    </message>
    <message>
        <source>;;All files (*)</source>
        <translation>;;Tout les fichiers (*)</translation>
    </message>
    <message>
        <source>Move files to trash?</source>
        <translation>Déplacer les fichiers dans la corbeille?</translation>
    </message>
    <message>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Voulez-vous déplacer le fichier %1 dans la corbeille?</translation>
    </message>
    <message>
        <source>Failed to move file to trash</source>
        <translation>Echec du déplacement dans la corbeille</translation>
    </message>
    <message>
        <source>Failed to move file to trash %1</source>
        <translation>Echec du déplacement de %1 dans la corbeille</translation>
    </message>
    <message>
        <source>Move marked files to trash</source>
        <translation>Déplacer les fichiers marqués dans la corbeille</translation>
    </message>
    <message>
        <source>Moving marked files to trash</source>
        <translation>Déplacement des fichiers marqués dans la corbeille</translation>
    </message>
    <message>
        <source>Export database to CSV</source>
        <translation>Exporter la base de données vers un fichier CSV</translation>
    </message>
    <message>
        <source>CSV file (*.csv)</source>
        <translation>Fichiers CSV (*.csv)</translation>
    </message>
    <message>
        <source>Histogram</source>
        <translation>Histogramme</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <source>Marked files</source>
        <translation>Fichiers marqués</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <source>Clear selected</source>
        <translation>Effacer la sélection</translation>
    </message>
    <message>
        <source>Clear all</source>
        <translation>Effacer tout</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <source>Shutter speed</source>
        <translation>Vitesse d&apos;obturation</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <source>Mean</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <source>Standart deviation</source>
        <translation>Écart-type</translation>
    </message>
    <message>
        <source>Median</source>
        <translation>Médiane</translation>
    </message>
    <message>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <source>Unsupported sample format</source>
        <translation>Format non pris en charge</translation>
    </message>
    <message>
        <source>Saturated</source>
        <translation>Saturé</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <source>Press Shift for fine tuning</source>
        <translation>Appuyez sur Shift pour un réglage fin</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <source>Select columns</source>
        <translation>Choix des colonnes</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>How many images are preloaded before and after current image.</source>
        <translation>Combien d&apos;images sont préchargées avant et après l&apos;image courante.</translation>
    </message>
    <message>
        <source>Thumbnail size in pixels</source>
        <translation>Taille des vignettes en pixels</translation>
    </message>
    <message>
        <source>Image preload count</source>
        <translation>Nombre d&apos;images préchargées</translation>
    </message>
    <message>
        <source>Thumbnails size</source>
        <translation>Taille des vignette</translation>
    </message>
    <message>
        <source>Don&apos;t use native file dialog</source>
        <translation>N&apos;utilisez pas la boîte de dialogue de fichier natif</translation>
    </message>
    <message>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Définissez la valeur seuil qui est considérée comme saturée lors de l&apos;affichage des statistiques.
Pour les fichiers RAW, vous pouvez définir 22&#xa0;%</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Saturé</translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <source>Stretch toolbar</source>
        <translation>Réglage de la luminosité</translation>
    </message>
    <message>
        <source>Auto Stretch F12</source>
        <translation>Luminosité automatique F12</translation>
    </message>
    <message>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Réinitialiser la fonction de transfert d&apos;écran F11</translation>
    </message>
    <message>
        <source>Invert colors</source>
        <translation>Inverser les couleurs</translation>
    </message>
    <message>
        <source>Apply auto stretch on load</source>
        <translation>Appliquer la luminosité automatiquement au chargement</translation>
    </message>
    <message>
        <source>Debayer CFA</source>
        <translation>Débayeriser CFA</translation>
    </message>
    <message>
        <source>False colors</source>
        <translation>Fausses couleurs</translation>
    </message>
    <message>
        <source>Linked channels</source>
        <translation>Chaînes liées</translation>
    </message>
</context>
</TS>
