<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="12"/>
        <source>About Tenmon</source>
        <translation>A propos de Tenmon</translation>
    </message>
</context>
<context>
    <name>BatchProcessing</name>
    <message>
        <location filename="../batchprocessing.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="208"/>
        <source>Batch Processing</source>
        <translation>Traitement par lot</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="22"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="209"/>
        <source>Input files and directories</source>
        <translation>Fichiers et répertoires d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="44"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="210"/>
        <source>Add files</source>
        <translation>Ajout de fichiers</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="51"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="211"/>
        <source>Add directories</source>
        <translation>Ajout de répertoires</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="58"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="212"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="213"/>
        <source>Remove all</source>
        <translation>Supprimer tout</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="78"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="214"/>
        <source>Output directory</source>
        <translation>Répertoire de sortie</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="92"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="215"/>
        <source>Browse</source>
        <translation>Naviger</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="103"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="216"/>
        <source>Scripts</source>
        <translation>Scripts</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="217"/>
        <source>Open scripts</source>
        <translation>Ouvrir les scripts</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="142"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="218"/>
        <source>Log</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="182"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="219"/>
        <source>Start script</source>
        <translation>Lancer le script</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="192"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="220"/>
        <source>Stop script</source>
        <translation>Arrêter le script</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="199"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="221"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="123"/>
        <source>Interrupt running script?</source>
        <translation>Interrompre l&apos;execution du script?</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="143"/>
        <source>Select files</source>
        <translation>Choisir les fichiers</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="154"/>
        <source>Select directory</source>
        <translation>Choisir le répertoire</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="175"/>
        <source>Select output directory</source>
        <translation>Choisir le répertoire de sortie</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Invalid output directory</source>
        <translation>Répertoire invalide</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Output directory path doesn&apos;t exist or is not writable</source>
        <translation>Le répertoire de sortie n&apos;existe pas ou ne peut pas être écrit</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="257"/>
        <source>Enter text</source>
        <translation>Entrer le texte</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="264"/>
        <source>Enter integer number</source>
        <translation>Entrer un nombre entier</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="271"/>
        <source>Enter float number</source>
        <translation>Entrer un nombre flottant</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="278"/>
        <source>Select item</source>
        <translation>Choisir l&apos;élément</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <location filename="../databaseview.cpp" line="255"/>
        <source>Select columns</source>
        <translation>Choix des colonnes</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="294"/>
        <source>Text to search, you can % as wildcard</source>
        <translation>Texte à chercher, utilisez % comme caractère générique</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="310"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <location filename="../databaseview.cpp" line="215"/>
        <source>Mark</source>
        <translation>Marquer</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="216"/>
        <source>Unmark</source>
        <translation>Décocher</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <location filename="../databaseview.cpp" line="194"/>
        <source>File name</source>
        <translation>Nom de fichier</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="26"/>
        <source>Sort by filename</source>
        <translation>Trier par nom de fichier</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="27"/>
        <source>Sort by time</source>
        <translation>Trier par heure</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="28"/>
        <source>Sort by size</source>
        <translation>Trier par taille</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="29"/>
        <source>Sort by type</source>
        <translation>Trier par type</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="30"/>
        <source>Reverse</source>
        <translation>Ordre inverse</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="90"/>
        <location filename="../filesystemwidget.cpp" line="94"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="96"/>
        <source>Copy marked files</source>
        <translation>Copier les fichiers marqués</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="97"/>
        <source>Move marked files</source>
        <translation>Déplacer les fichiers marqués</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="98"/>
        <source>Index directory</source>
        <translation>Indexer le répertoire</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="95"/>
        <source>Set as root</source>
        <translation>Définir comme répertoire racine</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="102"/>
        <source>Reset root</source>
        <translation>Réinitialiser la racine</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="103"/>
        <source>Go up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="104"/>
        <source>Show hidden files</source>
        <translation>Afficher les fichiers cachés</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../about.cpp" line="33"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <location filename="../histogram.cpp" line="73"/>
        <source>Logarithmic scale</source>
        <translation>Échelle logarithmique</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Property</source>
        <translation>Propriété</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="25"/>
        <source>FITS Header</source>
        <translation>En-tête FITS</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="34"/>
        <source>Image info</source>
        <translation>Informations sur l&apos;image</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <location filename="../imageringlist.cpp" line="406"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation type="vanished">Erreur OpenGL</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation type="vanished">Impossible d&apos;initialiser le contexte OpenGL 3.3. Assurez-vous que le pilote GPU approprié est installé.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation type="vanished">L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation type="vanished">X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation type="vanished">R:%1 G:%2 B:%3</translation>
    </message>
    <message>
        <source>Failed to load image</source>
        <translation type="vanished">Échec du chargement de l&apos;image</translation>
    </message>
</context>
<context>
    <name>ImageWidgetGL</name>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>OpenGL error</source>
        <translation>Erreur OpenGL</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Impossible d&apos;initialiser le contexte OpenGL 3.3. Assurez-vous que le pilote GPU approprié est installé.</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="109"/>
        <source>Failed to load image</source>
        <translation>Échec du chargement de l&apos;image</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="71"/>
        <source>Image info</source>
        <translation>Information sur l&apos;image</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open DB</source>
        <translation>Ne peut ouvrir la base de donnée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Ne peut ouvrir la base de donnée SQLITE</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <source>Filesystem</source>
        <translation>Système de fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Tenmon</source>
        <translation>Tenmon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="157"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="162"/>
        <source>Copy marked files</source>
        <translation>Copier les fichiers marqués</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <location filename="../mainwindow.cpp" line="614"/>
        <location filename="../mainwindow.cpp" line="776"/>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Open directory recursively</source>
        <translation>Ouvrir le répertoire de manière récursive</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>Batch processing</source>
        <translation>Traitement par lot</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Live mode</source>
        <translation>Mode temps réel</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="177"/>
        <source>Exit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="185"/>
        <source>View</source>
        <translation>Voir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="186"/>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="187"/>
        <source>Zoom Out</source>
        <translation>Zoom arrière</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="188"/>
        <source>Best Fit</source>
        <translation>Meilleur ajustement</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="212"/>
        <source>Colormap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="219"/>
        <source>User %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="245"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="246"/>
        <source>Mark</source>
        <translation>Marquer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="247"/>
        <source>Unmark</source>
        <translation>Décocher</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="249"/>
        <source>Mark and next</source>
        <translation>Marquer et suivant</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="250"/>
        <source>Unmark and next</source>
        <translation>Décocher et suivant</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Overwrite file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Destination file %1 already exists. Overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>Missing marked files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>%1 marked files were missing. They were skipped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <location filename="../mainwindow.cpp" line="801"/>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Update check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <source>You have newest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="801"/>
        <source>New version %1 is available. Do you want to download it now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Failed to check version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analyse</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation type="vanished">Statistiques de l&apos;image</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation type="vanished">Détecteur de pic</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Docks</source>
        <translation>Fenêtres encrables</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open file</source>
        <translation>Ouvrir le ficher</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="427"/>
        <source>Select destination</source>
        <translation>Choisir la destination</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Copying</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Cancel</source>
        <translation>Abandon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="163"/>
        <source>Move marked files</source>
        <translation>Déplacer les fichiers marqués</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="589"/>
        <source>Index directory</source>
        <translation>Indexer le répertoire</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>Thumbnails</source>
        <translation>Vignettes</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation type="vanished">Afficher marqué</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="294"/>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="296"/>
        <source>About Tenmon</source>
        <translation>A propos de Tenmon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="297"/>
        <source>About Qt</source>
        <translation>A propos de Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Moving</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <source>Indexing FITS files</source>
        <translation>Indexation des fichiers FITS</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>Reindex files</source>
        <translation>Ré-indexer les fichiers</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="124"/>
        <source>FITS/XISF files database</source>
        <translation>Base de donnée FITS/XISF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>File tree</source>
        <translation>Arborescence de fichiers</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation type="vanished">Détecteur d&apos;étoiles</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="182"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Images (</source>
        <translation>Images (</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="51"/>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>FITS image (*.fits *.fit);;XISF image (*.xisf);;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Show marked list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="253"/>
        <source>Open marked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Check for update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to copy</source>
        <translation>Échec de la copie</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to move</source>
        <translation>Échec du déplacement</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to move from %1 to %2</source>
        <translation>Échec du déplacement de %1 vers %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to copy from %1 to %2</source>
        <translation>Échec de la copie de %1 vers %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>;;All files (*)</source>
        <translation>;;Tout les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Move files to trash?</source>
        <translation>Déplacer les fichiers dans la corbeille?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Voulez-vous déplacer le fichier %1 dans la corbeille?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash</source>
        <translation>Echec du déplacement dans la corbeille</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash %1</source>
        <translation>Echec du déplacement de %1 dans la corbeille</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="164"/>
        <source>Move marked files to trash</source>
        <translation>Déplacer les fichiers marqués dans la corbeille</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Moving marked files to trash</source>
        <translation>Déplacement des fichiers marqués dans la corbeille</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="168"/>
        <source>Export database to CSV</source>
        <translation>Exporter la base de données vers un fichier CSV</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>CSV file (*.csv)</source>
        <translation>Fichiers CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source>Histogram</source>
        <translation>Histogramme</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>Bayer mask</source>
        <translation>Masque Bayer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="193"/>
        <source>RGGB</source>
        <translation>RGGB</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="194"/>
        <source>GRBG</source>
        <translation>GRBG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="195"/>
        <source>GBRG</source>
        <translation>GBRG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="196"/>
        <source>BGGR</source>
        <translation>BGGR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Slideshow</source>
        <translation>Diaporama</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <location filename="../markedfiles.cpp" line="11"/>
        <source>Marked files</source>
        <translation>Fichiers marqués</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="22"/>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="30"/>
        <source>Clear selected</source>
        <translation>Effacer la sélection</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="31"/>
        <source>Clear all</source>
        <translation>Effacer tout</translation>
    </message>
</context>
<context>
    <name>PlateSolving</name>
    <message>
        <location filename="../platesolving.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="345"/>
        <source>Plate Solving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="23"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="346"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="35"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="347"/>
        <source>Start point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="42"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="348"/>
        <source>Degree width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="47"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="349"/>
        <source>Arcmin width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="52"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="350"/>
        <source>Arcsec per pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="57"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="351"/>
        <source>35 mm equivalent focal length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="353"/>
        <source>Use position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="88"/>
        <location filename="../platesolving.ui" line="208"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="354"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="364"/>
        <source>DEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="355"/>
        <source> h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="111"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="356"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="357"/>
        <source>Use scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="358"/>
        <source> deg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="164"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="359"/>
        <source>Low </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="171"/>
        <location filename="../platesolving.ui" line="194"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="360"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="363"/>
        <source>RA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="178"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="361"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="188"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="362"/>
        <source>Solution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="222"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="365"/>
        <source>Field width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="236"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="366"/>
        <source>Field height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="250"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="367"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="264"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="368"/>
        <source>Pixel scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="278"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="369"/>
        <source>Stars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="292"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="370"/>
        <source>HFR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="318"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="371"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="325"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="372"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="332"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="373"/>
        <source>Solve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="339"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="374"/>
        <source>Extract with HFR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="346"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="375"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="353"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="376"/>
        <source>Update FITS header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolving.cpp" line="152"/>
        <source>Header update failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlateSolvingSettings</name>
    <message>
        <location filename="../platesolvingsettings.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="232"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="32"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="233"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="39"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="234"/>
        <source>Remove</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="48"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving need index files in order to solve an image. You can download them here by clicking on check box. It will download them into default location. Or you can reuse &lt;span style=&quot; font-weight:700;&quot;&gt;any&lt;/span&gt; index files (not only listed bellow) from astrometry.net by adding path pointing to them. &lt;a href=&quot;https://astrometrynet.readthedocs.io/en/latest/readme.html#getting-index-files&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;More details about index files.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;It is required to download index files that cover 100%-50% field of view and recomended 100%-10%. So for images with 70&apos; field of view it is required to download index files in 30&apos;-85&apos; and recomended 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="61"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="236"/>
        <source>Index files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="67"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="237"/>
        <source>240&apos; - 340&apos;index-4114.fits (1.4 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="74"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="238"/>
        <source>680&apos; - 1000&apos; index-4117.fits (242 kiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="81"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="239"/>
        <source>480&apos; - 680&apos; index-4116.fits (400 kiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="88"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="240"/>
        <source>1000&apos; - 1400&apos; index-4118.fits (183 kiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="241"/>
        <source>340&apos; - 480&apos; index-4115.fits (723 kiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="102"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="242"/>
        <source>1400&apos; - 2000&apos; index-4119.fits (141 kiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="109"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="243"/>
        <source>120&apos; - 170&apos; index-4112.fits (5.1MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="116"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="244"/>
        <source>170&apos; - 240&apos; index-4113.fits (2.7MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="245"/>
        <source>85&apos; - 120&apos; index-4111.fits (9.8 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="130"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="246"/>
        <source>60&apos; - 85&apos; index-4110.fits (24 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="247"/>
        <source>42&apos; - 60&apos; index-4109.fits (48 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="144"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="248"/>
        <source>30&apos; - 42&apos; index-4108.fits (91 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="249"/>
        <source>22&apos; - 30&apos; index-4107.fits (158 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="158"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="250"/>
        <source>16&apos; - 22&apos; index-5206-*.fits (294 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="165"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="251"/>
        <source>11&apos; - 16&apos; index-5205-*.fits (587 MiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="172"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="252"/>
        <source>8&apos; - 11&apos; index-5204-*.fits (1.2 GiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="179"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="253"/>
        <source>4.0&apos; - 5.6&apos; index-5203-*.fits (2.3 GiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="186"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="254"/>
        <source>5.6&apos; - 8.0&apos; index-5202-*.fits (4.6 GiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="193"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="255"/>
        <source>2.0&apos; - 2.8&apos; index-5201-*.fits (8.9 GiB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="205"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="256"/>
        <source>0 files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="219"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="257"/>
        <source>Stop download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="40"/>
        <source>Index files directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="133"/>
        <source>%1 files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../loadimage.cpp" line="371"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="372"/>
        <source>Shutter speed</source>
        <translation>Vitesse d&apos;obturation</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="156"/>
        <location filename="../loadimage.cpp" line="224"/>
        <location filename="../loadimage.cpp" line="369"/>
        <location filename="../loadimage.cpp" line="404"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="157"/>
        <location filename="../loadimage.cpp" line="225"/>
        <location filename="../loadimage.cpp" line="370"/>
        <location filename="../loadimage.cpp" line="405"/>
        <source>Height</source>
        <translation>Hauteur</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="96"/>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="32"/>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Failed to load image</source>
        <translation>Échec du chargement de l&apos;image</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="52"/>
        <location filename="../loadrunable.cpp" line="62"/>
        <source>Mean</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="53"/>
        <location filename="../loadrunable.cpp" line="63"/>
        <source>Standart deviation</source>
        <translation>Écart-type</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="54"/>
        <location filename="../loadrunable.cpp" line="64"/>
        <source>Median</source>
        <translation>Médiane</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="55"/>
        <location filename="../loadrunable.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="56"/>
        <location filename="../loadrunable.cpp" line="66"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="57"/>
        <location filename="../loadrunable.cpp" line="67"/>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <source>Unsupported sample format</source>
        <translation>Format non pris en charge</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="58"/>
        <location filename="../loadrunable.cpp" line="68"/>
        <source>Saturated</source>
        <translation>Saturé</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <location filename="../stfslider.cpp" line="41"/>
        <source>Press Shift for fine tuning</source>
        <translation>Appuyez sur Shift pour un réglage fin</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <location filename="../databaseview.cpp" line="52"/>
        <source>Select columns</source>
        <translation>Choix des colonnes</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.cpp" line="39"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="46"/>
        <source>How many images are preloaded before and after current image.</source>
        <translation>Combien d&apos;images sont préchargées avant et après l&apos;image courante.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="52"/>
        <source>Thumbnail size in pixels</source>
        <translation>Taille des vignettes en pixels</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="83"/>
        <source>Image preload count</source>
        <translation>Nombre d&apos;images préchargées</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="84"/>
        <source>Thumbnails size</source>
        <translation>Taille des vignette</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="68"/>
        <source>Don&apos;t use native file dialog</source>
        <translation>N&apos;utilisez pas la boîte de dialogue de fichier natif</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="59"/>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Définissez la valeur seuil qui est considérée comme saturée lors de l&apos;affichage des statistiques.
Pour les fichiers RAW, vous pouvez définir 22&#xa0;%</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="85"/>
        <source>Saturation</source>
        <translation>Saturé</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Nearest</source>
        <translation>Voisin le plus proche</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Linear</source>
        <translation>Linéaire</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Cubic</source>
        <translation>Cubique</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="75"/>
        <source>Smooth thumbnails</source>
        <translation>Miniatures fluides</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="77"/>
        <source>Use box filter when downsampling thumbnails instead of nearest. Slightly slower.</source>
        <translation>Utilisez un filtre boîte lors du sous-échantillonnage des vignettes au lieu des plus proches. Un peu plus lent.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="79"/>
        <source>Best Fit on image load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="80"/>
        <source>Set Best Fit zoom level when opening new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="86"/>
        <source>Slideshow interval</source>
        <translation>Intervalle du diaporama</translation>
    </message>
    <message>
        <source>Image filtering</source>
        <translation type="obsolete">Échantillonnage d&apos;images</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="87"/>
        <source>Image interpolation</source>
        <translation>Interpolation d&apos;images</translation>
    </message>
</context>
<context>
    <name>Solver</name>
    <message>
        <location filename="../solver.cpp" line="77"/>
        <source>Unsupported image data type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="157"/>
        <source>Solving is not finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="190"/>
        <source>Failed to update file header</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <location filename="../stretchtoolbar.cpp" line="20"/>
        <source>Stretch toolbar</source>
        <translation>Réglage de la luminosité</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="74"/>
        <source>Auto Stretch F12</source>
        <translation>Luminosité automatique F12</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="78"/>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Réinitialiser la fonction de transfert d&apos;écran F11</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="82"/>
        <source>Invert colors</source>
        <translation>Inverser les couleurs</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="94"/>
        <source>Apply auto stretch on load</source>
        <translation>Appliquer la luminosité automatiquement au chargement</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="90"/>
        <source>Debayer CFA</source>
        <translation>Débayeriser CFA</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="86"/>
        <source>False colors</source>
        <translation>Fausses couleurs</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="69"/>
        <source>Linked channels</source>
        <translation>Canaux liés</translation>
    </message>
</context>
</TS>
