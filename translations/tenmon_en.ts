<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <source>About Tenmon</source>
        <translation>About Tenmon</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <source>Select columns</source>
        <translation>Select columns</translation>
    </message>
    <message>
        <source>Text to search, you can % as wildcard</source>
        <translation>Text to search, you can % as wildcard</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <source>Mark</source>
        <translation>Mark</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Unmark</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <source>File name</source>
        <translation>File name</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <source>Sort by filename</source>
        <translation>Sort by file name</translation>
    </message>
    <message>
        <source>Sort by time</source>
        <translation>Sort by time</translation>
    </message>
    <message>
        <source>Sort by size</source>
        <translation>Sort by size</translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation>Sort by type</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation>Reverse order</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Copy marked files</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Move marked files</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Index directory</translation>
    </message>
    <message>
        <source>Set as root</source>
        <translation>Set as root directory</translation>
    </message>
    <message>
        <source>Reset root</source>
        <translation>Reset root directory</translation>
    </message>
    <message>
        <source>Go up</source>
        <translation>Go up</translation>
    </message>
    <message>
        <source>Show hidden files</source>
        <translation>Show hidden files</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <source>Logarithmic scale</source>
        <translation>Logarithmic scale</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <source>Property</source>
        <translation>Property</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Value</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Comment</translation>
    </message>
    <message>
        <source>FITS Header</source>
        <translation>FITS Header</translation>
    </message>
    <message>
        <source>Image info</source>
        <translation>Image info</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation>OpenGL error</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Image info</source>
        <translation>Image info</translation>
    </message>
    <message>
        <source>Can&apos;t open DB</source>
        <translation>Can&apos;t open DB</translation>
    </message>
    <message>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Can&apos;t open SQLITE database</translation>
    </message>
    <message>
        <source>Filesystem</source>
        <translation>File system</translation>
    </message>
    <message>
        <source>Tenmon</source>
        <translation>Tenmon</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <source>Copy marked files</source>
        <translation>Copy marked files</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Save as</translation>
    </message>
    <message>
        <source>Live mode</source>
        <translation>Live mode</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <source>View</source>
        <translation>View</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Zoom In</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Zoom Out</translation>
    </message>
    <message>
        <source>Best Fit</source>
        <translation>Best Fit</translation>
    </message>
    <message>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Select</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>Mark</translation>
    </message>
    <message>
        <source>Unmark</source>
        <translation>Unmark</translation>
    </message>
    <message>
        <source>Mark and next</source>
        <translation>Mark and next</translation>
    </message>
    <message>
        <source>Unmark and next</source>
        <translation>Unmark and next</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation>Analyze</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation>Image statistics</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation>Peak finder</translation>
    </message>
    <message>
        <source>Docks</source>
        <translation>Docks</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Open file</translation>
    </message>
    <message>
        <source>Select destination</source>
        <translation>Select destination</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation>Copying</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Move marked files</source>
        <translation>Move marked files</translation>
    </message>
    <message>
        <source>Index directory</source>
        <translation>Index directory</translation>
    </message>
    <message>
        <source>Thumbnails</source>
        <translation>Thumbnails</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation>Show marked</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>About Tenmon</source>
        <translation>About Tenmon</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>About Qt</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation>Moving</translation>
    </message>
    <message>
        <source>Indexing FITS files</source>
        <translation>Indexing FITS files</translation>
    </message>
    <message>
        <source>Reindex files</source>
        <translation>Reindex files</translation>
    </message>
    <message>
        <source>FITS/XISF files database</source>
        <translation>FITS/XISF files database</translation>
    </message>
    <message>
        <source>File tree</source>
        <translation>File tree</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation>Star finder</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Images (</source>
        <translation>Images (</translation>
    </message>
    <message>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>FITS image (*.fits *.fit);;XISF image (*.xisf);;</translation>
    </message>
    <message>
        <source>Failed to copy</source>
        <translation>Failed to copy</translation>
    </message>
    <message>
        <source>Failed to move</source>
        <translation>Failed to move</translation>
    </message>
    <message>
        <source>Failed to move from %1 to %2</source>
        <translation>Failed to move from %1 to %2</translation>
    </message>
    <message>
        <source>Failed to copy from %1 to %2</source>
        <translation>Failed to copy from %1 to %2</translation>
    </message>
    <message>
        <source>;;All files (*)</source>
        <translation>;;All files (*)</translation>
    </message>
    <message>
        <source>Move files to trash?</source>
        <translation>Move files to trash?</translation>
    </message>
    <message>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Do you want to move %1 files to trash?</translation>
    </message>
    <message>
        <source>Failed to move file to trash</source>
        <translation>Failed to move file to trash</translation>
    </message>
    <message>
        <source>Failed to move file to trash %1</source>
        <translation>Failed to move file to trash %1</translation>
    </message>
    <message>
        <source>Move marked files to trash</source>
        <translation>Move marked files to trash</translation>
    </message>
    <message>
        <source>Moving marked files to trash</source>
        <translation>Moving marked files to trash</translation>
    </message>
    <message>
        <source>Export database to CSV</source>
        <translation>Export database to CSV file</translation>
    </message>
    <message>
        <source>CSV file (*.csv)</source>
        <translation>CSV files (*.csv)</translation>
    </message>
    <message>
        <source>Histogram</source>
        <translation>Histogram</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <source>Marked files</source>
        <translation>Marked files</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>File name</translation>
    </message>
    <message>
        <source>Clear selected</source>
        <translation>Clear selected</translation>
    </message>
    <message>
        <source>Clear all</source>
        <translation>Clear all</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <source>Shutter speed</source>
        <translation>Shutter speed</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Height</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>File name</translation>
    </message>
    <message>
        <source>Mean</source>
        <translation>Mean</translation>
    </message>
    <message>
        <source>Standart deviation</source>
        <translation>Standart deviation</translation>
    </message>
    <message>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <source>Unsupported sample format</source>
        <translation>Unsupported sample format</translation>
    </message>
    <message>
        <source>Saturated</source>
        <translation>Saturated</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <source>Press Shift for fine tuning</source>
        <translation>Press Shift for fine tuning</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <source>Select columns</source>
        <translation>Select columns</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>How many images are preloaded before and after current image.</source>
        <translation>How many images are preloaded before and after current image.</translation>
    </message>
    <message>
        <source>Thumbnail size in pixels</source>
        <translation>Thumbnail size in pixels</translation>
    </message>
    <message>
        <source>Image preload count</source>
        <translation>Image preload count</translation>
    </message>
    <message>
        <source>Thumbnails size</source>
        <translation>Thumbnails size</translation>
    </message>
    <message>
        <source>Don&apos;t use native file dialog</source>
        <translation>Don&apos;t use native file dialog</translation>
    </message>
    <message>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Saturated</translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <source>Stretch toolbar</source>
        <translation>Stretch toolbar</translation>
    </message>
    <message>
        <source>Auto Stretch F12</source>
        <translation>Auto Stretch F12</translation>
    </message>
    <message>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Reset Screen Transfer Function F11</translation>
    </message>
    <message>
        <source>Invert colors</source>
        <translation>Invert colors</translation>
    </message>
    <message>
        <source>Apply auto stretch on load</source>
        <translation>Apply auto stretch on load</translation>
    </message>
    <message>
        <source>Debayer CFA</source>
        <translation>Debayer CFA</translation>
    </message>
    <message>
        <source>False colors</source>
        <translation>False colors</translation>
    </message>
    <message>
        <source>Linked channels</source>
        <translation>Linked channels</translation>
    </message>
</context>
</TS>
