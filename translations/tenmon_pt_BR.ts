<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR" sourcelanguage="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="12"/>
        <source>About Tenmon</source>
        <translatorcomment>Sobre Tenmon</translatorcomment>
        <translation>About Tenmon</translation>
    </message>
</context>
<context>
    <name>BatchProcessing</name>
    <message>
        <location filename="../batchprocessing.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="208"/>
        <source>Batch Processing</source>
        <translation>Processamento em lote</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="22"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="209"/>
        <source>Input files and directories</source>
        <translation>Arquivos e diretórios de entrada</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="44"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="210"/>
        <source>Add files</source>
        <translation>Adicionar arquivos</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="51"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="211"/>
        <source>Add directories</source>
        <translation>Adicionar diretório</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="58"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="212"/>
        <source>Remove</source>
        <translation>Retirar</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="213"/>
        <source>Remove all</source>
        <translation>Remover tudo</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="78"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="214"/>
        <source>Output directory</source>
        <translation>Diretório de saída</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="92"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="215"/>
        <source>Browse</source>
        <translation>Navegar</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="103"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="216"/>
        <source>Scripts</source>
        <translation>Scripts</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="217"/>
        <source>Open scripts</source>
        <translation>Abrir scripts</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="142"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="218"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="182"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="219"/>
        <source>Start script</source>
        <translation>Iniciar script</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="192"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="220"/>
        <source>Stop script</source>
        <translation>Script de parada</translation>
    </message>
    <message>
        <location filename="../batchprocessing.ui" line="199"/>
        <location filename="../build/tenmon_autogen/include/ui_batchprocessing.h" line="221"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="123"/>
        <source>Interrupt running script?</source>
        <translation>Interromper o script em execução?</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="143"/>
        <source>Select files</source>
        <translation>Selecionar arquivos</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="154"/>
        <source>Select directory</source>
        <translation>Selecione o diretório</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="175"/>
        <source>Select output directory</source>
        <translation>Selecione o diretório de saída</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Invalid output directory</source>
        <translation>Diretório de saída inválido</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="226"/>
        <source>Output directory path doesn&apos;t exist or is not writable</source>
        <translation>O caminho do diretório de saída não existe ou não é gravável</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="257"/>
        <source>Enter text</source>
        <translation>Inserir texto</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="264"/>
        <source>Enter integer number</source>
        <translation>Insira o número inteiro</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="271"/>
        <source>Enter float number</source>
        <translation>Digite o número real</translation>
    </message>
    <message>
        <location filename="../batchprocessing.cpp" line="278"/>
        <source>Select item</source>
        <translation>Selecione o item</translation>
    </message>
</context>
<context>
    <name>DataBaseView</name>
    <message>
        <location filename="../databaseview.cpp" line="255"/>
        <source>Select columns</source>
        <translation>Selecionar colunas</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="294"/>
        <source>Text to search, you can % as wildcard</source>
        <translation>Texto a ser pesquisado, você pode % como curinga</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="310"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
</context>
<context>
    <name>DatabaseTableView</name>
    <message>
        <location filename="../databaseview.cpp" line="215"/>
        <source>Mark</source>
        <translation>Assinalar</translation>
    </message>
    <message>
        <location filename="../databaseview.cpp" line="216"/>
        <source>Unmark</source>
        <translation>Desmarcar</translation>
    </message>
</context>
<context>
    <name>FITSFileModel</name>
    <message>
        <location filename="../databaseview.cpp" line="194"/>
        <source>File name</source>
        <translation>Nome do arquivo</translation>
    </message>
</context>
<context>
    <name>FilesystemWidget</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="26"/>
        <source>Sort by filename</source>
        <translation>Classificar por nome de arquivo</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="27"/>
        <source>Sort by time</source>
        <translation>Classificar por tempo</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="28"/>
        <source>Sort by size</source>
        <translation>Classificar por tamanho</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="29"/>
        <source>Sort by type</source>
        <translation>Classificar por tipo</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="30"/>
        <source>Reverse</source>
        <translation>Ordem inversa</translation>
    </message>
</context>
<context>
    <name>Filetree</name>
    <message>
        <location filename="../filesystemwidget.cpp" line="90"/>
        <location filename="../filesystemwidget.cpp" line="94"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="96"/>
        <source>Copy marked files</source>
        <translation>Copiar arquivos marcados</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="97"/>
        <source>Move marked files</source>
        <translation>Mover arquivos marcados</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="98"/>
        <source>Index directory</source>
        <translation>Diretório de índice</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="95"/>
        <source>Set as root</source>
        <translation>Definir como diretório raiz</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="102"/>
        <source>Reset root</source>
        <translation>Redefinir diretório raiz</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="103"/>
        <source>Go up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="../filesystemwidget.cpp" line="104"/>
        <source>Show hidden files</source>
        <translation>Mostrar arquivos ocultos</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../about.cpp" line="33"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <location filename="../histogram.cpp" line="73"/>
        <source>Logarithmic scale</source>
        <translation>Escala logarítmica</translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Property</source>
        <translation>Propriedade</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="8"/>
        <source>Comment</source>
        <translation>Comentário</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="25"/>
        <source>FITS Header</source>
        <translation>Cabeçalho FITS</translation>
    </message>
    <message>
        <location filename="../imageinfo.cpp" line="34"/>
        <source>Image info</source>
        <translation>Imagem de informação</translation>
    </message>
</context>
<context>
    <name>ImageRingList</name>
    <message>
        <location filename="../imageringlist.cpp" line="406"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
</context>
<context>
    <name>ImageWidget</name>
    <message>
        <source>OpenGL error</source>
        <translation type="vanished">OpenGL error</translation>
    </message>
    <message>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation type="vanished">Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</translation>
    </message>
    <message>
        <source>L:%1</source>
        <translation type="vanished">L:%1</translation>
    </message>
    <message>
        <source>X:%3 Y:%4</source>
        <translation type="vanished">X:%3 Y:%4</translation>
    </message>
    <message>
        <source>R:%1 G:%2 B:%3</source>
        <translation type="vanished">R:%1 G:%2 B:%3</translation>
    </message>
    <message>
        <source>Failed to load image</source>
        <translation type="vanished">Failed to load image</translation>
    </message>
</context>
<context>
    <name>ImageWidgetGL</name>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>OpenGL error</source>
        <translation>Erro de OpenGL</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="87"/>
        <location filename="../imagewidget.cpp" line="637"/>
        <source>Could not initialize OpenGL 3.3 context. Ensure that proper GPU driver is installed.</source>
        <translation>Não foi possível inicializar o contexto do OpenGL 3.3. Certifique-se de que o driver de GPU adequado esteja instalado.</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="109"/>
        <source>Failed to load image</source>
        <translation>Falha ao carregar a imagem</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <source>L:%1</source>
        <translation>L:%1</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="863"/>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>X:%3 Y:%4</source>
        <translation>X:%3 Y:%4</translation>
    </message>
    <message>
        <location filename="../imagewidget.cpp" line="865"/>
        <source>R:%1 G:%2 B:%3</source>
        <translation>R:%1 G:%2 B:%3</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="71"/>
        <source>Image info</source>
        <translation>Imagem de informação</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open DB</source>
        <translation>Não é possível abrir o banco de dados</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Can&apos;t open SQLITE database</source>
        <translation>Não é possível abrir o banco de dados SQLITE</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <source>Filesystem</source>
        <translation>Sistema de ficheiros</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>Tenmon</source>
        <translation>astronomia</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="157"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="162"/>
        <source>Copy marked files</source>
        <translation>Copiar arquivos marcados</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <location filename="../mainwindow.cpp" line="614"/>
        <location filename="../mainwindow.cpp" line="776"/>
        <source>Save as</source>
        <translation>Salvar como</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Open directory recursively</source>
        <translation>Abra o diretório recursivamente</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>Batch processing</source>
        <translation>Processamento em lote</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Live mode</source>
        <translation>Modo ao vivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="177"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="185"/>
        <source>View</source>
        <translation>Vista</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="186"/>
        <source>Zoom In</source>
        <translation>Ampliar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="187"/>
        <source>Zoom Out</source>
        <translation>Diminuir o zoom</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="188"/>
        <source>Best Fit</source>
        <translation>Melhor ajuste</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="212"/>
        <source>Colormap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="219"/>
        <source>User %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="245"/>
        <source>Select</source>
        <translation>Selecionar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="246"/>
        <source>Mark</source>
        <translation>Assinalar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="247"/>
        <source>Unmark</source>
        <translation>Desmarcar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="249"/>
        <source>Mark and next</source>
        <translation>Marcar e próximo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="250"/>
        <source>Unmark and next</source>
        <translation>Desmarcar e avançar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Show marked list</source>
        <translation>Mostrar lista marcada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="253"/>
        <source>Open marked</source>
        <translation>Aberto marcado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Overwrite file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Destination file %1 already exists. Overwrite?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>Missing marked files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="528"/>
        <source>%1 marked files were missing. They were skipped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <location filename="../mainwindow.cpp" line="801"/>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Update check</source>
        <translation>Verificação de atualização</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="798"/>
        <source>You have newest version</source>
        <translation>Você tem a versão mais recente</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="801"/>
        <source>New version %1 is available. Do you want to download it now?</source>
        <translation>A nova versão %1 está disponível. Deseja baixá-lo agora?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="812"/>
        <source>Failed to check version</source>
        <translation>Falha ao verificar a versão</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analyze</translation>
    </message>
    <message>
        <source>Image statistics</source>
        <translation type="vanished">Image statistics</translation>
    </message>
    <message>
        <source>Peak finder</source>
        <translation type="vanished">Peak finder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Docks</source>
        <translation>Docas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open file</source>
        <translation>Abrir arquivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="427"/>
        <source>Select destination</source>
        <translation>Selecione o destino</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Copying</source>
        <translation>Copiando</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="163"/>
        <source>Move marked files</source>
        <translation>Mover arquivos marcados</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="589"/>
        <source>Index directory</source>
        <translation>Diretório de índice</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>Thumbnails</source>
        <translation>Miniaturas</translation>
    </message>
    <message>
        <source>Show marked</source>
        <translation type="vanished">Show marked</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="294"/>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="296"/>
        <source>About Tenmon</source>
        <translation>Sobre Tenmon</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="297"/>
        <source>About Qt</source>
        <translation>Sobre o Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Check for update</source>
        <translation>Verificar atualizações</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="443"/>
        <source>Moving</source>
        <translation>Movente</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <location filename="../mainwindow.cpp" line="605"/>
        <source>Indexing FITS files</source>
        <translation>Indexação de arquivos FITS</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>Reindex files</source>
        <translation>Reindexar arquivos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="124"/>
        <source>FITS/XISF files database</source>
        <translation>Banco de dados de arquivos FITS/XISF</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>File tree</source>
        <translation>Árvore de arquivos</translation>
    </message>
    <message>
        <source>Star finder</source>
        <translation type="vanished">Star finder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="182"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Images (</source>
        <translation>Imagens (</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="51"/>
        <source>FITS (*.fits *.fit);;XISF (*.xisf);;</source>
        <translation>Imagem FITS (*.fits *.fit);; Imagem XISF (*.xisf);;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to copy</source>
        <translation>Falha ao copiar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>Failed to move</source>
        <translation>Falha ao mover</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to move from %1 to %2</source>
        <translation>Falha ao mover de %1 para %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="518"/>
        <source>Failed to copy from %1 to %2</source>
        <translation>Falha ao copiar de %1 para %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>;;All files (*)</source>
        <translation>;; Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Move files to trash?</source>
        <translation>Mover arquivos para a lixeira?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="713"/>
        <source>Do you want to move %1 files to trash?</source>
        <translation>Deseja mover arquivos %1 para a lixeira?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash</source>
        <translation>Falha ao mover o arquivo para a lixeira</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Failed to move file to trash %1</source>
        <translation>Falha ao mover o arquivo para a lixeira %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="164"/>
        <source>Move marked files to trash</source>
        <translation>Mover arquivos marcados para a lixeira</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>Moving marked files to trash</source>
        <translation>Movendo arquivos marcados para a lixeira</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="168"/>
        <source>Export database to CSV</source>
        <translation>Exportar banco de dados para arquivo CSV</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>CSV file (*.csv)</source>
        <translation>Arquivos CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <source>Histogram</source>
        <translation>Histograma</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>Bayer mask</source>
        <translation>máscara Bayer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="193"/>
        <source>RGGB</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="194"/>
        <source>GRBG</source>
        <translation>GRBG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="195"/>
        <source>GBRG</source>
        <translation>GBRG</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="196"/>
        <source>BGGR</source>
        <translation>BGGR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Slideshow</source>
        <translation>Apresentação de slides</translation>
    </message>
</context>
<context>
    <name>MarkedFiles</name>
    <message>
        <location filename="../markedfiles.cpp" line="11"/>
        <source>Marked files</source>
        <translation>Arquivos marcados</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="22"/>
        <source>Filename</source>
        <translation>Nome do arquivo</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="30"/>
        <source>Clear selected</source>
        <translation>Limpar selecionado</translation>
    </message>
    <message>
        <location filename="../markedfiles.cpp" line="31"/>
        <source>Clear all</source>
        <translation>Apagar tudo</translation>
    </message>
</context>
<context>
    <name>PlateSolving</name>
    <message>
        <location filename="../platesolving.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="345"/>
        <source>Plate Solving</source>
        <translation>Resolução de placas</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="23"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="346"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="35"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="347"/>
        <source>Start point</source>
        <translation>Ponto de partida</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="42"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="348"/>
        <source>Degree width</source>
        <translation>Largura do grau</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="47"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="349"/>
        <source>Arcmin width</source>
        <translation>Largura do arco</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="52"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="350"/>
        <source>Arcsec per pixel</source>
        <translation>Arcsec por pixel</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="57"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="351"/>
        <source>35 mm equivalent focal length</source>
        <translation>Distância focal equivalente a 35 mm</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="65"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="353"/>
        <source>Use position</source>
        <translation>Posição de uso</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="88"/>
        <location filename="../platesolving.ui" line="208"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="354"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="364"/>
        <source>DEC</source>
        <translation>DEC</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="355"/>
        <source> h</source>
        <translation> h</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="111"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="356"/>
        <source>High</source>
        <translation>Alto</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="357"/>
        <source>Use scale</source>
        <translation>Use escala</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="358"/>
        <source> deg</source>
        <translation> Você</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="164"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="359"/>
        <source>Low </source>
        <translation>Baixo </translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="171"/>
        <location filename="../platesolving.ui" line="194"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="360"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="363"/>
        <source>RA</source>
        <translation>RA</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="178"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="361"/>
        <source>Unit</source>
        <translation>Unidade</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="188"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="362"/>
        <source>Solution</source>
        <translation>Solução</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="222"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="365"/>
        <source>Field width</source>
        <translation>Largura do campo</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="236"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="366"/>
        <source>Field height</source>
        <translation>Altura do campo</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="250"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="367"/>
        <source>Orientation</source>
        <translation>Orientação</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="264"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="368"/>
        <source>Pixel scale</source>
        <translation>Escala de pixels</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="278"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="369"/>
        <source>Stars</source>
        <translation>Estrelas</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="292"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="370"/>
        <source>HFR</source>
        <translation>HFR</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="318"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="371"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="325"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="372"/>
        <source>Extract</source>
        <translation>Extrair</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="332"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="373"/>
        <source>Solve</source>
        <translation>Resolver</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="339"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="374"/>
        <source>Extract with HFR</source>
        <translation>Extrato com HFR</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="346"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="375"/>
        <source>Abort</source>
        <translation>Aborto</translation>
    </message>
    <message>
        <location filename="../platesolving.ui" line="353"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolving.h" line="376"/>
        <source>Update FITS header</source>
        <translation>Atualizar cabeçalho FITS</translation>
    </message>
    <message>
        <location filename="../platesolving.cpp" line="152"/>
        <source>Header update failed</source>
        <translation>Falha na atualização do cabeçalho</translation>
    </message>
</context>
<context>
    <name>PlateSolvingSettings</name>
    <message>
        <location filename="../platesolvingsettings.ui" line="14"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="232"/>
        <source>Dialog</source>
        <translation>Plate Solving Settings</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="32"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="233"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="39"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="234"/>
        <source>Remove</source>
        <translation>Retirar</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="48"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving need index files in order to solve an image. You can download them here by clicking on check box. It will download them into default location. Or you can reuse &lt;span style=&quot; font-weight:700;&quot;&gt;any&lt;/span&gt; index files (not only listed bellow) from astrometry.net by adding path pointing to them. &lt;a href=&quot;https://astrometrynet.readthedocs.io/en/latest/readme.html#getting-index-files&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;More details about index files.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;It is required to download index files that cover 100%-50% field of view and recomended 100%-10%. So for images with 70&apos; field of view it is required to download index files in 30&apos;-85&apos; and recomended 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plate solving need index files in order to solve an image. You can download them here by clicking on check box. It will download them into default location. Or you can reuse &lt;span style=&quot; font-weight:700;&quot;&gt;any&lt;/span&gt; index files (not only listed bellow) from astrometry.net by adding path pointing to them. &lt;a href=&quot;https://astrometrynet.readthedocs.io/en/latest/readme.html#getting-index-files&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;More details about index files.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;It is required to download index files that cover 100%-50% field of view and recomended 100%-10%. So for images with 70&apos; field of view it is required to download index files in 30&apos;-85&apos; and recomended 4&apos;-85&apos;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="61"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="236"/>
        <source>Index files</source>
        <translation>Index files</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="67"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="237"/>
        <source>240&apos; - 340&apos;index-4114.fits (1.4 MiB)</source>
        <translation>240&apos; - 340&apos;index-4114.fits (1.4 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="74"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="238"/>
        <source>680&apos; - 1000&apos; index-4117.fits (242 kiB)</source>
        <translation>680&apos; - 1000&apos; index-4117.fits (242 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="81"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="239"/>
        <source>480&apos; - 680&apos; index-4116.fits (400 kiB)</source>
        <translation>480&apos; - 680&apos; index-4116.fits (400 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="88"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="240"/>
        <source>1000&apos; - 1400&apos; index-4118.fits (183 kiB)</source>
        <translation>1000&apos; - 1400&apos; index-4118.fits (183 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="95"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="241"/>
        <source>340&apos; - 480&apos; index-4115.fits (723 kiB)</source>
        <translation>340&apos; - 480&apos; index-4115.fits (723 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="102"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="242"/>
        <source>1400&apos; - 2000&apos; index-4119.fits (141 kiB)</source>
        <translation>1400&apos; - 2000&apos; index-4119.fits (141 kiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="109"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="243"/>
        <source>120&apos; - 170&apos; index-4112.fits (5.1MiB)</source>
        <translation>120&apos; - 170&apos; index-4112.fits (5.1MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="116"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="244"/>
        <source>170&apos; - 240&apos; index-4113.fits (2.7MiB)</source>
        <translation>170&apos; - 240&apos; index-4113.fits (2.7MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="123"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="245"/>
        <source>85&apos; - 120&apos; index-4111.fits (9.8 MiB)</source>
        <translation>85&apos; - 120&apos; index-4111.fits (9.8 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="130"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="246"/>
        <source>60&apos; - 85&apos; index-4110.fits (24 MiB)</source>
        <translation>60&apos; - 85&apos; index-4110.fits (24 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="137"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="247"/>
        <source>42&apos; - 60&apos; index-4109.fits (48 MiB)</source>
        <translation>42&apos; - 60&apos; index-4109.fits (48 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="144"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="248"/>
        <source>30&apos; - 42&apos; index-4108.fits (91 MiB)</source>
        <translation>30&apos; - 42&apos; index-4108.fits (91 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="151"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="249"/>
        <source>22&apos; - 30&apos; index-4107.fits (158 MiB)</source>
        <translation>22&apos; - 30&apos; index-4107.fits (158 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="158"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="250"/>
        <source>16&apos; - 22&apos; index-5206-*.fits (294 MiB)</source>
        <translation>16&apos; - 22&apos; index-5206-*.fits (294 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="165"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="251"/>
        <source>11&apos; - 16&apos; index-5205-*.fits (587 MiB)</source>
        <translation>11&apos; - 16&apos; index-5205-*.fits (587 MiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="172"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="252"/>
        <source>8&apos; - 11&apos; index-5204-*.fits (1.2 GiB)</source>
        <translation>8&apos; - 11&apos; index-5204-*.fits (1.2 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="179"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="253"/>
        <source>4.0&apos; - 5.6&apos; index-5203-*.fits (2.3 GiB)</source>
        <translation>4.0&apos; - 5.6&apos; index-5203-*.fits (2.3 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="186"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="254"/>
        <source>5.6&apos; - 8.0&apos; index-5202-*.fits (4.6 GiB)</source>
        <translation>5.6&apos; - 8.0&apos; index-5202-*.fits (4.6 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="193"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="255"/>
        <source>2.0&apos; - 2.8&apos; index-5201-*.fits (8.9 GiB)</source>
        <translation>2.0&apos; - 2.8&apos; index-5201-*.fits (8.9 GiB)</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="205"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="256"/>
        <source>0 files</source>
        <translation>0 files</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.ui" line="219"/>
        <location filename="../build/tenmon_autogen/include/ui_platesolvingsettings.h" line="257"/>
        <source>Stop download</source>
        <translation>Parar download</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="40"/>
        <source>Index files directory</source>
        <translation>Diretório de arquivos de índice</translation>
    </message>
    <message>
        <location filename="../platesolvingsettings.cpp" line="133"/>
        <source>%1 files</source>
        <translation>%1 files</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../loadimage.cpp" line="371"/>
        <source>ISO</source>
        <translation>ISO</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="372"/>
        <source>Shutter speed</source>
        <translation>Shutter speed</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="156"/>
        <location filename="../loadimage.cpp" line="224"/>
        <location filename="../loadimage.cpp" line="369"/>
        <location filename="../loadimage.cpp" line="404"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="157"/>
        <location filename="../loadimage.cpp" line="225"/>
        <location filename="../loadimage.cpp" line="370"/>
        <location filename="../loadimage.cpp" line="405"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="96"/>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="32"/>
        <source>Filename</source>
        <translation>File name</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="36"/>
        <source>Failed to load image</source>
        <translation>Failed to load image</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="52"/>
        <location filename="../loadrunable.cpp" line="62"/>
        <source>Mean</source>
        <translation>Mean</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="53"/>
        <location filename="../loadrunable.cpp" line="63"/>
        <source>Standart deviation</source>
        <translation>Standart deviation</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="54"/>
        <location filename="../loadrunable.cpp" line="64"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="55"/>
        <location filename="../loadrunable.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="56"/>
        <location filename="../loadrunable.cpp" line="66"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="57"/>
        <location filename="../loadrunable.cpp" line="67"/>
        <source>MAD</source>
        <translation>MAD</translation>
    </message>
    <message>
        <location filename="../loadimage.cpp" line="147"/>
        <location filename="../loadimage.cpp" line="271"/>
        <source>Unsupported sample format</source>
        <translation>Unsupported sample format</translation>
    </message>
    <message>
        <location filename="../loadrunable.cpp" line="58"/>
        <location filename="../loadrunable.cpp" line="68"/>
        <source>Saturated</source>
        <translation>Saturated</translation>
    </message>
</context>
<context>
    <name>STFSlider</name>
    <message>
        <location filename="../stfslider.cpp" line="41"/>
        <source>Press Shift for fine tuning</source>
        <translation>Press Shift for fine tuning</translation>
    </message>
</context>
<context>
    <name>SelectColumnsDialog</name>
    <message>
        <location filename="../databaseview.cpp" line="52"/>
        <source>Select columns</source>
        <translation>Select columns</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../settingsdialog.cpp" line="39"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="46"/>
        <source>How many images are preloaded before and after current image.</source>
        <translation>How many images are preloaded before and after current image.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="52"/>
        <source>Thumbnail size in pixels</source>
        <translation>Thumbnail size in pixels</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="83"/>
        <source>Image preload count</source>
        <translation>Image preload count</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="84"/>
        <source>Thumbnails size</source>
        <translation>Thumbnails size</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="68"/>
        <source>Don&apos;t use native file dialog</source>
        <translation>Don&apos;t use native file dialog</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="59"/>
        <source>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</source>
        <translation>Set threshold value that is considered saturated when showing statistics.
For RAW files you may set 22%</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="85"/>
        <source>Saturation</source>
        <translation>Saturated</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Nearest</source>
        <translation>Nearest</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="72"/>
        <source>Cubic</source>
        <translation>Cubic</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="75"/>
        <source>Smooth thumbnails</source>
        <translation>Smooth thumbnails</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="77"/>
        <source>Use box filter when downsampling thumbnails instead of nearest. Slightly slower.</source>
        <translation>Use box filter when downsampling thumbnails instead of nearest. Slightly slower.</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="79"/>
        <source>Best Fit on image load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="80"/>
        <source>Set Best Fit zoom level when opening new image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="86"/>
        <source>Slideshow interval</source>
        <translation>Slideshow interval</translation>
    </message>
    <message>
        <source>Image filtering</source>
        <translation type="obsolete">Image sampling</translation>
    </message>
    <message>
        <location filename="../settingsdialog.cpp" line="87"/>
        <source>Image interpolation</source>
        <translation>Image interpolation</translation>
    </message>
</context>
<context>
    <name>Solver</name>
    <message>
        <location filename="../solver.cpp" line="77"/>
        <source>Unsupported image data type</source>
        <translation>Unsupported image data type</translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="157"/>
        <source>Solving is not finished</source>
        <translation>Solving is not finished</translation>
    </message>
    <message>
        <location filename="../solver.cpp" line="190"/>
        <source>Failed to update file header</source>
        <translation>Failed to update file header</translation>
    </message>
</context>
<context>
    <name>StretchToolbar</name>
    <message>
        <location filename="../stretchtoolbar.cpp" line="20"/>
        <source>Stretch toolbar</source>
        <translation>Stretch toolbar</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="74"/>
        <source>Auto Stretch F12</source>
        <translation>Auto Stretch F12</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="78"/>
        <source>Reset Screen Transfer Function F11</source>
        <translation>Reset Screen Transfer Function F11</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="82"/>
        <source>Invert colors</source>
        <translation>Invert colors</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="94"/>
        <source>Apply auto stretch on load</source>
        <translation>Apply auto stretch on load</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="90"/>
        <source>Debayer CFA</source>
        <translation>Debayer CFA</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="86"/>
        <source>False colors</source>
        <translation>False colors</translation>
    </message>
    <message>
        <location filename="../stretchtoolbar.cpp" line="69"/>
        <source>Linked channels</source>
        <translation>Linked channels</translation>
    </message>
</context>
</TS>
