#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSocketNotifier>
#include "imageringlist.h"
#include "database.h"
#include "imageinfo.h"
#include "imagescrollarea.h"
#include "filesystemwidget.h"
#include "stretchtoolbar.h"
#include "databaseview.h"
#include "platesolving.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    ImageScrollArea *m_image;
    ImageRingList *m_ringList;
    StretchToolbar *m_stretchPanel;
    Database *m_database;
    ImageInfo *m_info;
    FilesystemWidget *m_filesystem;
    Filetree *m_filetree;
    DataBaseView *m_databaseView;
    PlateSolving *_plateSolving;
    static int socketPair[2];
    QSocketNotifier *socketNotifier;
    QString _lastDir;
    bool _maximized;
    QString _openFilter;
    QString _saveFilter;
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow() override;
protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void setupSigterm();
    static void signalHandler(int);
    void closeEvent(QCloseEvent *event) override;
    void copyOrMove(bool copy);
    void copyOrMove(bool copy, const QString &dest);
protected slots:
    void socketNotify();
    void updateWindowTitle();
    void loadFile();
    void loadFile(const QString &path);
    void loadFile(int row);
    void loadDir();
    void indexDir();
    void indexDir(const QString &dir);
    void reindex();
    void saveAs();
    void convert(const QString &outfile, const QString &format);
    void markImage();
    void unmarkImage();
    void markAndNext();
    void unmarkAndNext();
    void copyMarked();
    void moveMarked();
    void deleteMarked();
    void liveMode(bool active);
    void imageStats(bool imageStats);
    void peakFinder(bool findPeaks);
    void starFinder(bool findStars);
    void showMarkFilesDialog();
    void showSettingsDialog();
    void exportCSV();
    void checkNewVersion();
};

#endif // MAINWINDOW_H
