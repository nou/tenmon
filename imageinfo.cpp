#include "imageinfo.h"
#include <QSettings>
#include <QHeaderView>

ImageInfo::ImageInfo(QWidget *parent) : QTreeWidget(parent)
{
    setColumnCount(3);
    setHeaderLabels({tr("Property"), tr("Value"), tr("Comment")});
    setIndentation(5);
    QSettings settings;
    header()->restoreState(settings.value("imageinfo/headerstate").toByteArray());
}

ImageInfo::~ImageInfo()
{
    QSettings settings;
    settings.setValue("imageinfo/headerstate", header()->saveState());
}

void ImageInfo::setInfo(const ImageInfoData &info)
{
    clear();
    if(info.fitsHeader.size())
    {
        QTreeWidgetItem *fitsHeader = new QTreeWidgetItem({tr("FITS Header")});
        for(const FITSRecord &record : info.fitsHeader)
        {
            new QTreeWidgetItem(fitsHeader, {record.key, record.value.toString().left(1024), record.comment});
        }
        addTopLevelItem(fitsHeader);
    }
    if(info.info.size())
    {
        QTreeWidgetItem *infoHeader = new QTreeWidgetItem({tr("Image info")});
        for(auto &item : info.info)
        {
            new QTreeWidgetItem(infoHeader, {item.first, item.second});
        }
        addTopLevelItem(infoHeader);
    }
    expandAll();
}
