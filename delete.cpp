#ifdef __linux__

#define QT_NO_KEYWORDS
#include <QString>
#include <iostream>
#include <gio/gio.h>

//flatpak bug prevent to use QFile::moveToTrash
bool moveToTrash(const QString &path)
{
    GFile *gfile = g_file_new_for_path(path.toLocal8Bit().data());
    GError *error = nullptr;
    g_file_trash(gfile, nullptr, &error);
    if(error)std::cerr << "failed to trash file " << error->code << " " << error->message << std::endl;
    g_clear_error(&error);
    g_object_unref(gfile);
    return true;
}

#else

#include <QFile>
#include <QString>

bool moveToTrash(const QString &path)
{
    return QFile::moveToTrash(path);
}

#endif
