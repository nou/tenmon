#ifdef FLATPAK

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusUnixFileDescriptor>
#include <QString>
#include <fcntl.h>
#include <unistd.h>

//flatpak bug prevent to use QFile::moveToTrash
bool moveToTrash(const QString &path)
{
    QDBusConnection con = QDBusConnection::sessionBus();
    QDBusMessage message = QDBusMessage::createMethodCall("org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop", "org.freedesktop.portal.Trash", "TrashFile");
    int fd = ::open(path.toLocal8Bit().data(), O_RDWR);
    if(fd >= 0)
    {
        QList<QVariant> args = {QVariant::fromValue(QDBusUnixFileDescriptor(fd))};
        message.setArguments(args);
        QDBusMessage reply = con.call(message);
        ::close(fd);
        if(reply.type() == QDBusMessage::ReplyMessage && reply.arguments().size() && reply.arguments().first().toInt())
            return true;
        else
            return false;
    }
    return false;
}

#else

#include <QFile>
#include <QString>

bool moveToTrash(const QString &path)
{
    return QFile::moveToTrash(path);
}

#endif
