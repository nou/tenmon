#include "imagescrollarea.h"
#include "imageringlist.h"
#include <QDebug>
#include <QKeyEvent>
#include <QGridLayout>
#include <QMimeData>
#include <QMessageBox>
#include <QCoreApplication>
#include <QPainter>
#include <QFileInfo>
#include <QScrollBar>
#include <cmath>

ImageScrollArea::ImageScrollArea(Database *database, QWidget *parent) : QWidget(parent)
{
    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    ImageWidgetGL *imageWidgetGL = new ImageWidgetGL(database, this);
    m_imageWidget = imageWidgetGL;

    m_verticalScrollBar = new QScrollBar(Qt::Vertical, this);
    m_horizontalScrollBar = new QScrollBar(Qt::Horizontal, this);

    layout->setSpacing(0);
    layout->addWidget(dynamic_cast<ImageWidgetGL*>(m_imageWidget), 0, 0);
    layout->addWidget(m_verticalScrollBar, 0, 1);
    layout->addWidget(m_horizontalScrollBar, 1, 0);

    connect(m_verticalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollEvent()));
    connect(m_horizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollEvent()));

    if(imageWidgetGL)
    {
        connect(imageWidgetGL, &ImageWidgetGL::fileDropped, this, &ImageScrollArea::fileDropped);
        connect(imageWidgetGL, &ImageWidgetGL::status, this, &ImageScrollArea::status);
        connect(imageWidgetGL, &ImageWidgetGL::scrollBarsUpdate, this, &ImageScrollArea::updateScrollbars);
    }
}

ImageScrollArea::~ImageScrollArea()
{

}

void ImageScrollArea::allocateThumbnails(const QStringList &paths)
{
    m_imageWidget->allocateThumbnails(paths);
}

void ImageScrollArea::showThumbnail(bool enable)
{
    m_imageWidget->showThumbnail(enable);
}

void ImageScrollArea::setBayerMask(int mask)
{
    m_imageWidget->setBayerMask(mask);
}

void ImageScrollArea::setColormap(int colormap)
{
    m_imageWidget->setColormap(colormap);
}

void ImageScrollArea::updateScrollbars(int valueH, int stepH, int maxH, int valueV, int stepV, int maxV)
{
    if(maxH > 0)
    {
        m_horizontalScrollBar->show();
        m_horizontalScrollBar->setRange(0, maxH);
        m_horizontalScrollBar->setPageStep(stepH);
        m_horizontalScrollBar->setValue(valueH);
    }
    else
        m_horizontalScrollBar->hide();

    if(maxV > 0)
    {
        m_verticalScrollBar->show();
        m_verticalScrollBar->setRange(0, maxV);
        m_verticalScrollBar->setPageStep(stepV);
        m_verticalScrollBar->setValue(valueV);
    }
    else
        m_verticalScrollBar->hide();
}

void ImageScrollArea::zoomIn()
{
    m_imageWidget->zoom(1);
}

void ImageScrollArea::zoomOut()
{
    m_imageWidget->zoom(-1);
}

void ImageScrollArea::bestFit()
{
    m_horizontalScrollBar->hide();
    m_verticalScrollBar->hide();
    m_imageWidget->bestFit();
}

void ImageScrollArea::oneToOne()
{
    m_imageWidget->zoom(0);
}

void ImageScrollArea::imageLoaded(Image *image)
{
    if(image)
    {
        m_imageWidget->setImage(image->rawImage(), image->number());
        m_imageWidget->setWCS(image->info().wcs);
    }
}

void ImageScrollArea::thumbnailLoaded(const Image *image)
{
    m_imageWidget->thumbnailLoaded(image);
}

void ImageScrollArea::setMTFParams(const MTFParam &params)
{
    m_imageWidget->setMTFParams(params);
}

void ImageScrollArea::invert(bool enable)
{
    m_imageWidget->invert(enable);
}

void ImageScrollArea::superPixel(bool enable)
{
    m_imageWidget->superPixel(enable);
}

void ImageScrollArea::falseColor(bool enable)
{
    m_imageWidget->falseColor(enable);
}

QImage ImageScrollArea::renderToImage()
{
    return m_imageWidget->renderToImage();
}

void ImageScrollArea::scrollEvent()
{
    m_imageWidget->setOffset(m_horizontalScrollBar->value(), m_verticalScrollBar->value());
}
