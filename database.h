#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDir>
#include <QProgressDialog>

class Database : public QObject
{
    Q_OBJECT
    QSqlQuery m_markQuery;
    QSqlQuery m_unmarkQuery;
    QSqlQuery m_isMarkedQuery;

    QSqlQuery m_insertFile;
    QSqlQuery m_insertFileWcs;
    QSqlQuery m_insertFitsHeader;
    QSqlQuery m_checkFile;
    QSqlQuery m_headerKeywords;
    QSqlQuery m_deleteFile;

    int m_progress;
public:
    explicit Database(QObject *parent = 0);
    bool init(const QLatin1String &connectionName = QLatin1String(QSqlDatabase::defaultConnection));
    bool mark(const QString &filename);
    bool unmark(const QString &filename);
    bool mark(const QStringList &filenames);
    bool unmark(const QStringList &filenames);
    bool isMarked(const QString &filename);
    QStringList getMarkedFiles();
    void clearMarkedFiles();

    void indexDir(const QDir &dir, QProgressDialog *progress);
    void reindex(QProgressDialog *progress);
    QStringList getFitsKeywords();
protected:
    bool indexDir2(const QDir &dir, QProgressDialog *progress, QStringList &scannedDirs);
    bool indexFile(const QFileInfo &file);
    bool checkError(QSqlQuery &query);
    int checkVersion();
signals:
    void databaseChanged();
};

#endif // DATABASE_H
