#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QWidget>
#include "imageringlist.h"

class Histogram : public QWidget
{
    Q_OBJECT
    std::vector<uint32_t> m_histogram;
    std::vector<float> m_points;
    bool m_log = false;
public:
    explicit Histogram(QWidget *parent = nullptr);
public slots:
    void imageLoaded(Image *img);
protected:
    void paintEvent(QPaintEvent *) override;
    void mouseReleaseEvent(QMouseEvent *) override;
};

#endif // HISTOGRAM_H
