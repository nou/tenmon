#ifndef BATCHPROCESSING_H
#define BATCHPROCESSING_H

#include <QDialog>
#include <QFileSystemWatcher>
#include "scriptengine.h"

namespace Ui { class BatchProcessing; }

class BatchProcessing : public QDialog
{
    Q_OBJECT
    Ui::BatchProcessing *_ui;
    QString _scriptBasePath;
    QFileSystemWatcher _fileWatcher;
    Script::ScriptEngineThread *_engineThread = nullptr;
    QColor _textColor;
private slots:
    void scanScriptDir();
public:
    explicit BatchProcessing(QWidget *parent = nullptr);
    ~BatchProcessing();
protected:
    void closeEvent(QCloseEvent *event);
public slots:
    void addFiles();
    void addDir();
    void removePath();
    void removeAllPaths();
    void browse();
    void openScriptDir();
    void runScript();
    void stopScript();
    void scriptFinished();
    void newMessage(const QString &message, bool error);

    QJSValue getString(const QString &label, const QString &text);
    QJSValue getInt(const QString &label, int value);
    QJSValue getFloat(const QString &label, double value, int decimals);
    QJSValue getItem(const QStringList &items, const QString &label, int current);
};

#endif // BATCHPROCESSING_H
