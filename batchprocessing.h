#ifndef BATCHPROCESSING_H
#define BATCHPROCESSING_H

#include <QDialog>
#include <QFileSystemWatcher>
#include "scriptengine.h"

namespace Ui { class BatchProcessing; }

class BatchProcessing : public QDialog
{
    Ui::BatchProcessing *_ui;
    QString _scriptBasePath;
    QFileSystemWatcher _fileWatcher;
    Script::ScriptEngineThread *_engineThread = nullptr;
private slots:
    void scanScriptDir();
public:
    explicit BatchProcessing(QWidget *parent = nullptr);
    ~BatchProcessing();
protected:
    void closeEvent(QCloseEvent *event);
public slots:
    void scriptDirChanged();
    void addFiles();
    void addDir();
    void removePath();
    void removeAllPaths();
    void browse();
    void openScriptDir();
    void runScript();
    void stopScript();
    void scriptFinished();
    void newMessage(const QString &message, bool error);
};

#endif // BATCHPROCESSING_H
