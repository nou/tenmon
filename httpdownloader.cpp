#include "httpdownloader.h"
#include <QNetworkReply>
#include <QDebug>
#include <QRegularExpression>
#include <QFileInfo>

#ifdef PLATESOLVER
#include "solver.h"
#endif

// filename             arcseconds range
// index-4119.fits      1400–2000
// index-4118.fits      1000–1400
// index-4117.fits      680–1000
// index-4116.fits      480–680
// index-4115.fits      340–480
// index-4114.fits      240–340
// index-4113.fits      170–240
// index-4112.fits      120–170
// index-4111.fits      85–120
// index-4110.fits      60—85
// index-4109.fits      42–60
// index-4108.fits      30–42
// index-4107.fits      22–30
// index-5206-*.fits 	16–22
// index-5205-*.fits 	11–16
// index-5204-*.fits 	8–11
// index-5203-*.fits 	5.6–8.0
// index-5202-*.fits 	4.0–5.6
// index-5201-*.fits 	2.8–4.0

static const QMap<QString, QByteArray> md5 = {
    {"index-4107.fits.zst", "b4c3bc2b162fcb6417b2c3358dbf0543"},
    {"index-4108.fits.zst", "14a54b8e0abcb58efb7a828fc8f00267"},
    {"index-4109.fits.zst", "d6bce03dfbb527cc807ec360a8b4afa6"},
    {"index-4110.fits.zst", "da0aded630ee4650850f5828b4289746"},
    {"index-4111.fits.zst", "c11547481f97727e546b3b7c776f6394"},
    {"index-4112.fits.zst", "fd3f5ad964d69c66555b2c5b6d65d426"},
    {"index-4113.fits.zst", "4546e33817a161b8011e5f1321d39445"},
    {"index-4114.fits.zst", "ebc815fa4d9a3fd259fe22b84796fbc4"},
    {"index-4115.fits.zst", "5395b7b225ffe5329867354bc653887f"},
    {"index-4116.fits.zst", "341cebc6b962cede0f27d08c3b3a4f23"},
    {"index-4117.fits.zst", "e362a868ae0751d1a1e7f6b9e48a2f79"},
    {"index-4118.fits.zst", "a7d38ec4b1d69c859e875c8d6ba1679b"},
    {"index-4119.fits.zst", "9e07b46f4c4ca9ba536383d201e70c35"},
    {"index-5201-00.fits.zst", "87255d073576674ec50959522cbbc9eb"},
    {"index-5201-01.fits.zst", "b5154f26c8b2a6e143bdc11a062213ab"},
    {"index-5201-02.fits.zst", "cf0b08e586fe2ce306adb370c9f113e8"},
    {"index-5201-03.fits.zst", "eda457e3b3b419156b0cbdbe6c262fb7"},
    {"index-5201-04.fits.zst", "e1344126047714aac771d37861da4698"},
    {"index-5201-05.fits.zst", "1b2bf2fe61e883db7e65628761a934e8"},
    {"index-5201-06.fits.zst", "e4338de4ae486cedd31ec24b2677fe1d"},
    {"index-5201-07.fits.zst", "14665b88b4ab179d1bedd46acdc0d9bd"},
    {"index-5201-08.fits.zst", "636f411a83dfcf0c02e13ad4c0fed948"},
    {"index-5201-09.fits.zst", "8afe4edf38794225c1c3b23d72671d96"},
    {"index-5201-10.fits.zst", "742db3b858e160f69f2d189961fdfcad"},
    {"index-5201-11.fits.zst", "0ffb50923c71d269acc9c3c661d5429a"},
    {"index-5201-12.fits.zst", "535eefd763e08593e775e0f4e19c69e3"},
    {"index-5201-13.fits.zst", "e94426ba2275e76b11495105d780890d"},
    {"index-5201-14.fits.zst", "754a22f37153773662acaea5ea34a417"},
    {"index-5201-15.fits.zst", "7e399e94b7a15c2b97e14f49b3999070"},
    {"index-5201-16.fits.zst", "7441074047de8bccd1c09570b122466d"},
    {"index-5201-17.fits.zst", "bb7f5979b0d7963420dabfc5dd58407c"},
    {"index-5201-18.fits.zst", "ca950e0190d849d709357bacce6fc1d0"},
    {"index-5201-19.fits.zst", "36b84a8ac921064ad1a89f1155af7b31"},
    {"index-5201-20.fits.zst", "25eeda073f427462e0064acf23a38498"},
    {"index-5201-21.fits.zst", "0bd79e677363442dc7e994b2f088cd27"},
    {"index-5201-22.fits.zst", "071abfb9131ca5a6cda792870f97bd8d"},
    {"index-5201-23.fits.zst", "56721c1918e7ac114d43602ec6b17402"},
    {"index-5201-24.fits.zst", "4409be2965dacf376b0124d8f7342c3c"},
    {"index-5201-25.fits.zst", "e784c443787e6c3b3b51e7c82701b3b6"},
    {"index-5201-26.fits.zst", "02e58904a47e3305dd2a2c1e754c2b56"},
    {"index-5201-27.fits.zst", "f4f37044f787349dfda36e9aab07c348"},
    {"index-5201-28.fits.zst", "69893cbd149173c98d496b3d62d23526"},
    {"index-5201-29.fits.zst", "d55efc9ffca98742f7575c0fa7cd9420"},
    {"index-5201-30.fits.zst", "014c94da04a6e94897af09001e08bad8"},
    {"index-5201-31.fits.zst", "376319584d0b6a66bcaced5b31f705d4"},
    {"index-5201-32.fits.zst", "00f2873b2468d103661e6938fed2d905"},
    {"index-5201-33.fits.zst", "fa1ce3020ec8511885472c0eda777cd7"},
    {"index-5201-34.fits.zst", "7c66e555866806d61f90769bc626ef32"},
    {"index-5201-35.fits.zst", "f1767cf0b802a97b939711f3ecd788c8"},
    {"index-5201-36.fits.zst", "76825b18fef6546bbbeef3f8538a06cb"},
    {"index-5201-37.fits.zst", "af507a214fc69c7daa0688fce2924c7e"},
    {"index-5201-38.fits.zst", "05fc75e562c612c51bf7bacb3907aa02"},
    {"index-5201-39.fits.zst", "3eeaabf9b945d71fafff7c282f9a3add"},
    {"index-5201-40.fits.zst", "f891a7def591965ad4aa4ddc9cfb7718"},
    {"index-5201-41.fits.zst", "48ef1d61841567de4d94d3dc366df643"},
    {"index-5201-42.fits.zst", "d2c8041bbada7df9dcc5614c35edd7f1"},
    {"index-5201-43.fits.zst", "24fc923bdc21f696b1da418a131dc2bc"},
    {"index-5201-44.fits.zst", "690eb483b2d60e1e31ff0e71e1c19167"},
    {"index-5201-45.fits.zst", "7b7972184b9bd5d485680cb10ad7f566"},
    {"index-5201-46.fits.zst", "e09515bdd779241b6871eb9130980924"},
    {"index-5201-47.fits.zst", "95583b10a270336b4cfb31153305b666"},
    {"index-5202-00.fits.zst", "c877e6a6790d62a77753bc0b5c1c471f"},
    {"index-5202-01.fits.zst", "2069168ce477a4b9c0659eb97d9d3f3e"},
    {"index-5202-02.fits.zst", "80b53bdc44addc02c5a9a47183ae405e"},
    {"index-5202-03.fits.zst", "fcef358afae1ac87e1072bf94c33919f"},
    {"index-5202-04.fits.zst", "fb6e067de3d8f59868fc5daad9e45ac1"},
    {"index-5202-05.fits.zst", "168861bd176f0c9283ef091b855cefe8"},
    {"index-5202-06.fits.zst", "c88d93502450e872004d952f5cc970c6"},
    {"index-5202-07.fits.zst", "0eb1b5b3b15212f734f150087872a84c"},
    {"index-5202-08.fits.zst", "03a110b7092787f0da40117d3daf4ee8"},
    {"index-5202-09.fits.zst", "10b89b70f19e0042c1a832dfbb0f157c"},
    {"index-5202-10.fits.zst", "6d55a5356f820b437137586037049392"},
    {"index-5202-11.fits.zst", "ee561de1f6ad229b1aec1d2d576cf2d6"},
    {"index-5202-12.fits.zst", "16bb2e40a0a71a91b4304c0e030d9f14"},
    {"index-5202-13.fits.zst", "d6259841cb5209f1fe2262a94ebba80e"},
    {"index-5202-14.fits.zst", "7fcabd9e89f560dae0ea9032817ffc95"},
    {"index-5202-15.fits.zst", "42c4006c6482e6a46ed81191d03a6e54"},
    {"index-5202-16.fits.zst", "a726672e54dd30367664781f533a5f48"},
    {"index-5202-17.fits.zst", "67fc64ba28344d9fd31143fc5123acb3"},
    {"index-5202-18.fits.zst", "97ca32bc2a0ab5313547bd01485902e1"},
    {"index-5202-19.fits.zst", "d261fb13fac3aa19e930d48c6cf13929"},
    {"index-5202-20.fits.zst", "7a67bc4e1d1dd003280f48815d244b52"},
    {"index-5202-21.fits.zst", "bbc66dabd84be8fbb47452807aa6cbd5"},
    {"index-5202-22.fits.zst", "264b65ac94678334ea5dfbc4b329f2ca"},
    {"index-5202-23.fits.zst", "657492ac072d1679d77abc8f532aa2c9"},
    {"index-5202-24.fits.zst", "7cbd56e15c84d8b0ad605983aa0eabcb"},
    {"index-5202-25.fits.zst", "5cd3457ec29821bfca8da6da1ef76684"},
    {"index-5202-26.fits.zst", "253639c9680bafbbfa465d5de51de235"},
    {"index-5202-27.fits.zst", "a891918b3c22f7b1e2876358a6e971e2"},
    {"index-5202-28.fits.zst", "69ee777be98231c104a2e28d2c349111"},
    {"index-5202-29.fits.zst", "5b9985f33d66e4da27d4c618565f35f4"},
    {"index-5202-30.fits.zst", "04d6b9acb868242cf3615ca9bef4c1d8"},
    {"index-5202-31.fits.zst", "24e98426ed5a60b12a6b5652b8f68ce6"},
    {"index-5202-32.fits.zst", "502ca42a47d5234aab0829a387242dfc"},
    {"index-5202-33.fits.zst", "253c838df836f569afe854cf598f0c79"},
    {"index-5202-34.fits.zst", "8dd8e8289e9925058c9cc11e7e76c3e3"},
    {"index-5202-35.fits.zst", "5b1bb19b81633bb3c2c8d1dff4bb8507"},
    {"index-5202-36.fits.zst", "7990d2b9a7f120df9095d5a93d3c94f2"},
    {"index-5202-37.fits.zst", "9e5f0ff891ff1b726df0001547ffd322"},
    {"index-5202-38.fits.zst", "f06244e6825e4ddb101482295b5294cb"},
    {"index-5202-39.fits.zst", "390f90dae3a4124cc4c7aa157e8c8597"},
    {"index-5202-40.fits.zst", "b2d380ef7974fc55f0bf31ebb62ee019"},
    {"index-5202-41.fits.zst", "ae8058e144898d1b786202345b6581cf"},
    {"index-5202-42.fits.zst", "1247b8a91c3a9d6b10a324247c9e02c6"},
    {"index-5202-43.fits.zst", "e01049718b0c6f4eb8884c647c2cdf17"},
    {"index-5202-44.fits.zst", "802f0e2d56c0e4ec3d8c6d69832102d7"},
    {"index-5202-45.fits.zst", "83fe2cff3cf65317f5c1bf7b953519e9"},
    {"index-5202-46.fits.zst", "f12f308a3b53d95ffd7bc420700e4f44"},
    {"index-5202-47.fits.zst", "608a14303810c9762b25fc68896d2a26"},
    {"index-5203-00.fits.zst", "2862efb33765b7bbefb635dcad970298"},
    {"index-5203-01.fits.zst", "2cd34cef4b44ad1e770396baccb2a46c"},
    {"index-5203-02.fits.zst", "40c9f67282210cc374281cde023c4e71"},
    {"index-5203-03.fits.zst", "c8f40e164ec3ce1df92e3a121a127716"},
    {"index-5203-04.fits.zst", "cb40c64cad1d99b55dcb0b645ae388aa"},
    {"index-5203-05.fits.zst", "fe1900531baaa1bb3c513b356befd522"},
    {"index-5203-06.fits.zst", "8d4b7d902bacbd478b3a372338887097"},
    {"index-5203-07.fits.zst", "0f18e0822ea6b67a8e5536680df16218"},
    {"index-5203-08.fits.zst", "4cd0aa9bf00f903f3c71b37e047dfd2d"},
    {"index-5203-09.fits.zst", "c34aeb0674c2cbd3de31e2d9b20708f0"},
    {"index-5203-10.fits.zst", "64c3f710c11b5e18743d93a1e9e204f2"},
    {"index-5203-11.fits.zst", "518ee18fae552e2fd83f664028219f28"},
    {"index-5203-12.fits.zst", "712d6cddc97f8c183c4d9a130ba87ca4"},
    {"index-5203-13.fits.zst", "185a056e25091c23bbfa425026b9897b"},
    {"index-5203-14.fits.zst", "e85ade3d5b7d1c98b5d9174fb520c154"},
    {"index-5203-15.fits.zst", "182d21f53ddbec1f3585936e6463b9e8"},
    {"index-5203-16.fits.zst", "c2cf948d5714d61ecb6a5e235885c5ea"},
    {"index-5203-17.fits.zst", "748862d448c996eda58ede16ea37b5a3"},
    {"index-5203-18.fits.zst", "d145bd1cba6ccc3948fca16fd04e7efe"},
    {"index-5203-19.fits.zst", "6a031fee285d47357c3cd98148c416c7"},
    {"index-5203-20.fits.zst", "d08f64480576cbcb3ce1f5625e82bc87"},
    {"index-5203-21.fits.zst", "5dcec75f91802cb05c36b185ea5b26de"},
    {"index-5203-22.fits.zst", "c76d7ad199114e77f4e05a290eff1de8"},
    {"index-5203-23.fits.zst", "3d8909cb4322a7b7baa3cd2e464269c8"},
    {"index-5203-24.fits.zst", "dd5a0ce7d08940fba606546140ccd38d"},
    {"index-5203-25.fits.zst", "bd27d2e07a96d7eceb26bbfff4eaf4f4"},
    {"index-5203-26.fits.zst", "8d82ba9557c9b4fea8ee1a16d1cc4bb9"},
    {"index-5203-27.fits.zst", "9f7e923674521562dd54903d8102bd0c"},
    {"index-5203-28.fits.zst", "3064ae36821a24d67b8c53000a6b67bc"},
    {"index-5203-29.fits.zst", "4eb82a64d7c9d8f7314cfda94e160e43"},
    {"index-5203-30.fits.zst", "e8cf8a17c62cf0ef09b61065d1bba527"},
    {"index-5203-31.fits.zst", "488fec71fc896c780aa970228d65f749"},
    {"index-5203-32.fits.zst", "8d09558d167283cf5bff4feca9202421"},
    {"index-5203-33.fits.zst", "c1f61ffaaee068d0a1d1829b71f46a30"},
    {"index-5203-34.fits.zst", "e2567ca06041ee6995f2cb9e282fe12b"},
    {"index-5203-35.fits.zst", "1c61653eb8851385a70adb15bbb8c836"},
    {"index-5203-36.fits.zst", "4d5360eea4e466121f3ffc0ad2574152"},
    {"index-5203-37.fits.zst", "95b713845864aa8418af634f16a0cb84"},
    {"index-5203-38.fits.zst", "7ccf07966a95072e621672dfc588d127"},
    {"index-5203-39.fits.zst", "0bf97501842e571c84a90d30c4b62c45"},
    {"index-5203-40.fits.zst", "f0ec8a7f888c225c749dd0ca6bb946be"},
    {"index-5203-41.fits.zst", "bd6fca77c9c0aae43de799b7ad823ab5"},
    {"index-5203-42.fits.zst", "71ffbc8755c943c67f8b67deda4a9d44"},
    {"index-5203-43.fits.zst", "4a48b878fd510a9bde3101acd7210cdb"},
    {"index-5203-44.fits.zst", "1ed3d2e05dd619d145d0aac46dd69320"},
    {"index-5203-45.fits.zst", "70e4d9fb4b5d66fc24990310cfc913d4"},
    {"index-5203-46.fits.zst", "ecd1d7b1cb94ba52031314d189bd2390"},
    {"index-5203-47.fits.zst", "894ae74eb43a8f34ad06edea62bd4337"},
    {"index-5204-00.fits.zst", "6bdb9974308249e68f1ed707d6951848"},
    {"index-5204-01.fits.zst", "c10ce6a6d2375bcf3e3babced3722ecd"},
    {"index-5204-02.fits.zst", "9e7ed423196691e4c9f38449957860bd"},
    {"index-5204-03.fits.zst", "60ccf82d3d7443423c84c789ad5d5604"},
    {"index-5204-04.fits.zst", "e8ba7567c5bda04c4fb58bb93454b8ed"},
    {"index-5204-05.fits.zst", "1f36a1432c055fc96582642ea5c853b2"},
    {"index-5204-06.fits.zst", "bb8a67b877eeccdfef5668796a677f4f"},
    {"index-5204-07.fits.zst", "2c547a8abd2410530a7547db80c40eaa"},
    {"index-5204-08.fits.zst", "5be1251fcc27f3f95c38a87ba6e0335d"},
    {"index-5204-09.fits.zst", "291cbc557df140dc3caccad105f9d515"},
    {"index-5204-10.fits.zst", "b155a2c52e3b5a3d99d0fa5b112cd1e4"},
    {"index-5204-11.fits.zst", "0a21c7bff80b6225f00e9c2213282003"},
    {"index-5204-12.fits.zst", "2ca005ea103d668ebdd2f07d215dc824"},
    {"index-5204-13.fits.zst", "6a3677a3e55af336dbbaa7db29492c1d"},
    {"index-5204-14.fits.zst", "00c4922987950b875ddb6d68cf22dbf2"},
    {"index-5204-15.fits.zst", "4faec4fdaae6ab10d91e42f77a8786b2"},
    {"index-5204-16.fits.zst", "211cb590033d680cabfa3559243bbe0f"},
    {"index-5204-17.fits.zst", "f8c27d1b6448ca442b5ec13d09d161ca"},
    {"index-5204-18.fits.zst", "6a264515e128f61b89a5ec94d649aa05"},
    {"index-5204-19.fits.zst", "d24b3fd902dbea191953d173bf85627a"},
    {"index-5204-20.fits.zst", "a240cf519935d77ebda8a1ba89629b19"},
    {"index-5204-21.fits.zst", "8633a5f455a70b089916bb952649abc5"},
    {"index-5204-22.fits.zst", "a64cc9fc8dc5d38d530d161dde40adb0"},
    {"index-5204-23.fits.zst", "639bf9f5433a272b9208094435dfacf0"},
    {"index-5204-24.fits.zst", "20eece3a49f82fe2ae575bce9bc57dc9"},
    {"index-5204-25.fits.zst", "6895bc172752aa20a9975e9123d6867b"},
    {"index-5204-26.fits.zst", "9cb92cd20d8060dcf8c694b670800a19"},
    {"index-5204-27.fits.zst", "09894bc3185f68b49cf2eb0cc7eacebe"},
    {"index-5204-28.fits.zst", "bb5a2a09b531d2ca13f341cb0b00041b"},
    {"index-5204-29.fits.zst", "f2d5f146ff97b86dfb4b59c8636c69d8"},
    {"index-5204-30.fits.zst", "cb8bec9885e23cce0d86a94a886858ff"},
    {"index-5204-31.fits.zst", "ff92d11ee8aebd9e4cd7c63006e2ba0f"},
    {"index-5204-32.fits.zst", "5bc007791035420ab06a8a8dee13f50b"},
    {"index-5204-33.fits.zst", "98305f6ec87af98d0a7fb82f6cb38397"},
    {"index-5204-34.fits.zst", "5af466f48514b9bec75e877e3aa348e7"},
    {"index-5204-35.fits.zst", "f84d32ef9278e2fa0aa013334ebddedc"},
    {"index-5204-36.fits.zst", "5e7afe529e949d83812c15ca66e5fbe4"},
    {"index-5204-37.fits.zst", "091d775d07623d86adf0c6f0d61da00f"},
    {"index-5204-38.fits.zst", "61c3b59cd6614357da8427887fa1d7be"},
    {"index-5204-39.fits.zst", "ad5687f7e7e6d65c25f52696a5be73fb"},
    {"index-5204-40.fits.zst", "d95ca1f3d0abe527518ad3c4797e3b69"},
    {"index-5204-41.fits.zst", "4d49cd25ea1cf1c348916b39f026a6e1"},
    {"index-5204-42.fits.zst", "7f517937c94d9db3d7515cadb5cd3b10"},
    {"index-5204-43.fits.zst", "f3d336795a32af76d61742c9a29bfb14"},
    {"index-5204-44.fits.zst", "3372d5b85a802f891acdadfc65e05893"},
    {"index-5204-45.fits.zst", "7e6c52552bf25c63af732ec7243d8766"},
    {"index-5204-46.fits.zst", "5ce56079d24213af35d9ec730e12121f"},
    {"index-5204-47.fits.zst", "d33d355ad900766c8fcdd53522124d01"},
    {"index-5205-00.fits.zst", "d95511d75f6915caed5a4cf010e51056"},
    {"index-5205-01.fits.zst", "53857e19e4ff54360ed9335c35d20ac8"},
    {"index-5205-02.fits.zst", "f8bdcd851d44da92a4a90bc71deb0782"},
    {"index-5205-03.fits.zst", "4782fc867bd02c58140daecc7a4f9cab"},
    {"index-5205-04.fits.zst", "b63b9bfdda4a85e9377b512038aa9627"},
    {"index-5205-05.fits.zst", "ffb688a56d6dc70842765a7e1fdc9ca7"},
    {"index-5205-06.fits.zst", "8d906365279b2f41baa7fedd76683619"},
    {"index-5205-07.fits.zst", "82ec7cc676c9ef825f218fceb236d216"},
    {"index-5205-08.fits.zst", "29171a06fd40f5c5df6e637550bc7626"},
    {"index-5205-09.fits.zst", "dca0e789c482eef07bee53100e10f73a"},
    {"index-5205-10.fits.zst", "7d66c8c27198481c587c1432275feced"},
    {"index-5205-11.fits.zst", "ce66e30646b02e7128a004cda4240b6d"},
    {"index-5205-12.fits.zst", "6a42dcd534efb467a0a53c69a6047866"},
    {"index-5205-13.fits.zst", "950331af7d668da1006c1b6902fd6439"},
    {"index-5205-14.fits.zst", "ac6c30027cd93e91b5baf6e344032254"},
    {"index-5205-15.fits.zst", "3c8d77076a49d3dc051089df8025308b"},
    {"index-5205-16.fits.zst", "5c7a0c57f7bf6fcc886c7518adc2b882"},
    {"index-5205-17.fits.zst", "6daa68b68104426b3e92a433107e565e"},
    {"index-5205-18.fits.zst", "74b94ab3f7ee6a260560b5d78614df30"},
    {"index-5205-19.fits.zst", "01597167da7a9e6fde3ace7d6e9c6788"},
    {"index-5205-20.fits.zst", "3dffc55b7ab5c15e1c689c0d73f880f6"},
    {"index-5205-21.fits.zst", "bfa484d631819e2a2b7a8d3dec337a9b"},
    {"index-5205-22.fits.zst", "de2a3ebbf56bb640411e0a50ed0653eb"},
    {"index-5205-23.fits.zst", "5498579da779e625617140b04a88659c"},
    {"index-5205-24.fits.zst", "41589963565a4d1d056ac2551c94bc5b"},
    {"index-5205-25.fits.zst", "88dac5e97a8e3cccd4962ee9d1f062fb"},
    {"index-5205-26.fits.zst", "528044ec968e08a1347f97d2d58bc9f8"},
    {"index-5205-27.fits.zst", "50890dbe9394c9101138f781394a62da"},
    {"index-5205-28.fits.zst", "da541fb011826588a7ff682d3fc1065f"},
    {"index-5205-29.fits.zst", "96873ae405bd9ac727656d4fbf3c508c"},
    {"index-5205-30.fits.zst", "e320dad418e7e64bbd4700e074af97b6"},
    {"index-5205-31.fits.zst", "5cb52c69ad1a9b780dddd82da4295f01"},
    {"index-5205-32.fits.zst", "af2f00cbfc50a82138f01ee26b9e9d91"},
    {"index-5205-33.fits.zst", "0e3abcccf8295f99b846e69e0a82ee55"},
    {"index-5205-34.fits.zst", "02220a844210cbab3dbf32f15f25d6ff"},
    {"index-5205-35.fits.zst", "21380e2a86b908f5cef98cd5b2ba5fc5"},
    {"index-5205-36.fits.zst", "5898e4e3b3f4961420124fe23c106e7e"},
    {"index-5205-37.fits.zst", "12a7eebcfcb9871366f27bab7bd7c02b"},
    {"index-5205-38.fits.zst", "ad7ae57547afae6d7e7b5bfde0f2dd4e"},
    {"index-5205-39.fits.zst", "ce92be215ddb055395db6ff1469a13c5"},
    {"index-5205-40.fits.zst", "21f0f02bf765bea7577e9c379cc32aaf"},
    {"index-5205-41.fits.zst", "d7bb45a9cc162262cf860554cb577cb3"},
    {"index-5205-42.fits.zst", "923d46b2900879a7deb9c07a71a5a604"},
    {"index-5205-43.fits.zst", "12036538e03f7e87e7e5197a176bfbeb"},
    {"index-5205-44.fits.zst", "763625ff1a99a09010b4d29ee26c45f5"},
    {"index-5205-45.fits.zst", "515b596b4ccb4684d84ac5bae00c3ec7"},
    {"index-5205-46.fits.zst", "69c36255820c21846ce066ef9727ad9c"},
    {"index-5205-47.fits.zst", "7208bf7057c156f68f8797055279c396"},
    {"index-5206-00.fits.zst", "ec763f6717dc23aa74f0c37d37bbc79d"},
    {"index-5206-01.fits.zst", "77c60eb07dca413177f265fd3a7358d1"},
    {"index-5206-02.fits.zst", "7b04e7e1bdd5d10a7ecd8784458dfe3a"},
    {"index-5206-03.fits.zst", "ff096041f96c1a928583277d53b70754"},
    {"index-5206-04.fits.zst", "edfab290c5d79b16142e8e29b930276e"},
    {"index-5206-05.fits.zst", "01842535f9cd6cabebdbc99eba0c2469"},
    {"index-5206-06.fits.zst", "8c191abe714e0e2c709bf1b3f1e46534"},
    {"index-5206-07.fits.zst", "221bc2471617105004d213b44238fa41"},
    {"index-5206-08.fits.zst", "c04330df6a106b55618cc0d0467c349d"},
    {"index-5206-09.fits.zst", "ac6d28cad4716da936f5f9878ecab761"},
    {"index-5206-10.fits.zst", "bd79f130d1931d167a2a9cf801b05cfd"},
    {"index-5206-11.fits.zst", "2e50c634e80b32ca13d643e0535a37c1"},
    {"index-5206-12.fits.zst", "c132774b1cb656056d04e8175948559f"},
    {"index-5206-13.fits.zst", "bc491d7a0a773f9e499b9f18f4cc2d26"},
    {"index-5206-14.fits.zst", "9209a393a7341d08982925936d587178"},
    {"index-5206-15.fits.zst", "416f3f4c655fdc56504030442d52f21b"},
    {"index-5206-16.fits.zst", "9e6f16e687376c17c15d3f2bb7621b8f"},
    {"index-5206-17.fits.zst", "4f131eff7aa8eee019dd081a250f15bd"},
    {"index-5206-18.fits.zst", "7535d000a0d9ef54c1e50202319f2a4d"},
    {"index-5206-19.fits.zst", "2a20fb3cf2f2bd39c9d8f0efa376e5ea"},
    {"index-5206-20.fits.zst", "0e08f721f97341a0f737b4d9ffc1bafc"},
    {"index-5206-21.fits.zst", "aa2b2719031262219b9e105853655d84"},
    {"index-5206-22.fits.zst", "c4966b370e8e0abe7c0827712419b63f"},
    {"index-5206-23.fits.zst", "baff2c6b458965754a33c2b8e4ddae30"},
    {"index-5206-24.fits.zst", "f9c37d9dadf7b5c10e417909b89dd0f6"},
    {"index-5206-25.fits.zst", "c420b02c3f701459762ffd24d3ee0b7f"},
    {"index-5206-26.fits.zst", "f8c296fe490a6449233787f7b2275f7d"},
    {"index-5206-27.fits.zst", "b63de1ef274c7b3482ec49b038c95e4a"},
    {"index-5206-28.fits.zst", "0e91227a868e4d626d05f8556fd385db"},
    {"index-5206-29.fits.zst", "010af2760055eb0b0f139f26808d3d0a"},
    {"index-5206-30.fits.zst", "bb75c13afb642f8d1039627885591adf"},
    {"index-5206-31.fits.zst", "9cbec1344ba47dd477d6d8a1f680527e"},
    {"index-5206-32.fits.zst", "74e832eb93be5e6d58793418253c1b1c"},
    {"index-5206-33.fits.zst", "303d2fecce3f69914d2aec9b137cd65b"},
    {"index-5206-34.fits.zst", "b2fa4d6404f11552dd0ae3212a893813"},
    {"index-5206-35.fits.zst", "18533d28093a1b01ba0a17811237c9c2"},
    {"index-5206-36.fits.zst", "7f8a6ce0c1e0fe7998a047172bee9390"},
    {"index-5206-37.fits.zst", "eef8a7d8de31e0865de6a2563cd54602"},
    {"index-5206-38.fits.zst", "27d0395d69aa600d2c334664ee88191d"},
    {"index-5206-39.fits.zst", "455e2ee060c16a560e62df0bf6790027"},
    {"index-5206-40.fits.zst", "9d7f12a0adfb97d7d3b904bf6f8788c4"},
    {"index-5206-41.fits.zst", "d05556c1d3c15f0cdb72363a82ab6d7c"},
    {"index-5206-42.fits.zst", "99268fca7e2a9161f4f1c144b13dea3a"},
    {"index-5206-43.fits.zst", "e3b6becbf0949d9c40dac1d366805493"},
    {"index-5206-44.fits.zst", "eb6802ea492c8ab920699a47cd8e5ccf"},
    {"index-5206-45.fits.zst", "d3f692ee8ee9d6c9d3483818f2b81584"},
    {"index-5206-46.fits.zst", "aff3a7ba7140e5e850c1395fba6402c0"},
    {"index-5206-47.fits.zst", "27b479b738a7cd3379e105638b1fc43e"}
};

Download::Download(QNetworkReply *reply, const QString indexPath, QObject *parent) : QObject(parent)
    ,_reply(reply)
    ,_hash(QCryptographicHash::Md5)
{
    connect(_reply, &QNetworkReply::finished, this, &Download::finished);
    connect(_reply, &QNetworkReply::readyRead, this, &Download::readData);
    connect(_reply, &QNetworkReply::downloadProgress, this, &Download::progress);

    QString filename = _reply->url().fileName();
    filename.remove(QRegularExpression("\\.zst$"));

    _fw.setFileName(indexPath + "/" + filename);
    _fw.open(QIODevice::WriteOnly | QIODevice::Truncate);
    if(_fw.isOpen())
    {
        qDebug() << "open file" << _fw.fileName();
    }

    _dstream = ZSTD_createDStream();
}

Download::~Download()
{
    ZSTD_freeDStream(_dstream);
}

void Download::abort()
{
    _reply->abort();
}

void Download::readData()
{
    QByteArray data = _reply->readAll();
    decompress(data);
}

void Download::finished()
{
    if(_reply->error() == QNetworkReply::NoError)
    {
        QByteArray data = _reply->readAll();
        qDebug() << "finished" << data.size();
        decompress(data);

        if(md5.contains(_reply->url().fileName()))
        {
            if(_hash.result().toHex() == md5[_reply->url().fileName()])
                qDebug() << "DOWNLOAD OK";
            else
            {
                qDebug() << "DOWNLOAD BAD";
                _fw.remove();
                return;
            }
        }

        _fw.flush();
        _fw.close();
    }
    else
    {
        qDebug() << "Failed to perform http request" << _reply->url();
        _fw.remove();
    }
}

void Download::decompress(QByteArray &data)
{
    if(data.isEmpty())return;

    _hash.addData(data);

    ZSTD_inBuffer inBuffer = {data.constData(), static_cast<size_t>(data.size()), 0};
    QByteArray outData(ZSTD_DStreamOutSize(), '\0');
    while(inBuffer.pos < inBuffer.size)
    {
        ZSTD_outBuffer outBuffer = {outData.data(), static_cast<size_t>(outData.size()), 0};
        size_t ret = ZSTD_decompressStream(_dstream, &outBuffer, &inBuffer);
        if(ZSTD_isError(ret))
        {
            qDebug() << "decompress error" << ZSTD_getErrorName(ret);
            _fw.remove();
            _reply->abort();
            break;
        }
        else if(outBuffer.pos)
        {
            _fw.write(static_cast<char*>(outBuffer.dst), outBuffer.pos);
        }
    }
}

HttpDownloader::HttpDownloader(QObject *parent) : QObject(parent)
    ,_manager(new QNetworkAccessManager(this))
{
    _manager->setAutoDeleteReplies(true);
    connect(_manager, &QNetworkAccessManager::finished, this, &HttpDownloader::finished);
#ifdef PLATESOLVER
    QDir dir(Solver::getTenmonIndexPath());
    if(!dir.exists())
    {
        if(dir.mkpath("."))
            qDebug() << "Failed to create astrometry directory";
    }

    _indexPath = dir.absolutePath();
#endif
}

void HttpDownloader::download(const QUrl &url)
{
    if(!_queue.contains(url))
        _queue.enqueue(url);

    if(!_download)
        finished();
}

bool HttpDownloader::downloadIndex(int scale)
{
    if(scale > 19 || scale < 1)
        return false;

    QUrl url("https://tenmon.nouspiro.space/");
    QStringList files = indexFileNames(scale);

    for(auto &file : files)
    {
        if(QFile::exists(_indexPath + "/" + file))
        {
            qDebug() << "File already exists, skipping" << file;
        }
        else
        {
            url.setPath("/astrometry/" + file + ".zst");
            download(url);
        }
    }
    return true;
}

void HttpDownloader::abort()
{
    if(_download)
        _download->abort();
}

QStringList HttpDownloader::indexFileNames(int scale)
{
    QStringList ret;

    if(scale >= 7)
    {
        ret.append(QString("index-%1.fits").arg(4100 + scale));
    }
    else
    {
        for(int i=0; i<48; i++)
            ret.append(QString("index-%1-%2.fits").arg(5200 + scale).arg(i, 2, 10, QChar('0')));
    }
    return ret;
}

void HttpDownloader::finished()
{
    if(_queue.isEmpty())
    {
        _download = nullptr;
    }
    else
    {
        QUrl url = _queue.dequeue();
        QString filename = url.fileName();
        filename.remove(QRegularExpression("\\.zst$"));
        QFileInfo info(_indexPath + "/" + filename);
        if(info.exists())
        {
            finished();
            return;
        }
        QNetworkRequest request(url);
        _download = new Download(_manager->get(request), _indexPath, this);
        connect(_download, &Download::progress, this, &HttpDownloader::updateProgress);
    }
}

void HttpDownloader::updateProgress(qint64 received, qint64 total)
{
    emit progress((float)received / total * 100.0f, _queue.size());
}

