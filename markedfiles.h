#ifndef MARKEDFILES_H
#define MARKEDFILES_H

#include <QDialog>
#include <QTableView>
#include <QSqlTableModel>

class MarkedFiles : public QDialog
{
    Q_OBJECT
    QTableView *m_tableView;
    QSqlTableModel *m_model;
public:
    MarkedFiles(QWidget *parent = nullptr);
protected slots:
    void clearSelected();
    void clearAll();
};

#endif // MARKEDFILES_H
