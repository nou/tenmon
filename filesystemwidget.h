#ifndef FILESYSTEMWIDGET_H
#define FILESYSTEMWIDGET_H

#include <QWidget>
#include <QFileSystemModel>
#include <QListView>
#include <QTreeView>

class FilesystemWidget : public QWidget
{
    Q_OBJECT
    QListView *m_listView;
    QAbstractItemModel *m_model;
public:
    explicit FilesystemWidget(QAbstractItemModel *model, QWidget *parent = nullptr);
    void contextMenuEvent(QContextMenuEvent *event) override;
public slots:
    void selectFile(int row);
protected slots:
    void fileClicked(const QModelIndex &index, const QModelIndex &);
signals:
    void fileSelected(int row);
    void sortChanged(QDir::SortFlag sort);
    void reverseSort();
};

class FileSystemModel : public QFileSystemModel
{
public:
    explicit FileSystemModel(QObject *parent) : QFileSystemModel(parent){}
    QVariant data(const QModelIndex &index, int role) const override;
};

class Filetree : public QTreeView
{
    Q_OBJECT
    FileSystemModel *m_fileSystemModel;
    QString m_rootDir;
public:
    explicit Filetree(QWidget *parent = nullptr);
    ~Filetree() override;
    void contextMenuEvent(QContextMenuEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
signals:
    void fileSelected(const QString &path);
    void copyFiles(const QString &path);
    void moveFiles(const QString &path);
    void indexDirectory(const QString &path);
};

#endif // FILESYSTEMWIDGET_H
