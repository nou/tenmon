#ifndef PLATESOLVING_H
#define PLATESOLVING_H

#include "qelapsedtimer.h"
#include <QDockWidget>

class Solver;
class RawImage;
class Image;

namespace Ui {
class PlateSolving;
}

class PlateSolving : public QDockWidget
{
    Q_OBJECT
    Solver *_solver;
    std::shared_ptr<RawImage> _rawImage;
    QString _path;
    QElapsedTimer _solvingTime;
public:
    explicit PlateSolving(QWidget *parent = nullptr);
    ~PlateSolving();

public slots:
    void extract();
    void extractionDone();
    void solve();
    void solvingDone();
    void abort();
    void updateHeader();
    void imageLoaded(Image *image);
    void settings();
private:
    Ui::PlateSolving *_ui;
};

#endif // PLATESOLVING_H
