#include "settingsdialog.h"
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QSettings>
#include <QApplication>
#include "rawimage.h"

extern int DEFAULT_WIDTH;
extern double SATURATION;
extern int FILTERING;

class EvenNumber : public QSpinBox
{
public:
    explicit EvenNumber(QWidget *parent) : QSpinBox(parent){}
protected:
    QValidator::State validate(QString &text, int &) const
    {
        bool ok;
        int val = text.toInt(&ok);
        if(ok && (val & 1) == 0)return QValidator::Acceptable;
        if(ok)return QValidator::Intermediate;
        return QValidator::Invalid;
    }
    void fixup(QString &input) const
    {
        bool ok;
        int val = input.toInt(&ok);
        val -= val & 1;
        input = QString::number(val);
    }
};

SettingsDialog::SettingsDialog(QWidget *parent) : QDialog(parent)
{
    QFormLayout *layout = new QFormLayout(this);
    setWindowTitle(tr("Settings"));

    QSettings settings;

    m_preloadImages = new QSpinBox(this);
    m_preloadImages->setRange(0, 32);
    m_preloadImages->setValue(settings.value("settings/preloadimagecount", DEFAULT_WIDTH).toInt());
    m_preloadImages->setToolTip(tr("How many images are preloaded before and after current image."));

    m_thumSize = new EvenNumber(this);
    m_thumSize->setRange(64, 512);
    m_thumSize->setSingleStep(2);
    m_thumSize->setValue(settings.value("settings/thumnailsize", THUMB_SIZE).toInt());
    m_thumSize->setToolTip(tr("Thumbnail size in pixels"));

    m_saturation = new QDoubleSpinBox(this);
    m_saturation->setMinimum(0);
    m_saturation->setMaximum(100);
    m_saturation->setSuffix(" %");
    m_saturation->setValue(settings.value("settings/saturation", SATURATION * 100.0).toDouble());
    m_saturation->setToolTip(tr("Set threshold value that is considered saturated when showing statistics.\nFor RAW files you may set 22%"));

    m_slideShowTime = new QDoubleSpinBox(this);
    m_slideShowTime->setMinimum(0.01);
    m_slideShowTime->setMaximum(10);
    m_slideShowTime->setSuffix(" s");
    m_slideShowTime->setValue(settings.value("settings/slideshowtime", 1.0).toDouble());
    m_slideShowTime->setSingleStep(0.1);

    m_useNativeDialog = new QCheckBox(tr("Don't use native file dialog"), this);
    m_useNativeDialog->setChecked(QApplication::testAttribute(Qt::AA_DontUseNativeDialogs));

    m_filtering = new QComboBox(this);
    m_filtering->addItems({tr("Nearest"), tr("Linear"), tr("Cubic")});
    m_filtering->setCurrentIndex(FILTERING);

    layout->addRow(tr("Image preload count"), m_preloadImages);
    layout->addRow(tr("Thumbnails size"), m_thumSize);
    layout->addRow(tr("Saturation"), m_saturation);
    layout->addRow(tr("Slideshow interval"), m_slideShowTime);
    layout->addRow(tr("Image filtering"), m_filtering);
    layout->addRow(m_useNativeDialog);
    //layout->addRow(new QLabel(tr("Changes in settings will take effect after program restart.")));

    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    buttonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    connect(this, &QDialog::accepted, this, &SettingsDialog::saveSettings);

    layout->addRow(buttonBox);
}

void SettingsDialog::loadSettings()
{
    QSettings settings;
    THUMB_SIZE = settings.value("settings/thumbnailsize", THUMB_SIZE).toInt();
    THUMB_SIZE_BORDER = THUMB_SIZE + 10;
    THUMB_SIZE_BORDER_Y = THUMB_SIZE + 30;
    DEFAULT_WIDTH = settings.value("settings/preloadimagecount", DEFAULT_WIDTH).toInt();
    SATURATION = settings.value("settings/saturation", 95.0).toDouble() / 100.0;
    FILTERING = settings.value("settings/filtering", FILTERING).toInt();
    QApplication::setAttribute(Qt::AA_DontUseNativeDialogs, settings.value("settings/dontusenativedialogs", false).toBool());
}

bool SettingsDialog::loadThumbsizes()
{
    QSettings settings;
    int OLD_THUMB_SIZE = THUMB_SIZE;
    THUMB_SIZE = settings.value("settings/thumbnailsize", THUMB_SIZE).toInt();
    THUMB_SIZE_BORDER = THUMB_SIZE + 10;
    THUMB_SIZE_BORDER_Y = THUMB_SIZE + 30;
    return OLD_THUMB_SIZE != THUMB_SIZE;
}

void SettingsDialog::saveSettings()
{
    QSettings settings;
    settings.setValue("settings/thumbnailsize", m_thumSize->value());
    settings.setValue("settings/preloadimagecount", m_preloadImages->value());
    settings.setValue("settings/dontusenativedialogs", m_useNativeDialog->isChecked());
    settings.setValue("settings/saturation", m_saturation->value());
    settings.setValue("settings/slideshowtime", m_slideShowTime->value());
    FILTERING = m_filtering->currentIndex();
    settings.setValue("settings/filtering", FILTERING);
    SATURATION = m_saturation->value() / 100.0;
    QApplication::setAttribute(Qt::AA_DontUseNativeDialogs, m_useNativeDialog->isChecked());
    if(DEFAULT_WIDTH != m_preloadImages->value())
        emit preloadChanged(m_preloadImages->value());
}
