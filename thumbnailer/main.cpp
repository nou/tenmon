#include <QCoreApplication>
#include <QCommandLineParser>
#include "../rawimage.h"
#include "../loadimage.h"

bool OpenGLES = false;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.addOption({{"s", "size"}, "Size of the thumbnail in pixels (default: 128)", "size", "128"});
    parser.addPositionalArgument("input", "Input image file");
    parser.addPositionalArgument("output", "Output image file");
    parser.addHelpOption();

    parser.process(a);

    QStringList args = parser.positionalArguments();

    if(args.size() < 2)
        return 1;

    QString input = args[0];
    QString output = args[1];

    ImageInfoData info;
    std::shared_ptr<RawImage> rawImage;
    if(!loadImage(input, info, rawImage))
        return 1;

    if(!rawImage)
        return 2;

    rawImage->convertToType(RawImage::UINT8);

    QImage img((const uchar*)rawImage->data(), rawImage->width(), rawImage->height(), QImage::Format_RGBA8888);
    bool ok = false;
    int size = parser.value("s").toInt(&ok);
    if(!ok)size = 128;
    img = img.scaled(size, size, Qt::KeepAspectRatio);
    img.save(output, "png");

    //rawImage->convertTosRGB();

    return 0;
}
