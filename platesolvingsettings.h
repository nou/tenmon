#ifndef PLATESOLVINGSETTINGS_H
#define PLATESOLVINGSETTINGS_H

#include <QDialog>
#include <QFileSystemWatcher>
#include "httpdownloader.h"

namespace Ui {
class PlateSolvingSettings;
}

class PlateSolvingSettings : public QDialog
{
    Q_OBJECT
    HttpDownloader *_download;
    QFileSystemWatcher *_watcher;
public:
    explicit PlateSolvingSettings(QWidget *parent = nullptr);
    ~PlateSolvingSettings();
    void checkIndexFiles();
    QString indexDirectory() const;
protected slots:
    void progress(int percent, int files);
private:
    Ui::PlateSolvingSettings *_ui;
};

#endif // PLATESOLVINGSETTINGS_H
