#include "statusbar.h"
#include <QFontMetrics>

StatusBar::StatusBar(QWidget *parent) : QStatusBar(parent)
{
    m_value = new QLabel(this);
    m_pixelCoords = new QLabel(this);
    m_celestianCoords = new QLabel(this);

    m_value->setMinimumWidth(m_value->fontMetrics().horizontalAdvance("R:65536 G:65536 B:65536     "));
    m_pixelCoords->setMinimumWidth(m_pixelCoords->fontMetrics().horizontalAdvance("X:65536 Y:65536     "));
    m_celestianCoords->setMinimumWidth(m_celestianCoords->fontMetrics().horizontalAdvance("RA: 00h00m00s DEC: 00° 00' 00\"     "));
    addPermanentWidget(m_value);
    addPermanentWidget(m_pixelCoords);
    addPermanentWidget(m_celestianCoords);
}

void StatusBar::newStatus(const QString &value, const QString &pixelCoords, const QString &celestialCoords)
{
    m_value->setText(value);
    m_pixelCoords->setText(pixelCoords);
    m_celestianCoords->setText(celestialCoords);
}
