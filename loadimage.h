#ifndef LOADIMAGE_H
#define LOADIMAGE_H

#include <QString>
#include "imageinfodata.h"

class RawImage;

QString makeUNCPath(const QString &path);
bool readFITSHeader(const QString &path, ImageInfoData &info);
bool readXISFHeader(const QString &path, ImageInfoData &info);
bool loadImage(const QString &path, ImageInfoData &info, std::shared_ptr<RawImage> &rawImage, bool planar = false);

#endif // LOADIMAGE_H
