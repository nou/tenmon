#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>
#include <QTranslator>
#include <stdlib.h>
#include "libxisf.h"

int main(int argc, char *argv[])
{
#ifdef __linux__
    setenv("LC_NUMERIC", "C", 1);
#endif

    QSurfaceFormat format;
    format.setMajorVersion(3);
    format.setMinorVersion(3);
    //format.setOption(QSurfaceFormat::DebugContext);
    format.setProfile(QSurfaceFormat::OpenGLContextProfile::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    QApplication a(argc, argv);
    a.setOrganizationName("nou");
    a.setApplicationName("Tenmon");
    a.setWindowIcon(QIcon(":/space.nouspiro.tenmon.png"));

    QTranslator translator;
    QTranslator translator2;
    if(translator.load(QLocale(), "tenmon", "_", ":/translations"))
        a.installTranslator(&translator);
    if(translator2.load(QLocale(), "tenmon", "_", a.applicationDirPath()))
        a.installTranslator(&translator2);

    MainWindow w;
    w.show();

    return a.exec();
}
