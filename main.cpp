#include "mainwindow.h"
#include <QApplication>
#include <QSurfaceFormat>
#include <QTranslator>
#include <stdlib.h>

int main(int argc, char *argv[])
{
#ifdef __linux__
    setenv("LC_NUMERIC", "C", 1);
#endif

#if defined(__i386__) || defined(__x86_64__) || defined(__APPLE__)
    bool useGLES = false;
#else
    bool useGLES = true;
#endif
    for(int i = 0; i < argc; i++)
    {
        if(std::strcmp("-gl", argv[i]) == 0)
            useGLES = false;
        if(std::strcmp("-gles", argv[i]) == 0)
            useGLES = true;
    }

    QSurfaceFormat format;
    if(useGLES)
    {
        format.setMajorVersion(3);
        format.setMinorVersion(0);
        format.setRenderableType(QSurfaceFormat::OpenGLES);
    }
    else
    {
        format.setMajorVersion(3);
        format.setMinorVersion(3);
        //format.setOption(QSurfaceFormat::DebugContext);
        format.setProfile(QSurfaceFormat::OpenGLContextProfile::CoreProfile);
    }
    QSurfaceFormat::setDefaultFormat(format);

    QApplication a(argc, argv);
    a.setOrganizationName("nou");
    a.setApplicationName("Tenmon");
    a.setWindowIcon(QIcon(":/space.nouspiro.tenmon.png"));

    QTranslator translator;
    QTranslator translator2;
    if(translator.load(QLocale(), "tenmon", "_", ":/translations"))
        a.installTranslator(&translator);
    if(translator2.load(QLocale(), "tenmon", "_", a.applicationDirPath()))
        a.installTranslator(&translator2);

    MainWindow w;
    w.show();

    return a.exec();
}
