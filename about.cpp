#include "about.h"
#include <QTextEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QFile>
#include <QLocale>
#include "gitversion.h"

About::About(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("About Tenmon"));

    QVBoxLayout *layout = new QVBoxLayout(this);
    QLabel *label = new QLabel(this);

    QFile tenmonText(":/about/tenmon");
    tenmonText.open(QIODevice::ReadOnly);
    QByteArray text = tenmonText.readAll();
    text.replace("@GITVERSION@", GITVERSION);
    label->setText(text);
    label->setOpenExternalLinks(true);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);

    layout->addWidget(label);
    layout->addWidget(buttonBox);
}

HelpDialog::HelpDialog(QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Help"));
    resize(800, 600);

    QLocale locale;
    QString l = QLocale::languageToString(locale.language());

    QVBoxLayout *layout = new QVBoxLayout(this);
    QTextEdit *helpText = new QTextEdit(this);
    helpText->setReadOnly(true);

    QFile tenmonText(":/help");
    tenmonText.open(QIODevice::ReadOnly);
    helpText->setHtml(tenmonText.readAll());

    layout->addWidget(helpText);
}
