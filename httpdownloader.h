#ifndef HTTPDOWNLOADER_H
#define HTTPDOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QFile>
#include <QQueue>
#include <QCryptographicHash>
#include <zstd.h>

class Download : public QObject
{
    Q_OBJECT
    QNetworkReply *_reply;
    ZSTD_DStream *_dstream;
    QFile _fw;
    QCryptographicHash _hash;
public:
    Download(QNetworkReply *reply, const QString indexPath, QObject *parent);
    ~Download();
    void abort();
public slots:
    void readData();
    void finished();
signals:
    void progress(qint64 received, qint64 total);
protected:
    void decompress(QByteArray &data);
};

class HttpDownloader : public QObject
{
    Q_OBJECT
    QNetworkAccessManager *_manager;
    Download *_download = nullptr;
    QQueue<QUrl> _queue;
    QString _indexPath;
public:
    explicit HttpDownloader(QObject *parent = nullptr);
    void download(const QUrl &url);
    // scale in range 19-1
    bool downloadIndex(int scale);
    void abort();
    static QStringList indexFileNames(int scale);
signals:
    void progress(int percent, int files);
protected slots:
    void finished();
    void updateProgress(qint64 received, qint64 total);
};

#endif // HTTPDOWNLOADER_H
