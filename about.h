#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>

class About : public QDialog
{
    Q_OBJECT
public:
    About(QWidget *parent = nullptr);
};

class HelpDialog : public QDialog
{
    Q_OBJECT
public:
    HelpDialog(QWidget *parent = nullptr);
};

#endif // ABOUT_H
