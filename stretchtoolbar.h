#ifndef STRETCHTOOLBAR_H
#define STRETCHTOOLBAR_H

#include <QToolBar>
#include <QStackedWidget>
#include "stfslider.h"
#include "mtfparam.h"

class Image;

class StretchToolbar : public QToolBar
{
    Q_OBJECT
    STFSlider *m_stfSlider;
    STFSlider *m_stfSliderR;
    STFSlider *m_stfSliderG;
    STFSlider *m_stfSliderB;
    QAction *m_autoStretchOnLoad;
    QAction *m_debayer;
    QStackedWidget *m_stack;
    MTFParam m_mtfParam;
public:
    explicit StretchToolbar(QWidget *parent = nullptr);
    ~StretchToolbar();
public slots:
    void stretchImage(Image *img);
    void resetMTF();
    void imageLoaded(Image *img);
    void unlinkStretch(bool enable);
signals:
    void paramChanged(const MTFParam &params);
    void autoStretch();
    void invert(bool enable);
    void superPixel(bool enable);
    void falseColor(bool enable);
};

#endif // STRETCHTOOLBAR_H
