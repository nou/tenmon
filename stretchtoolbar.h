#ifndef STRETCHTOOLBAR_H
#define STRETCHTOOLBAR_H

#include <QToolBar>
#include <QStackedWidget>
#include "stfslider.h"

class Image;

struct MTFParam
{
    float blackPoint[3] = {0.0f, 0.0f, 0.0f};
    float midPoint[3] = {0.5f, 0.5f, 0.5f};
    float whitePoint[3] = {1.0f, 1.0f, 1.0f};
};

class StretchToolbar : public QToolBar
{
    Q_OBJECT
    STFSlider *m_stfSlider;
    STFSlider *m_stfSliderR;
    STFSlider *m_stfSliderG;
    STFSlider *m_stfSliderB;
    QAction *m_autoStretchOnLoad;
    QAction *m_debayer;
    QStackedWidget *m_stack;
    MTFParam m_mtfParam;
public:
    explicit StretchToolbar(QWidget *parent = nullptr);
public slots:
    void stretchImage(Image *img);
    void resetMTF();
    void imageLoaded(Image *img);
    void unlinkStretch(bool enable);
signals:
    void paramChanged(const MTFParam &params);
    void autoStretch();
    void invert(bool enable);
    void superPixel(bool enable);
    void falseColor(bool enable);
};

#endif // STRETCHTOOLBAR_H
