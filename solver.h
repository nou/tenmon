#ifndef SOLVER_H
#define SOLVER_H

#include <stellarsolver.h>

class RawImage;

class Solver : public QObject
{
    Q_OBJECT
    StellarSolver *_solver;
    FITSImage::Statistic _stats;
    SSolver::ProcessType _process = SSolver::SOLVE;
    bool _loaded = false;
    QString _path;
    QString _error;
    std::shared_ptr<RawImage> _rawImage;
    std::shared_ptr<RawImage> _rawImagePlanar;
public:
    explicit Solver(QObject *parent = nullptr);
    ~Solver();
    void setIndexFolder(const QString &indexPath);

    bool loadImage(const QString &path);
    bool loadImage(std::shared_ptr<RawImage> &image, const QString &path);
    bool solveImage(bool sync = false);
    bool extractSources(bool hfr, bool sync = false);
    void abort();
    const FITSImage::Solution& getSolution() const;
    const QList<FITSImage::Star>& getStars() const;
    double getHFR() const;

    QString errorMessage() const;
    bool updateHeader(QString &error);
    void setParameters(SSolver::Parameters::ParametersProfile profile);
    void setParameters(const SSolver::Parameters &parameters);
    SSolver::Parameters getProfile() const;
    void setSearchScale(double fovLow, double fowHigh, ScaleUnits units);
    void setSearchPosition(double ra, double dec);
    void clearStartingPositionAndScale();

    static QStringList getIndexPaths();
    static QString getTenmonIndexPath();
public slots:
    void finished();
signals:
    void solvingDone();
    void extractionDone();
    void logOutput(const QString &log);
};

#endif // SOLVER_H
