#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QStatusBar>
#include <QLabel>

class StatusBar : public QStatusBar
{
    Q_OBJECT
    QLabel *m_value;
    QLabel *m_pixelCoords;
    QLabel *m_celestianCoords;
public:
    explicit StatusBar(QWidget *parent = nullptr);
public slots:
    void newStatus(const QString &value, const QString &pixelCoords, const QString &celestialCoords);
};

#endif // STATUSBAR_H
