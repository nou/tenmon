#ifndef IMAGESCROLLAREAGL_H
#define IMAGESCROLLAREAGL_H

#include <memory>
#include <QObject>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QScrollBar>
#include <QTimer>
#include "rawimage.h"
#include "imageringlist.h"
#include "database.h"
#include "stretchtoolbar.h"

struct ImageThumb
{
    QString name;
    QString path;
    QSize size;
    bool selected;
    bool dirty;
};

class ImageWidget : public QOpenGLWidget
{
    Q_OBJECT
    QOpenGLFunctions *f = nullptr;
    QOpenGLFunctions_3_3_Core *f3 = nullptr;
    QTimer *m_updateTimer = nullptr;
    std::unique_ptr<QOpenGLShaderProgram> m_program;
    std::unique_ptr<QOpenGLShaderProgram> m_thumbnailProgram;
    std::unique_ptr<QOpenGLShaderProgram> m_debayerProgram;
    std::unique_ptr<QOpenGLBuffer> m_buffer;
    std::unique_ptr<QOpenGLBuffer> m_bufferSizes;
    std::unique_ptr<QOpenGLTexture> m_image;
    std::unique_ptr<QOpenGLVertexArrayObject> m_vao;
    std::unique_ptr<QOpenGLVertexArrayObject> m_vaoThumb;
    std::unique_ptr<QOpenGLPixelTransferOptions> m_transferOptions;
    std::unique_ptr<QOpenGLTexture> m_thumbnailTexture;
    GLuint m_debayerTex = 0;
    std::shared_ptr<RawImage> m_rawImage;
    std::shared_ptr<WCSData> m_wcs;
    int m_width, m_height;
    int m_imgWidth = -1, m_imgHeight = -1;
    int m_currentImg = 0;
    MTFParam m_mtfParams;
    float m_unit_scale[2] = {1.0f, 0.0f}; // scale and offset
    float m_dx = 0, m_dy = 0;
    float m_scale = 1.0f;
    int m_scaleStop = 0;
    bool m_bestFit = false;
    bool m_blockRepaint = false;
    bool m_bwImg = false;
    bool m_falseColor = false;
    bool m_invert = false;
    bool m_superpixel = false;
    bool m_showThumbnails = false;
    bool m_selecting = false;
    bool m_sizesDirty = false;
    bool m_srgb = false;
    int m_thumbnailCount = 0;
    int m_maxTextureSize = 0;
    int m_maxArrayLayers = 0;
    int m_firstRed[2] = {0, 0};
    QVector<ImageThumb> m_thumnails;
    Database *m_database = nullptr;
    QPointF m_lastPos;
    QString m_error;
public:
    explicit ImageWidget(Database *database, QWidget *parent = nullptr);
    ~ImageWidget() override;
    void setImage(std::shared_ptr<RawImage> image, int index);
    void setImage(const QPixmap &pixmap);
    void setWCS(std::shared_ptr<WCSData> wcs);
    void zoom(int zoom, const QPointF &mousePos = QPointF());
    void bestFit();
    void blockRepaint(bool block);
    void allocateThumbnails(const QStringList &paths);
    QVector2D getImagePixelCoord(const QVector2D &pos);
    void setBayerMask(int mask);
public slots:
    void setMTFParams(const MTFParam &params);
    void setOffset(float dx, float dy);
    void superPixel(bool enable);
    void invert(bool enable);
    void falseColor(bool enable);
    QImage renderToImage();
    void thumbnailLoaded(const Image *image);
    void showThumbnail(bool enable);
protected:
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void initializeGL() override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void thumbSelect(QMouseEvent *event);
    void debayer();
    void updateScrollBars();
signals:
    void fileDropped(const QString &path);
    void status(const QString &value, const QString &pixelCoords, const QString &celestialCoords);
    void scrollBarsUpdate(int valueH, int stepH, int maxH, int valueV, int stepV, int maxV);
};

class ImageScrollAreaGL : public QWidget
{
    Q_OBJECT
    QScrollBar *m_verticalScrollBar;
    QScrollBar *m_horizontalScrollBar;
    ImageWidget *m_imageWidget;
public:
    explicit ImageScrollAreaGL(Database *database, QWidget *parent = nullptr);
    ~ImageScrollAreaGL() override;
    void setImage(Image *image);
    ImageWidget* imageWidget();
protected:
    void updateScrollbars(int valueH, int stepH, int maxH, int valueV, int stepV, int maxV);
public slots:
    void zoomIn();
    void zoomOut();
    void bestFit();
    void oneToOne();
protected slots:
    void scrollEvent();
};

#endif // IMAGESCROLLAREAGL_H
