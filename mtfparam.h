#ifndef MTFPARAM_H
#define MTFPARAM_H

struct MTFParam
{
    float blackPoint[3] = {0.0f, 0.0f, 0.0f};
    float midPoint[3] = {0.5f, 0.5f, 0.5f};
    float whitePoint[3] = {1.0f, 1.0f, 1.0f};
};

#endif // MTFPARAM_H
