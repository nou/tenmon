#include "platesolvingsettings.h"
#include "ui_platesolvingsettings.h"
#include <QSettings>
#include <QFileDialog>
#include "solver.h"

PlateSolvingSettings::PlateSolvingSettings(QWidget *parent) : QDialog(parent)
    , _ui(new Ui::PlateSolvingSettings)
{
    _ui->setupUi(this);

    _download = new HttpDownloader(this);
    connect(_download, &HttpDownloader::progress, this, &PlateSolvingSettings::progress);
    connect(_ui->stopDownloadButton, &QPushButton::clicked, _download, &HttpDownloader::abort);

    connect(_ui->scale01, &QCheckBox::clicked, [this](){ if(_ui->scale01->isChecked())_download->downloadIndex(1); });
    connect(_ui->scale02, &QCheckBox::clicked, [this](){ if(_ui->scale02->isChecked())_download->downloadIndex(2); });
    connect(_ui->scale03, &QCheckBox::clicked, [this](){ if(_ui->scale03->isChecked())_download->downloadIndex(3); });
    connect(_ui->scale04, &QCheckBox::clicked, [this](){ if(_ui->scale04->isChecked())_download->downloadIndex(4); });
    connect(_ui->scale05, &QCheckBox::clicked, [this](){ if(_ui->scale05->isChecked())_download->downloadIndex(5); });
    connect(_ui->scale06, &QCheckBox::clicked, [this](){ if(_ui->scale06->isChecked())_download->downloadIndex(6); });
    connect(_ui->scale07, &QCheckBox::clicked, [this](){ if(_ui->scale07->isChecked())_download->downloadIndex(7); });
    connect(_ui->scale08, &QCheckBox::clicked, [this](){ if(_ui->scale08->isChecked())_download->downloadIndex(8); });
    connect(_ui->scale09, &QCheckBox::clicked, [this](){ if(_ui->scale09->isChecked())_download->downloadIndex(9); });
    connect(_ui->scale10, &QCheckBox::clicked, [this](){ if(_ui->scale10->isChecked())_download->downloadIndex(10); });
    connect(_ui->scale11, &QCheckBox::clicked, [this](){ if(_ui->scale11->isChecked())_download->downloadIndex(11); });
    connect(_ui->scale12, &QCheckBox::clicked, [this](){ if(_ui->scale12->isChecked())_download->downloadIndex(12); });
    connect(_ui->scale13, &QCheckBox::clicked, [this](){ if(_ui->scale13->isChecked())_download->downloadIndex(13); });
    connect(_ui->scale14, &QCheckBox::clicked, [this](){ if(_ui->scale14->isChecked())_download->downloadIndex(14); });
    connect(_ui->scale15, &QCheckBox::clicked, [this](){ if(_ui->scale15->isChecked())_download->downloadIndex(15); });
    connect(_ui->scale16, &QCheckBox::clicked, [this](){ if(_ui->scale16->isChecked())_download->downloadIndex(16); });
    connect(_ui->scale17, &QCheckBox::clicked, [this](){ if(_ui->scale17->isChecked())_download->downloadIndex(17); });
    connect(_ui->scale18, &QCheckBox::clicked, [this](){ if(_ui->scale18->isChecked())_download->downloadIndex(18); });
    connect(_ui->scale19, &QCheckBox::clicked, [this](){ if(_ui->scale19->isChecked())_download->downloadIndex(19); });

    QSettings settings;
    _ui->indexPaths->addItems(settings.value("platesolving/indexPaths", Solver::getIndexPaths()).toStringList());
    _ui->indexPaths->setCurrentText(settings.value("platesolving/indexPath", Solver::getTenmonIndexPath()).toString());
    connect(_ui->addButton, &QPushButton::clicked, [this](){
        QString path = QFileDialog::getExistingDirectory(this, tr("Index files directory"), Solver::getTenmonIndexPath());
        if(!path.isEmpty())
        {
            bool contain = false;
            for(int i=0; i<_ui->indexPaths->count(); i++)
            {
                if(path == _ui->indexPaths->itemText(i))
                {
                    contain = true;
                    break;
                }
            }

            if(!contain)_ui->indexPaths->addItem(path);
        }
    });
    connect(_ui->removeButton, &QPushButton::clicked, [this](){
        int current = _ui->indexPaths->currentIndex();
        if(current > 0)_ui->indexPaths->removeItem(current);
    });

    _watcher = new QFileSystemWatcher(this);
    _watcher->addPath(Solver::getTenmonIndexPath());
    connect(_watcher, &QFileSystemWatcher::directoryChanged, this, &PlateSolvingSettings::checkIndexFiles);
    connect(_ui->indexPaths, &QComboBox::currentTextChanged, [this](const QString &text){
        _watcher->removePaths(_watcher->directories());
        _watcher->addPath(text);
    });
    connect(_ui->indexPaths, &QComboBox::currentIndexChanged, [this](int index){
        _ui->indexFilesGroup->setEnabled(index == 0);
    });
    checkIndexFiles();
}

PlateSolvingSettings::~PlateSolvingSettings()
{
    QSettings settings;
    settings.setValue("platesolving/indexPath", _ui->indexPaths->currentText());
    QStringList paths;
    for(int i=0; i<_ui->indexPaths->count(); i++)
        paths.append(_ui->indexPaths->itemText(i));

    settings.setValue("platesolving/indexPaths", paths);
    delete _ui;
}

void PlateSolvingSettings::checkIndexFiles()
{
    QString indexDir = Solver::getTenmonIndexPath() + "/";
    auto checkScale = [indexDir](QCheckBox *box, int scale)
    {
        bool all = true;
        QStringList files = HttpDownloader::indexFileNames(scale);
        for(auto &file : files)
            if(!QFile::exists(indexDir + file))
            {
                all = false;
                break;
            }

        box->setChecked(all);
        if(all)box->setStyleSheet("color: green; font: bold;");
        else box->setStyleSheet("");
    };

    checkScale(_ui->scale01, 1);
    checkScale(_ui->scale02, 2);
    checkScale(_ui->scale03, 3);
    checkScale(_ui->scale04, 4);
    checkScale(_ui->scale05, 5);
    checkScale(_ui->scale06, 6);
    checkScale(_ui->scale07, 7);
    checkScale(_ui->scale08, 8);
    checkScale(_ui->scale09, 9);
    checkScale(_ui->scale10, 10);
    checkScale(_ui->scale11, 11);
    checkScale(_ui->scale12, 12);
    checkScale(_ui->scale13, 13);
    checkScale(_ui->scale14, 14);
    checkScale(_ui->scale15, 15);
    checkScale(_ui->scale16, 16);
    checkScale(_ui->scale17, 17);
    checkScale(_ui->scale18, 18);
    checkScale(_ui->scale19, 19);
}

QString PlateSolvingSettings::indexDirectory() const
{
    return _ui->indexPaths->currentText();
}

void PlateSolvingSettings::progress(int percent, int files)
{
    _ui->filesRemaining->setText(tr("%1 files").arg(files));
    _ui->downloadProgressbar->setValue(percent);
}
